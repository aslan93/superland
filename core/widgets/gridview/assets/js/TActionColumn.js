var TActionColumn = {
	
	init: function()
	{
		TActionColumn.delete_button.init();
	},
	
	delete_button: {
		
		selector: '',
		
		init: function()
		{
			var delete_button = TActionColumn.delete_button;
			
			if($(document).find(delete_button.selector).length > 0)
			{
				delete_button.run();
			}
			else
			{
				TActionColumn.log('delete_button', 'selector "' + delete_button.selector + '" not found');
			}
		},
		
		run: function()
		{
			var delete_button = TActionColumn.delete_button;
			
			$(document).on('ready pjax:success', function() {
				$(delete_button.selector).on('click', function(e) {
					
					e.preventDefault();
					
					if(!delete_button.check_selector($(this)))
					{
						return false;
					}
					
					var deleteUrl = $(this).attr('delete-url');
					
					var pjaxContainer = $(this).attr('pjax-container');
					
					if(confirm('Are you sure you want to delete this item?'))
					{
						$.ajax({
							url: deleteUrl,
							type: 'get',
							error: function(xhr, status, error) {
								alert('There was an error with your request.' + xhr.responseText);
							}
						})
						.done(function(data) { 
							$.pjax.reload({container: '#' + $.trim(pjaxContainer)});
						});
					}           
				});
			});
		},
		
		check_selector: function(selector)
		{
			var is_valid = true;
			if(!(selector.attr('delete-url').length > 0))
			{
				is_valid = false;
				TActionColumn.log('check_selector', "selector doesn't have attr:'delete-url'");
			}
			
			if(is_valid && !(selector.attr('pjax-container').length > 0))
			{
				is_valid = false;
				TActionColumn.log('check_selector', "selector doesn't have attr:'pjax-container'");
			}
			
			return is_valid;
		}
		
	},
	
	log: function(method, message)
	{
		console.warn('TActionColumn.' + method + ': ' + message);
	}
}