<?php

namespace app\controllers;

use app\models\Gallery\Gallery;
use app\models\Games\Games;
use app\models\Games\GamesSearch;
use app\models\User\User;
use app\models\User\UserChild;
use app\modules\BuyTickets\models\BuyTickets;
use app\modules\EventMenu\models\EventMenu;
use app\modules\EventPackage\models\EventPackage;
use app\modules\EventRoom\models\EventRoom;
use app\modules\Events\models\Events;
use app\modules\Events\models\EventsSearch;
use app\modules\Events\models\PlanEvent;
use app\modules\ExtraOptions\models\ExtraOptions;
use app\modules\Food\models\FoodSearch;
use app\modules\FoodCategory\FoodCategory;
use app\modules\Map\models\MapMarkers;
use app\modules\Tickets\models\Tickets;
use Yii;
use app\controllers\FrontController;
use app\models\Article\Article;
use app\models\Link\Link;




class BuyController extends FrontController
{

    public $layout = 'frontend';


    public function actionIndex($route)
    {
        return $this->resolveRoute($route);
    }

    public function resolveRoute($route)
    {
        $segments = explode('/', $route);
        $segments = array_filter($segments);
        $current_segment = end($segments);
        if ($current_segment) {
            $item = Link::find()->with(['article', 'article.lang'])->where(['Link' => $current_segment])->one();

            if (!empty($item->article) && $item->article != NULL) {
                $article = $item->article;

                if (empty($article->Template)) {
                    $view = strtolower($article->Type);
                } else {
                    $view = strtolower($article->Template);
                }

                return $this->render($view, [
                    'article' => $article
                ]);
            }
        } else {
            $games = Games::find()->with('lang')->all();
            $news = Article::find()->with('lang')->where(['Type' => 'News'])->all();
            $news = array_chunk($news, 3);

            return $this->render('home', [
                'games' => $games,
                'news' => $news,
            ]);
        }

        return $this->actionError();
    }

    public function actionError()
    {
        return $this->render('error', [
            'message' => 'Page not found'
        ]);
    }

    public function actionTicket1()
    {
        $buyer = new BuyTickets();
        $ticketsP = Tickets::find()->with('lang')->where(['Type'=>Tickets::TicketTypeParents])->all();
        $ticketsK = Tickets::find()->with('lang')->where(['Type'=>Tickets::TicketTypeKids])->all();

        if ($buyer->load(Yii::$app->request->post()) && $buyer->save()) {
            unset($_POST);
            return $this->redirect(['ticket2', 'id' => $buyer->ID]);

        } else {

            return $this->render('ticket1', [
                'buyer' => $buyer,
                'ticketsP' => $ticketsP,
                'ticketsK' => $ticketsK,
            ]);
        }
    }
    public function actionTicket2($id)
    {
        $buyer = BuyTickets::find()->where(['ID'=>$id])->one();

        if ($buyer->load(Yii::$app->request->post()) && $buyer->save()) {
            unset($_POST);
            return $this->redirect(['ticket3', 'id' => $buyer->ID]);

        } else {
            $tickets = $buyer->tickets;
            return $this->render('ticket2', [
                'buyer' => $buyer,
                'tickets' => $tickets,
            ]);
        }
    }

    public function actionTicket3($id)
    {
        $buyer = BuyTickets::find()->where(['ID'=>$id])->one();

        if ($buyer->load(Yii::$app->request->post()) && $buyer->save()) {
            unset($_POST);
            return $this->redirect(['pay', 'id' => $buyer->ID]);
        } else {
            $tickets = $buyer->tickets;
            $price = 0;
            foreach ($tickets as $ticket){
                $price = $price + $ticket->Price;
            }
            $buyer->Total = $price;
            $buyer->Save();
            return $this->render('ticket3', [
                'buyer' => $buyer,
                'tickets' => $tickets,
                'price' => $price,
            ]);
        }
    }

    public function actionPay($id)
    {
        return 'payment: '.$id;
    }

    public function actionPlan1()
    {
        $model = new PlanEvent();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->Price = $model->Price + $model->roomPrice;
            unset($_POST);
            $model->save();
            return $this->redirect(['plan2', 'id' => $model->ID]);

        } else {
            $rooms = EventRoom::find()->with('lang')->all();
            return $this->render('plan1',[
                'model'=>$model,
                'rooms'=>$rooms,
            ]);
        }

    }
    public function actionPlan2($id)
    {
        $model = PlanEvent::find()->where(['ID'=>$id])->one();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->Price = $model->Price + $model->packagePrice;
            $model->save();
            unset($_POST);
            return $this->redirect(['plan3', 'id' => $model->ID]);

        } else {
            $packages = EventPackage::find()->with('lang')->all();
            return $this->render('plan2',[
                'model'=>$model,
                'packages'=>$packages,
            ]);
        }

    }

    public function actionPlan3($id)
    {
        $model = PlanEvent::find()->where(['ID'=>$id])->one();

        if ($model->load(Yii::$app->request->post()) ) {

            if ($_POST['Menu']) {
                $model->Menu = serialize($_POST['Menu']);
            }
            if ($_POST['ExtraOptions']) {
                $model->ExtraOptions = serialize($_POST['ExtraOptions']);
            }
            $model->save();
            unset($_POST);
            return $this->redirect(['payment', 'id' => $model->ID]);

        } else {
            $eventMenus = EventMenu::find()->with('lang')->all();
            $extraOptions = ExtraOptions::find()->with('lang')->all();
            $modelEO = $model->eo;
            $modelEM = $model->em;
            $price = $model->Price;
            if($modelEO) {
                foreach ($modelEO as $eo) {
                    $price = $price + ($eo->Price * $model->KidsNumber);
                }
            }
            if($modelEM) {
                foreach ($modelEM as $em) {
                    $price = $price + ($em->Price * $model->KidsNumber);
                }
            }

            return $this->render('plan3',[
                'model' => $model,
                'price' => $price,
                'eventMenus' => $eventMenus,
                'extraOptions'=>$extraOptions,
                'id' =>$id,
            ]);
        }
    }
}


