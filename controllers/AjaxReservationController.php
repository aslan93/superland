<?php
/**
 * Created by PhpStorm.
 * User: dennix
 * Date: 05.09.17
 * Time: 16:00
 */

namespace app\controllers;

use app\controllers\FrontController;
use app\models\Article\Article;
use app\modules\Hotel\models\Hotel;
use yii\data\ActiveDataProvider;

class AjaxReservationController extends FrontController
{
    public function actionGetHotels($hid = false){
        if ($hid){
            $hotels = Hotel::find()->where(['ID' => $hid])->with('lang')->all();
            $dataProvider = new ActiveDataProvider([
                'query' => Hotel::find()->where(['ID' => $hid])->with('lang'),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
        }else {
            $hotels = Hotel::find()->with('lang')->all();
            $dataProvider = new ActiveDataProvider([
                'query' => Hotel::find()->with('lang'),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
        }

        return $this->renderPartial('reservation', [
            'hotels' => $hotels,
            'dataProvider' => $dataProvider,


        ]);

    }
}