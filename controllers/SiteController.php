<?php

namespace app\controllers;

use app\models\Gallery\Gallery;
use app\models\Games\Games;
use app\models\Games\GamesLang;
use app\models\Games\GamesSearch;
use app\models\User\LoginForm;
use app\models\User\User;
use app\models\User\UserChild;
use app\modules\Boutique\models\BoutiqueLang;
use app\modules\Boutique\models\BoutiqueSearch;
use app\modules\BoutiqueCategory\models\BoutiqueCategory;
use app\modules\Events\models\Events;
use app\modules\Events\models\EventsLang;
use app\modules\Events\models\EventsSearch;
use app\modules\Faq\models\Faq;
use app\modules\FaqCategory\models\FaqCategory;
use app\modules\Food\models\FoodLang;
use app\modules\Food\models\FoodSearch;
use app\modules\FoodCategory\FoodCategory;
use app\modules\Map\models\MapMarkers;
use Yii;
use app\controllers\FrontController;
use app\models\Article\Article;
use app\models\Link\Link;
use yii\helpers\Url;


class SiteController extends FrontController
{
    
    public $layout = 'frontend';

    
    public function actionIndex()
    {
        return $this->resolveRoute();
    }
    
    public function resolveRoute()
    {


            $events = Events::find()->with('lang')->all();
            return $this->render('home', [
                'events' => $events,
            ]);

    }

    public function actionError()
    {
        return $this->render('error', [
            'message' => 'Page not found'
        ]);
    }

    public function actionMap(){
        $markers1 = MapMarkers::find()->with('lang')->where(['Etaj'=>1])->all();
        $markers2 = MapMarkers::find()->with('lang')->where(['Etaj'=>2])->all();
        $childs = Yii::$app->user->identity->userChilds;
        $childMarker = [];
        if ($childs){
            foreach ($childs as $key => $child){
                $childMarker[$key] = [
                    'type' => 'child',
                    'top' => 50+($key*5),
                    'left' => 50+($key*10),
                    'icon' => 'fa-child',
                    'style' => "color:red!important;",
                    'class' => 'child-marker',
                ];
            }
        }
        $array1 = [];
        foreach ($markers1 as $key => $marker){
           $array1[$key]['id'] = $marker->ID;
            $array1[$key]['top'] = $marker->Top;
            $array1[$key]['left'] = $marker->Left;
            $array1[$key]['icon'] = $marker->Icon;

        }
        $array1 = array_merge($array1,$childMarker);
        $array2 = [];
        foreach ($markers2 as $key => $marker){
            $array2[$key]['id'] = $marker->ID;
            $array2[$key]['top'] = $marker->Top;
            $array2[$key]['left'] = $marker->Left;
            $array2[$key]['icon'] = $marker->Icon;

        }
        $array1 = \GuzzleHttp\json_encode($array1);
        $array2 = \GuzzleHttp\json_encode($array2);
        return $this->render('map',[
            'markers1' => $array1,
            'markers2' => $array2,
        ]);
    }

    public function actionRegistration()
    {
        return $this->render('registration');
    }

    public function actionEvents()
    {
        $galleries = Gallery::find()->with('items')->limit(6)->all();
        return $this->render('events',[
            'galleries'=> $galleries,
            ]);
    }
    public function actionFaq()
    {
        $faqCats = FaqCategory::find()->with(['lang','faqs'])->all();
        return $this->render('faq',[
            'faqCats' => $faqCats,
        ]);
    }
    public function actionContact()
    {
        return $this->render('contactUS');
    }
    public function actionJoinUs()
    {
        return $this->render('join-us');
    }
    public function actionLogin()
    {   $model = new LoginForm();
        if (Yii::$app->request->isPost){
            if ($model->load(Yii::$app->request->post()) && $model->login()){
                return $this->redirect(Url::to(['/site']));
            }
        }
        return $this->render('login',[
            'model'=>$model,
        ]);
    }
    public function actionEventsTab()
    {

    if (Yii::$app->request->isGet) {
        $id = Yii::$app->request->get('id');
        $searchModel = new EventsSearch([
            'ID' => $id,
        ]);
    }else{
        $searchModel = new EventsSearch();
    }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $galleries = Gallery::find()->with('items')->limit(6)->all();
        return $this->render('events-tab',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'galleries' => $galleries,

            ]);
    }
    public function actionAttractions()
    {
        if (Yii::$app->request->isGet) {
            $id = Yii::$app->request->get('id');
            $searchModel = new GamesSearch([
                'ID' => $id
            ]);
        }else{
            $searchModel = new GamesSearch();
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('attractions',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionFood()
    {
        if (Yii::$app->request->isGet) {
            $id = Yii::$app->request->get('id');
            $searchModel = new FoodSearch([
                'ID' => $id
            ]);
        }else{
            $searchModel = new FoodSearch();
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $categories = \app\modules\FoodCategory\models\FoodCategory::find()->with('lang')->all();
        return $this->render('food-tab',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'categories' => $categories,
        ]);
    }

    public function actionBoutique()
    {
        if (Yii::$app->request->isGet) {
            $id = Yii::$app->request->get('id');
            $searchModel = new BoutiqueSearch([
                'ID' => $id
            ]);
        }else{
            $searchModel = new BoutiqueSearch();
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $categories = BoutiqueCategory::find()->with('lang')->all();
        return $this->render('boutique-tab',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'categories' => $categories,
        ]);
    }

    public function actionResult($slug)
        {  if(EventsLang::find()->where(['EventID'=>$slug])->one()){
            return $this->redirect(['/events-tab','id'=>$slug,'#'=>'view']);
        }elseif($game = GamesLang::find()->where(['Title'=>$slug])->one()){
            return $this->redirect(['/attractions','id'=> $game->GameID,'#'=>'view']);
        }elseif($food = FoodLang::find()->where(['Name'=>$slug])->one()){
            return $this->redirect(['/food','id'=> $food->FoodID,'#'=>'view']);
        }elseif($bout = BoutiqueLang::find()->where(['Name'=>$slug])->one()){
            return $this->redirect(['/boutique','id'=> $bout->BoutiqueID,'#'=>'view']);
        }else{
            return $this->render('noResults');
        }
    }
}
