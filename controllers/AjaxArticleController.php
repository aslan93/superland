<?php
/**
 * Created by PhpStorm.
 * User: dennix
 * Date: 05.09.17
 * Time: 16:00
 */

namespace app\controllers;

use app\controllers\FrontController;
use app\models\Article\Article;

class AjaxArticleController extends FrontController
{
    public function actionGetNew($id){

        $new = Article::find()->where(['ID' => $id])->with('lang')->one();
        return $this->renderPartial('/news/new', [
            'new' => $new,
        ]);

    }
}