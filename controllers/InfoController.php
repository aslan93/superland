<?php

namespace app\controllers;

use app\models\Gallery\Gallery;
use app\models\Games\Games;
use app\models\Games\GamesSearch;
use app\models\User\User;
use app\models\User\UserChild;
use app\modules\BuyTickets\models\BuyTickets;
use app\modules\EventMenu\models\EventMenu;
use app\modules\EventPackage\models\EventPackage;
use app\modules\EventRoom\models\EventRoom;
use app\modules\Events\models\Events;
use app\modules\Events\models\EventsSearch;
use app\modules\Events\models\PlanEvent;
use app\modules\ExtraOptions\models\ExtraOptions;
use app\modules\Food\models\FoodSearch;
use app\modules\FoodCategory\FoodCategory;
use app\modules\Jobs\models\Jobs;
use app\modules\Jobs\models\JobsSearch;
use app\modules\Map\models\MapMarkers;
use app\modules\Post\models\Post;
use app\modules\Post\models\PostSearch;
use app\modules\Tickets\models\Tickets;
use Yii;
use app\controllers\FrontController;
use app\models\Article\Article;
use app\models\Link\Link;


class InfoController extends FrontController
{

    public $layout = 'frontend';


    public function actionIndex($route)
    {
        return $this->resolveRoute($route);
    }

    public function resolveRoute($route)
    {
        $segments = explode('/', $route);
        $segments = array_filter($segments);
        $current_segment = end($segments);
        if ($current_segment) {
            $item = Link::find()->with(['article', 'article.lang'])->where(['Link' => $current_segment])->one();

            if (!empty($item->article) && $item->article != NULL) {
                $article = $item->article;

                if (empty($article->Template)) {
                    $view = strtolower($article->Type);
                } else {
                    $view = strtolower($article->Template);
                }

                return $this->render($view, [
                    'article' => $article
                ]);
            }
        } else {
//            $games = Games::find()->with('lang')->all();
//            $news = Article::find()->with('lang')->where(['Type' => 'News'])->all();
//            $news = array_chunk($news, 3);

            return $this->redirect('/', [

            ]);
        }

        return $this->actionError();
    }

    public function actionError()
    {
        return $this->render('error', [
            'message' => 'Page not found'
        ]);
    }

    public function actionBlog()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('blog',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionSingleBlog($id)
    {
        $post = Post::find()->where(['ID'=>$id])->with()->one();
        $related = Post::find()->with('lang')->limit(4)->andWhere(['<>','ID', $id])->all();
        return $this->render('single_blog',[
            'post'=>$post,
            'related' =>$related
        ]);
    }

    public function actionJobs()
    {
        $searchModel = new JobsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('jobs',[
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionSingleJob($id)
    {
        $job = Jobs::find()->where(['ID'=>$id])->with()->one();
        return $this->render('single_job',[
            'job'=>$job,
        ]);
    }


    public function actionTerms()
    {
        return $this->render('terms');
    }

    public function actionPrivacyPolicy()
    {
        return $this->render('privacy');
    }

    public function actionEvent($id)
    {
        $this->layout = 'event';
        $event = Events::find()->with(['lang','location'])->where(['ID' => $id])->one();
        $event->Visualisations = $event->Visualisations + 1;
        $event->save();
        $views = $event->Visualisations;
        $galleries = Gallery::find()->with('items')->where(['EventID'=>$id])->limit(6)->all();

        return $this->render('event_view', [
            'event' => $event,
            'galleries'=> $galleries,
            'views'=>$views,
        ]);
    }

}
