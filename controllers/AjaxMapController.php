<?php


namespace app\controllers;

use app\controllers\FrontController;
use app\modules\Map\models\MapMarkers;

class AjaxMapController extends  FrontController
{
    public function actionGetMarker($id)
    {
        $marker = MapMarkers::find()->with(['lang','location'])->where(['ID' => $id])->one();


        return $this->renderPartial('map-info', [
            'marker' => $marker,
        ]);

    }
}