<?php


namespace app\controllers;


use app\models\Gallery\Gallery;
use app\models\Games\GamesLang;
use app\modules\Boutique\models\Boutique;
use app\modules\Boutique\models\BoutiqueLang;
use app\modules\EventRoom\models\EventRoom;
use app\modules\Events\models\Events;
use app\modules\Events\models\EventsLang;
use app\modules\Events\models\PlanEvent;
use app\modules\Food\models\Food;
use app\modules\Food\models\FoodLang;


class AjaxController extends  FrontController
{
    public function actionSearch($term)
    {
        $data1 = EventsLang::find()
            ->select(["Name as value" ,"Name as label"  , "EventID as id"])
            ->where(['like','Name',$term])
            ->andWhere(['LangID' => \Yii::$app->language])
            ->asArray()
            ->all();

        foreach ($data1 as $key => $dt){
            $data1[$key]['label'] = $data1[$key]['label'] .' ('.\Yii::t('app','Eveniment').')';
        }


        $data2 = FoodLang::find()
            ->select(["Name as value" ,"Name as label"  , "Name as id",])
            ->where(['like','Name',$term])
            ->andWhere(['LangID' => \Yii::$app->language])
            ->asArray()
            ->all();
        foreach ($data2 as $key => $dt){
            $data2[$key]['label'] = $data2[$key]['label'] .' ('.\Yii::t('app','Food&Relax').')';
        }
        $data3 = BoutiqueLang::find()
            ->select(["Name as value" ,"Name as label"  , "Name as id",])
            ->where(['like','Name',$term])
            ->andWhere(['LangID' => \Yii::$app->language])
            ->asArray()
            ->all();
        foreach ($data3 as $key => $dt){
            $data3[$key]['label'] = $data3[$key]['label'] .' ('.\Yii::t('app','Boutique Shopping').')';
        }
        $data4 = GamesLang::find()
            ->select(["Title as value" ,"Title as label"  , "Title as id",])
            ->where(['like','Title',$term])
            ->andWhere(['LangID' => \Yii::$app->language])
            ->asArray()
            ->all();
        foreach ($data4 as $key => $dt){
            $data4[$key]['label'] = $data4[$key]['label'] .' ('.\Yii::t('app','Games').')';
        }
       $data = array_merge($data1,$data2,$data3,$data4);

        return json_encode($data);
    }

    public function actionGetEvent($id)
    {
        $event = Events::find()->with(['lang','location'])->where(['ID' => $id])->one();
        $event->Visualisations = $event->Visualisations + 1;
        $event->save();
        $views = $event->Visualisations;
        $galleries = Gallery::find()->with('items')->where(['EventID'=>$id])->limit(6)->all();

        return $this->renderPartial('event_view', [
            'event' => $event,
            'galleries'=> $galleries,
            'views'=>$views,
        ]);
    }

    public function actionGetRoom($id)
    {
        $room = EventRoom::find()->where(['ID'=>$id])->with('lang')->one();

        return $this->renderPartial('room_view', [
                'room' => $room,
        ]);
    }

    public function actionGetGallery($id)
    {
        $gallery = Gallery::find()->with(['lang','event'])->where(['ID' => $id])->one();
        $gallery->Visualisations = $gallery->Visualisations + 1;
        $gallery->save();
        return $this->renderPartial('gallery_view', [
            'gallery' => $gallery,
        ]);
    }
    public function actionGetFood($id)
    {
        $event = Food::find()->with(['lang','category'])->where(['ID' => $id])->one();
        return $this->renderPartial('food_view', [
            'event' => $event,
        ]);

    }

    public function actionGetBoutique($id)
    {
        $bout = Boutique::find()->with(['lang','category'])->where(['ID' => $id])->one();
        return $this->renderPartial('food_view', [
            'event' => $bout,
        ]);

    }

    public function actionSaveEvent($id)
    {
        $model = PlanEvent::find()->where(['ID'=>$id])->one();

        if ($model->load(\Yii::$app->request->post()) ) {
            if (isset(\Yii::$app->request->post('PlanEvent')['Menu'])) {
                $model->Menu = serialize(\Yii::$app->request->post('PlanEvent')['Menu']);
            }else{
                $model->Menu = '';
            }

            if (isset(\Yii::$app->request->post('PlanEvent')['ExtraOptions'])) {
                $model->ExtraOptions = serialize(\Yii::$app->request->post('PlanEvent')['ExtraOptions']);
            }else{
                $model->ExtraOptions = '';
            }

            $model->save();
            print_r($model);
        }

    }
}