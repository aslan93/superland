<?php

namespace app\controllers;

use app\models\Gallery\Gallery;
use app\models\Games\Games;
use app\models\Games\GamesLang;
use app\models\Games\GamesSearch;
use app\models\User\User;
use app\models\User\UserChild;
use app\modules\Boutique\models\BoutiqueLang;
use app\modules\Boutique\models\BoutiqueSearch;
use app\modules\BoutiqueCategory\models\BoutiqueCategory;
use app\modules\Events\models\Events;
use app\modules\Events\models\EventsLang;
use app\modules\Events\models\EventsSearch;
use app\modules\Faq\models\Faq;
use app\modules\FaqCategory\models\FaqCategory;
use app\modules\Food\models\FoodLang;
use app\modules\Food\models\FoodSearch;
use app\modules\FoodCategory\FoodCategory;
use app\modules\Map\models\MapMarkers;
use Yii;
use app\controllers\FrontController;
use app\models\Article\Article;
use app\models\Link\Link;




class LandingController extends FrontController
{
    
    public $layout = 'landing';

    
    public function actionIndex()
    {
            return $this->render('landing');
    }
    
//    public function resolveRoute($route)
//    {
//        $segments = explode('/', $route);
//        $segments = array_filter($segments);
//        $current_segment = end($segments);
//        if ($current_segment)
//        {
//            $item = Link::find()->with(['article', 'article.lang'])->where(['Link' => $current_segment])->one();
//
//            if (!empty($item->article) && $item->article != NULL)
//            {
//                $article = $item->article;
//
//                if (empty($article->Template))
//                {
//                    $view = strtolower($article->Type);
//                }
//                else
//                {
//                    $view =  strtolower($article->Template);
//                }
//
//                return $this->render($view, [
//                    'article' => $article
//                ]);
//            }
//        }
//        else
//        {
//
//            $events = Events::find()->with('lang')->all();
//            return $this->render('home', [
//                'events' => $events,
//            ]);
//        }
//
//        return $this->actionError();
//    }
//
//    public function actionError()
//    {
//        return $this->render('error', [
//            'message' => 'Page not found'
//        ]);
//    }

}
