<?php

namespace app\modules\Post\controllers;

use app\controllers\SiteController;

/**
 * Default controller for the `post` module
 */
class DefaultController extends SiteController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
