<?php
 use app\modules\Category\models\Category;
 use dosamigos\datepicker\DatePicker;
 use yii\bootstrap\Tabs;
?>

<?= $form->field($model, 'CreatedAt')->input('text',['class'=>'hidden'])->label(false)?>

<?php
$items = [];
foreach ($model->langs as $langID => $langModel)
{
    $items[] = [
        'label' => strtoupper($langID),
        'content' => $this->render('_desc_form',[
            'form' => $form,
            'langModel' => $langModel,
        ]),
    ];
}

echo Tabs::widget([
    'items' => $items,
]);
?>
