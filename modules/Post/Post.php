<?php

namespace app\modules\Post;

use yii\base\Module;



/**
 * post module definition class
 */
class Post extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Post\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
