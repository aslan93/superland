<?php
?>
<div>
    <h3>Feedbacks</h3>
    <?php
    foreach ($feedbacks as $feedback) {
        ?>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <b style="text-transform: uppercase;">
                        <?= $feedback->Name ?>
                    </b>
                    <div class="pull-right">
                        <?= $feedback->getdMYDate() ?>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <?= $feedback->getShortText() ?>...
                    </div>
                    <br>
                </div>
            </div>
        </div>

        <?php
    }
    ?>
</div>
