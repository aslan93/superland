<?php

namespace app\modules\Dashboard\controllers;

use app\controllers\BackendController;
use app\models\Feedback\Feedback;
use Yii;
/**
 * Default controller for the `Dashboard` module
 */
class DefaultController extends BackendController
{
    
    public function init()
    {
        parent::init();
    }
    
    public function permissions()
    {
        return [
            'index' => [
                'permission' => 'view_dashboard',
                'label' => Yii::t("app", 'Access to dashboard')
            ]
        ];
    }

    public function actionIndex()
    {
        $feedbacks = Feedback::find()->limit(4)->orderBy('Date ASC')->all();
        return $this->render('index',[
            'feedbacks' => $feedbacks,
        ]);
    }
}
