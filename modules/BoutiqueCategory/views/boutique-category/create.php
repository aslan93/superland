<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\BoutiqueCategory\models\BoutiqueCategory */

$this->title = 'Create Boutique Category';
$this->params['breadcrumbs'][] = ['label' => 'Boutique Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boutique-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
