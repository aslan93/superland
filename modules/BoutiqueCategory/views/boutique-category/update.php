<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\BoutiqueCategory\models\BoutiqueCategory */

$this->title = 'Update Boutique Category: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Boutique Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="boutique-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
