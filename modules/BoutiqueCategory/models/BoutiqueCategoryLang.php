<?php

namespace app\modules\BoutiqueCategory\models;

use Yii;

/**
 * This is the model class for table "BoutiqueCategoryLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property integer $BoutiqueCategoryID
 * @property string $LangID
 *
 * @property BoutiqueCategory $boutiqueCategory
 */
class BoutiqueCategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BoutiqueCategoryLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'BoutiqueCategoryID', 'LangID'], 'required'],
            [['Description'], 'string'],
            [['BoutiqueCategoryID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['BoutiqueCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => BoutiqueCategory::className(), 'targetAttribute' => ['BoutiqueCategoryID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'BoutiqueCategoryID' => 'Boutique Category ID',
            'LangID' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoutiqueCategory()
    {
        return $this->hasOne(BoutiqueCategory::className(), ['ID' => 'BoutiqueCategoryID']);
    }
}
