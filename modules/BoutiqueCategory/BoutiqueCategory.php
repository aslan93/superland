<?php

namespace app\modules\BoutiqueCategory;

/**
 * BoutiqueCategory module definition class
 */
class BoutiqueCategory extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\BoutiqueCategory\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
