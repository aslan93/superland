<?php

namespace app\modules\Map\controllers;

use app\modules\Map\models\MapMarkersLang;
use Yii;
use app\modules\Map\models\MapMarkers;
use app\modules\Map\models\MapMarkersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\BackendController;
use yii\base\Model;

/**
 * MapMarkersController implements the CRUD actions for MapMarkers model.
 */
class MapMarkersController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MapMarkers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MapMarkersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $renderFunction = Yii::$app->request->isAjax ? 'renderAjax' : 'render'; // spetz efect
        return $this->$renderFunction('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MapMarkers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MapMarkers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MapMarkers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MapMarkers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MapMarkers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MapMarkers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MapMarkers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MapMarkers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSaveMarkerPosition(){
        $position = Yii::$app->request->post();
        $model = MapMarkers::findOne($position['id']);
        $model->Top = $position['top'];
        $model->Left = $position['left'];
        $model->save();

        print_r($model);
        return 1;
    }

    public function actionGetItem()
    {
        $id = (int)Yii::$app->request->post('id');

        $sliderItem = $id > 0 ? MapMarkers::findOne($id) : new MapMarkers();

        $sliderItemLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $sliderItemLangs[$i] = isset($sliderItem->langs[$lang]) ? $sliderItem->langs[$lang] : new MapMarkersLang([
                'LangID' => $lang
            ]);
        }

        return $this->renderAjax('item', [
            'sliderItem' => $sliderItem,
            'sliderItemLangs' => $sliderItemLangs
        ]);
    }

    public function actionSaveItem()
    {
        $id = Yii::$app->request->post('MapMarkers')['ID'];

        $sliderItem = $id > 0 ? MapMarkers::findOne($id) : new MapMarkers();

        $sliderItemLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $sliderItemLangs[$i] = isset($sliderItem->langs[$lang]) ? $sliderItem->langs[$lang] : new MapMarkersLang([
                'LangID' => $lang
            ]);
        }

        if ($sliderItem->load(Yii::$app->request->post()))
        {
            $sliderItem->save();
            Model::loadMultiple($sliderItemLangs, Yii::$app->request->post());
            foreach ($sliderItemLangs as $sil)
            {
                $sil->MapMarkerID = $sliderItem->ID;
                $sil->save();
            }
            return 1;
        }
    }

    public function actionDeleteItem($id)
    {
        MapMarkers::deleteAll(['ID' => $id]);
        return 1;
    }
}
