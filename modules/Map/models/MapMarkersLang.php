<?php

namespace app\modules\Map\models;

use Yii;

/**
 * This is the model class for table "MapMarkersLang".
 *
 * @property integer $ID
 * @property integer $MapMarkerID
 * @property string $LangID
 * @property string $Title
 * @property string $Description
 *
 * @property MapMarkers $mapMarker
 */
class MapMarkersLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MapMarkersLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['MapMarkerID'], 'integer'],
            [['Description'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['MapMarkerID'], 'exist', 'skipOnError' => true, 'targetClass' => MapMarkers::className(), 'targetAttribute' => ['MapMarkerID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'MapMarkerID' => 'Map Marker ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapMarker()
    {
        return $this->hasOne(MapMarkers::className(), ['ID' => 'MapMarkerID']);
    }
}
