<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\Map\models\MapMarkers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="map-markers-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    $items = [];
    foreach ($model->langs as $key => $langModel)
    {
        $items[] = [

            'label' => strtoupper($key),
            'content' => $this->render('_desc_form',[
                'form' => $form,
                'langModel' => $langModel,
                'model' => $model,
            ]),
        ];
    }
    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>
    <?= $form->field($model,'Etaj')->dropDownList(\app\modules\Map\models\MapMarkers::getEtajList())?>
    <div class="form-group" >
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

