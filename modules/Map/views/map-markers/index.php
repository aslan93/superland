<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\Draggable;
use yii\jui\Droppable;
use app\modules\Map\assets\MapAssets;
use app\modules\Map\models\MapMarkers;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Map\models\MapMarkersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Map Markers';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="map-markers-index">
    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="add">
        <?= Html::button('Create Map Markers', [
            'class' => 'btn btn-primary edit-slider-item',
            'item-id' => 0
        ]) ?>
    </p>
    <br><br>
    <?php Pjax::begin([
        'enablePushState'    => false,
        'enableReplaceState' => false,
        'id'                 => 'pjax-index',
        'timeout'            => 5000,
    ]); ?>
    <?php
    $bundle = MapAssets::register($this);
    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'label' => 'Icon',
                'format' => 'html',
                'value' => function($model){
                    return "<i class='fa ".$model->Icon."' style='font-size: 30px;'></i>";
                },

            ],
            [
                'label' => 'Denumire',
                'value' => 'lang.Title',
            ],
            [
                    'label' => 'Etaj',
                    'value' => 'Etaj',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::button ('<span class="glyphicon glyphicon-pencil"></span> ',[
                            'class' => 'edit-slider-item spc',
                            'item-id' => $model->ID,
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::button ('<span class="glyphicon glyphicon-trash"></span> ',[
                            'class' => 'remove-item spc',
                            'data-id' => $model->ID
                        ]);
                    },
                ],
                'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;{delete}</div>'
            ],
        ],
    ]); ?>
    <div class="row">
        <br>
        <h1 class="text-center"> Etajul 1</h1>
        <br>
        <div class="col-md-12">
            <div id="droppable" class="ui-widget-header">
                <?php
                foreach (MapMarkers::find()->with('lang')->where(['Etaj'=>1])->all() as $marker){
                    ?>
                    <div id="dr2" class="draggable" data-id="<?=$marker->ID?>" data-top="<?=$marker->Top?>" data-left="<?=$marker->Left?>" >
                        <i class="fa <?=$marker->Icon?>"  style="font-size: 30px;">

                        </i>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <br>
    <div class="row ">
        <div class="remove-block col-md-1 pull-right " style="margin-right: 50px"><br><h4 class="text-center">Delete</h4></div>
    </div>
    <div class="row">
        <h1 class="text-center"> Etajul 2</h1>
        <br>
        <div class="col-md-12">
            <div id="droppable2" class="ui-widget-header">
                <?php
                foreach (MapMarkers::find()->with('lang')->where(['Etaj'=>2])->all() as $marker){
                    ?>
                    <div id="dr2" class="draggable2" data-id="<?=$marker->ID?>" data-top="<?=$marker->Top?>" data-left="<?=$marker->Left?>" >
                        <i class="fa <?=$marker->Icon?>"  style="font-size: 30px;">

                        </i>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <?php Modal::begin([
        'id' => 'edit-slider-item-modal',
        'header' => Yii::t("app", 'Editing slider item'),
        'size' => Modal::SIZE_LARGE
    ]); ?>

    <?php Modal::end(); ?>

    <?php $this->registerJs("
    $(document).on('click', '.edit-slider-item', function(){
        $('#edit-slider-item-modal .modal-body').empty();
        $.post('" . Url::to(['get-item']) . "', {id: $(this).attr('item-id')}, function(html){
            $('#edit-slider-item-modal .modal-body').html(html);
            $('#edit-slider-item-modal').modal();
        });
    });
       
    $(document).on('pjax:success', '#pjax-index',  function(event){
            initMarkers();
          }
        );
") ?>

<?php Pjax::end(); ?>
</div>


