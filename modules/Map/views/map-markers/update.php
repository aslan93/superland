<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Map\models\MapMarkers */

$this->title = 'Update Map Markers: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Map Markers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="map-markers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
