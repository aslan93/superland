<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\widgets\FileInput;
    use yii\bootstrap\Tabs;
    use yii\helpers\Url;
    use dosamigos\ckeditor\CKEditor;
    use dosamigos\tinymce\TinyMce;
?>

<?php $form = ActiveForm::begin([
    'id' => 'save-slider-item-form',
    'action' => Url::to(['save-slider-item'])
]); ?>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($sliderItem, 'Icon')->widget('\insolita\iconpicker\Iconpicker',
                [
                    'iconset'=>'fontawesome',
                    'clientOptions'=>[
                        'rows'=>5,
                        'cols'=>5,
                        'placement'=>'right',
                        'search'=> false,
                        ],
                ])->label('Choose icon');
            ?>

            <?= $form->field($sliderItem, 'ID')->hiddenInput(['value' => (int)$sliderItem->ID])->label(false) ?>

            <?php
                $items = [];
                foreach ($sliderItemLangs as $key => $sil)
                {
                    $items[] = [
                        'label' => strtoupper(Yii::$app->params['siteLanguages'][$key]),
                        'content' => $this->render('item-lang', [
                            'sil' => $sil,
                            'key' => $key,
                            'form' => $form
                        ]),
                        'active' => $key == 0
                    ];
                }
                
                echo Tabs::widget([
                   'items' => $items
                ]); ?>
            <?= $form->field($sliderItem,'Etaj')->dropDownList(\app\modules\Map\models\MapMarkers::getEtajList())?>
        </div>
    </div>

    <div>
        <div class="form-group text-right">
            <label style="display: block;" class="control-label">&nbsp;</label>
            <?= Html::button($sliderItem->isNewRecord ? Yii::t("app", 'Create') : Yii::t("app", 'Update'), ['class' => 'btn btn-primary', 'id' => 'save-slider-item-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>


<?php $this->registerJs("
    $('#save-slider-item-button').click(function(e){
        $.ajax({
            url: '" . Url::to(['save-item']) . "',
            data: new FormData($('#save-slider-item-form')[0]),
            type: 'post',
            processData: false,
            contentType: false,
            success: function(){
                $('#edit-slider-item-modal').modal('hide');
                $.pjax.reload({container: '#pjax-index'});
            }
        });
    });

") ?>