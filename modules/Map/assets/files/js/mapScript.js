
function initMarkers() {


    $('.draggable').each(function () {
        var width = $("#droppable").width();
        var height = $("#droppable").height();
        var top = ($(this).attr('data-top')/100)*height;
        var left = ($(this).attr('data-left')/100)*width;


        $(this).css({'position':'absolute','top':top +'px','left': left+'px'});

    });
    $('.draggable2').each(function () {
        var width = $("#droppable2").width();
        var height = $("#droppable2").height();
        var top = ($(this).attr('data-top')/100)*height;
        var left = ($(this).attr('data-left')/100)*width;


        $(this).css({'position':'absolute','top':top +'px','left': left+'px'});

    });

    //maps and markers

    //****** marker
    $( ".draggable" ).draggable({
        revert:"invalid",
        stop: function( event, ui ) {
        }
    });

    //******* map
    $( "#droppable" ).droppable({
        classes: {
            "ui-droppable-hover": "ui-state-hover"
        },
        accept:".draggable",
        drop: function( event, ui ) {
            var width = $("#droppable").width();
            var height = $("#droppable").height();
            var top = (ui.position.top/height)*100;
            var left = (ui.position.left/width)*100;
            $.ajax({
                method: "POST",
                url: baseUrl('/admin/map/map-markers/save-marker-position'),
                data: {id : $(ui.helper[0]).attr('data-id'),top : top , left:left},
            })
                .done(function( msg ) {

                });


        },
        out: function( event, ui ) {
            $( this )
                .removeClass( "ui-state-highlight" );
        }

    });

    $( ".draggable2" ).draggable({
        revert:"invalid",

    });
    $( "#droppable2" ).droppable({
        classes: {
            "ui-droppable-hover": "ui-state-hover"
        },
        accept: ".draggable2",
        drop: function( event, ui ) {

            var width = $("#droppable2").width();
            var height = $("#droppable2").height();
            var top = (ui.position.top/height)*100;
            var left = (ui.position.left/width)*100;
            $.ajax({
                method: "POST",
                url: baseUrl('/admin/map/map-markers/save-marker-position'),
                data: {id : $(ui.helper[0]).attr('data-id'),top : top , left:left},
            })
                .done(function( msg ) {
                    //alert( "Data Saved: " + msg );
                });


        },
        out: function( event, ui ) {
            $( this )
                .removeClass( "ui-state-highlight" );
        }

    });
    //end maps and markers
    $( ".remove-block" ).droppable({
        classes: {
            "ui-droppable-hover": "red-back"
        },
        accept:".draggable2,.draggable",
        drop: function( event, ui ) {


            $.ajax({
                method: "get",
                url: baseUrl('/admin/map/map-markers/delete-item'),
                data: {id : $(ui.helper[0]).attr('data-id')},
            })
                .done(function( msg ) {
                    $.pjax.reload({container: '#pjax-index'});
                });


        },


    });


}

$(function () {
    initMarkers();
    $(document).on('click','.remove-item',function(){

        $.get( baseUrl('/admin/map/map-markers/delete-item?id=')+ $(this).attr('data-id')
        ).success( function (data) {
            $.pjax.reload({container: '#pjax-index'});
        });
    });


    $('.draggable').each(function () {
        var width = $("#droppable").width();
        var height = $("#droppable").height();
        var top = ($(this).attr('data-top')/100)*height;
        var left = ($(this).attr('data-left')/100)*width;


        $(this).css({'position':'absolute','top':top +'px','left': left+'px'});

    });
    $('.draggable2').each(function () {
        var width = $("#droppable2").width();
        var height = $("#droppable2").height();
        var top = ($(this).attr('data-top')/100)*height;
        var left = ($(this).attr('data-left')/100)*width;


        $(this).css({'position':'absolute','top':top +'px','left': left+'px'});

    });
});