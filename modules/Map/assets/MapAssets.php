<?php

namespace app\modules\Map\assets;

use yii\web\AssetBundle;



class MapAssets extends AssetBundle
{   public $sourcePath = '@app/modules/Map/assets';

    public $css = [
        'files/css/mapStyle.css'
    ];
    
    public $js = [
        'files/js/mapScript.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\jui\JuiAsset',

    ];
    
}