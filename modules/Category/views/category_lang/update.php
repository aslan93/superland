<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryLang\CategoryLang */

$this->title = Yii::t("app", 'Updating Category Lang').': ' . $model->Title;
$this->params['breadcrumbs'][] = ['label' => Yii::t("app","Category Langs"), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Title, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t("app", 'Updating');
?>
<div class="category-lang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
