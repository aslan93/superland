<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;

?>


<div class="food-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?php
    $items[0] = [
        'label' => 'Descriere',
        'content' => $this->render('_lang_form',[
            'form' => $form,
            'model' => $model,
        ])];

    $initialPreview = [];
    if ($model->Image)
    {
        $initialPreview[] = Html::img($model->image, ['width' => 190]);
    }
    ?>
    <div class="row">
        <div class="col-md-4">
            <label>Image</label>
            <?= \kartik\file\FileInput::widget([
                'name' => 'Image',
                'options' => ['accept' => 'image/.*'],
                'pluginOptions' => [
                    'showRemove' => false,
                    'showUpload' => false,
                    'initialPreview' => $initialPreview,
                ]
            ]) ?>
        </div>
    </div>

    <?php
    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'CategoryID')->dropDownList(\app\modules\FoodCategory\models\FoodCategory::getList()) ?>
        </div>
    </div>
    <br><br><br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
