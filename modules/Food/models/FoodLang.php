<?php

namespace app\modules\Food\models;

use Yii;

/**
 * This is the model class for table "FoodLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property string $LangID
 * @property integer $FoodID
 *
 * @property Food $food
 */
class FoodLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FoodLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'LangID', 'FoodID'], 'required'],
            [['Description'], 'string'],
            [['FoodID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['FoodID'], 'exist', 'skipOnError' => true, 'targetClass' => Food::className(), 'targetAttribute' => ['FoodID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
            'FoodID' => 'Food ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFood()
    {
        return $this->hasOne(Food::className(), ['ID' => 'FoodID']);
    }
}
