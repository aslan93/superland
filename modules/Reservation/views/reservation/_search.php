<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Reservation\models\ReservationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'UserID') ?>

    <?= $form->field($model, 'DateIN') ?>

    <?= $form->field($model, 'DateOUT') ?>

    <?= $form->field($model, 'Adults') ?>

    <?php // echo $form->field($model, 'Childs') ?>

    <?php // echo $form->field($model, 'HotelID') ?>

    <?php // echo $form->field($model, 'CreatedAt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
