<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Reservation\models\Reservation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'UserID')->textInput() ?>

    <?= $form->field($model, 'DateIN')->textInput() ?>

    <?= $form->field($model, 'DateOUT')->textInput() ?>

    <?= $form->field($model, 'Adults')->textInput() ?>

    <?= $form->field($model, 'Childs')->textInput() ?>

    <?= $form->field($model, 'HotelID')->textInput() ?>

    <?= $form->field($model, 'CreatedAt')->textInput() ?>

    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
