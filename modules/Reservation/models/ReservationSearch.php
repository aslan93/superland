<?php

namespace app\modules\Reservation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Reservation\models\Reservation;

/**
 * ReservationSearch represents the model behind the search form about `app\modules\Reservation\models\Reservation`.
 */
class ReservationSearch extends Reservation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'UserID', 'Adults', 'Childs', 'HotelID'], 'integer'],
            [['DateIN', 'DateOUT', 'CreatedAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reservation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'UserID' => $this->UserID,
            'DateIN' => $this->DateIN,
            'DateOUT' => $this->DateOUT,
            'Adults' => $this->Adults,
            'Childs' => $this->Childs,
            'HotelID' => $this->HotelID,
            'CreatedAt' => $this->CreatedAt,
        ]);

        return $dataProvider;
    }
}
