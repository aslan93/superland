<?php

namespace app\modules\Reservation\models;

use Yii;
use app\modules\Hotel\models\Hotel;
use app\models\User\User;

/**
 * This is the model class for table "Reservation".
 *
 * @property integer $ID
 * @property integer $UserID
 * @property string $DateIN
 * @property string $DateOUT
 * @property integer $Adults
 * @property integer $Childs
 * @property integer $HotelID
 * @property string $CreatedAt
 *
 * @property Hotel $hotel
 * @property User $user
 */
class Reservation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Reservation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID', 'Adults', 'Childs', 'HotelID'], 'integer'],
            [['DateIN', 'DateOUT'], 'required'],
            [['DateIN', 'DateOUT', 'CreatedAt'], 'safe'],
            [['HotelID'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['HotelID' => 'ID']],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'DateIN' => 'Date In',
            'DateOUT' => 'Date Out',
            'Adults' => 'Adults',
            'Childs' => 'Childs',
            'HotelID' => 'Hotel ID',
            'CreatedAt' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['ID' => 'HotelID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }
}
