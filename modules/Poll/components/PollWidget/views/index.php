<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;
    
?>

    <?php Pjax::begin(['id'=>'poll-pjax']); ?>

        <?php if($voted){ ?>

            <h4><?= $model->lang->Title ?></h4>

            <?php 
                $totalVotes = 0;        
                foreach ($model->pollAnswers as $votes){ ?>
                <?php 
                    $totalVotes = $totalVotes + $votes->pollVotesCount; 
                    $voteArray[] = ['title'=>$votes->lang->Title, 'votesCount'=>$votes->pollVotesCount];
                ?>
            <?php } ?>

            <?php foreach ($voteArray as $vote){ ?>
                <?= Html::tag('p', $vote['title']) ?> 
                <?= Html::tag('div', '&nbsp;&nbsp;' . $vote['votesCount'] . ' ' . Yii::t("app", 'Votes'), [
                    'class'=>'poll-vote-chart', 'style' => 'width: ' . ($vote['votesCount'] / $totalVotes * 100) . '%'
                ]) ?>        
            <?php } ?>


        <?php } else { ?>
    
            <?php $form = ActiveForm::begin([    
                'id' => 'poll-form',
                'options' => [
                    "method" => "post",                
                ],
                'fieldConfig' => [
                    'template' => "{error}\n{input}",
                    'options' => [
                        'tag' => false,
                    ],                
                ],
            ]); ?>
                <fieldset>
                    
                <h4><?= $model->lang->Title ?></h4>

                <?php $i=1; foreach($model->pollAnswers as $pa){ ?>
                    <?= Html::radio("PollVotes[PollAnswerID]", false, ["required"=>"required", "value"=> $pa->ID, "id" => "poll-answer-".$pa->ID  ]) ?> <label for="poll-answer-<?= $pa->ID ?>"><?= $pa->lang->Title ?></label><br />
                <?php $i++; } ?>

                <br />
                    
                <?= Html::submitButton(Yii::t("app", "OK"), ['class' => 'btn btn-red']) ?>
                
                </fieldset>

            <?php ActiveForm::end(); ?>

        <?php } ?>
            
    <?php Pjax::end(); ?>
                    
<?php $this->registerJs("
    $('#poll-form').on('beforeSubmit', function(e) {
    var form = $(this);
    var formData = form.serialize();
    form.find('fieldset').attr('disabled', true);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        success: function (data) {
            form.attr('disabled', false);
            form.trigger('reset'); 
            $.pjax.reload({container: '#poll-pjax', async:false});
        },
        error: function () {
            //alert('Something went wrong');
        }
    });
}).on('submit', function(e){
    e.preventDefault();    
});
", yii\web\View::POS_READY) ?>
    
    
