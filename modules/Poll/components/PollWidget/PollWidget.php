<?php

namespace app\modules\Poll\components\PollWidget;

use Yii;
use yii\base\Widget;
use app\models\Poll\Poll;
use app\models\PollVotes\PollVotes;


class PollWidget extends Widget
{
    public $success = false;

    public function run()
    {
        $model = Poll::find()->with('lang')->where(["Status"=>"Active"])->orderBy("ID Desc")->limit(1)->one();
        $useIP = Yii::$app->getRequest()->getUserIP();
        $voted = PollVotes::find()->where(["UserIP" => $useIP, "PollID" => $model->ID])->count() > 0 ? true : false;
        
        if(count($model) > 0)
        {          
            if (Yii::$app->request->isPost)
            {
                $votesModel = new PollVotes();

                if ($votesModel->load(Yii::$app->request->post()) && !$voted);
                {
                    $votesModel->PollID = $model->ID;
                    $votesModel->PollAnswerID = Yii::$app->request->post('PollVotes')['PollAnswerID'];
                    $votesModel->UserIP = $useIP;
                    $votesModel->save();

    //                $totalVotes = 0;        
    //                foreach ($model->pollAnswers as $votes){
    //                    $totalVotes = $totalVotes + $votes->pollVotesCount; 
    //                    $voteArray[] = ['title'=>$votes->lang->Title, 'votesCount'=>$votes->pollVotesCount];           
    //                }
    //                
    //                
    //                $response = "<div class='poll-widtget'><h3>".$model->lang->Title."</h3>";
    //                
    //                foreach ($voteArray as $vote){
    //                    $response .= "<p>".$vote['title']."</p>";
    //                    $response .= "<div class='poll-vote-chart' style='background-color: #fff;width: ". ($vote['votesCount'] / $totalVotes * 100) . " %'>".$vote['votesCount']." voturi</div>";
    //                }
    //                
    //                $response .= "</div>";
    //                
    //                return $response;
                }

            }

            return $this->render('index', [
                'model' => $model,
                'voted' => $voted,
            ]);
        
        }
    }
    
}