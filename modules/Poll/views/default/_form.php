<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

$dataProvider = new yii\data\ActiveDataProvider(['query'=> app\models\PollAnswer\PollAnswer::find()->with('pollVotes')->where(['PollID'=>$pollModel->ID])])


/* @var $this yii\web\View */
/* @var $model app\models\Poll\Poll */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poll-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
        
        <?php
            $items = [];
            foreach ($pollLangs as $key => $sil)
            {
                $items[] = [
                    'label' => strtoupper(Yii::$app->params['siteLanguages'][$key]),
                    'content' => $this->render('poll-lang', [
                        'sil' => $sil,
                        'key' => $key,
                        'form' => $form
                    ]),
                    'active' => $key == 0
                ];
            }

            echo Tabs::widget([
               'items' => $items
            ]); ?>
                </div>
            </div>
            <div class="col-md-2"><br /><br />

                <?= $form->field($pollModel, "Status")->dropDownList(['Active'=>'Active', 'Inactive'=>'Inactive']); ?>

            </div>        
        </div>            
        
        
    

    <div class="form-group">
        <?= Html::submitButton($pollModel->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $pollModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    
<?php if($pollModel->isNewRecord != "Create"){ ?>

<hr />

<div>
    <?= Html::button('<i class="fa fa-plus"></i> Add poll item', [
        'class' => 'btn btn-primary edit-poll-item',
        'poll-item-id' => 0
    ]) ?>
</div>

<div>
        
    <?php Pjax::begin(['id'=>'answer-langs-pjax', 'timeout' => 5000 ]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => [
                    'width' => '50px'
                ]
            ],
            [
                'label'  => 'Name',
                'value' => function($model){
                    return $model->lang->Title;
                }
            ],  
            [
                'label'  => 'Votes',
                'value' => function($model){
                    return $model->pollVotesCount;
                }
            ],  
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => [
                    'width' => '140px'
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::button('<i class="fa fa-pencil"></i>', [
                            'class' => 'btn btn-success btn-xs edit-poll-item',
                            'poll-item-id' => $model->ID,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', Url::to(['delete-answer-item', 'id' => $model->ID]), [
                            'class' => 'btn btn-danger btn-xs',
                            'onclick' => "return confirm('Delete?')"
                        ]);
                    }
                ],
                'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>'
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
    
    
</div>



<?php Modal::begin([
    'id' => 'edit-poll-item-modal',
    'header' => 'Create/Edit poll answer',
    'size' => Modal::SIZE_LARGE
]); ?>

<?php Modal::end(); ?>

<?php $this->registerJs("
    $(document).on('click', '.edit-poll-item', function(){
        $('#edit-poll-item-modal .modal-body').empty();
        $.post('" . Url::to(['get-answer']) . "', {id: $(this).attr('poll-item-id'), pollID:".Yii::$app->getRequest()->getQueryParam('id')."}, function(html){
            $('#edit-poll-item-modal .modal-body').html(html);
            $('#edit-poll-item-modal').modal();
        });
    });
") ?>

<?php } ?>

</div>
