<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Poll\PollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Polls');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poll-index">

    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="add">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'Create Poll'), ['create'], ['class' => 'btn btn-primary']) ?>
    </p>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'], 
            [
                'label'  => 'Title',
                'value' => function($model){
                    return $model->lang->Title;
                }
            ],
            'Status',
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => [
                    'width' => '100px'
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => 'Edit'
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, [
                            'title' => 'Delete',
                            'onclick' => "return confirm('Delete?')"
                        ]);
                    }
                ],
                'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>'
            ]
        ],
    ]); ?></div>
