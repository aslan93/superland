<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poll\Poll */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Poll',
]) . $model->lang->Title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Polls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lang->Title, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="poll-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'pollModel' => $model,
        'pollLangs' => $pollLangs,
    ]) ?>

</div>
