<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'save-answer-item-form',
]); ?>

<?php foreach ($answerLangs as $key => $al){ ?>

<?= $form->field($al, "[$key]Title")->textInput()->label("Title ".$al->LangID) ?>

<?= $form->field($al, "[$key]LangID")->hiddenInput(['value' => $al->LangID])->label(false) ?>

<?php } ?>

<?= $form->field($answerItem, "ID")->hiddenInput(['value' => $answerItem->ID])->label(false) ?>
<?= $form->field($answerItem, "PollID")->hiddenInput(['value' => (int)Yii::$app->request->post('pollID') ])->label(false) ?>

    <div>
        <div class="form-group text-right">
            <label style="display: block;" class="control-label">&nbsp;</label>
            <?= Html::button($answerItem->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'id' => 'save-answer-item-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php $this->registerJs("
    $('#save-answer-item-button').click(function(e){        
        $.ajax({
            url: '" . Url::to(['save-answer-item']) . "',
            data: new FormData($('#save-answer-item-form')[0]),
            type: 'post',
            processData: false,
            contentType: false,
            success: function(){            
                $.pjax.reload({container: '#answer-langs-pjax'});
                $('#edit-poll-item-modal').modal('hide');
            }
        });
    });
") ?>