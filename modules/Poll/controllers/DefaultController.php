<?php

namespace app\modules\Poll\controllers;

use Yii;
use app\controllers\BackendController;
use app\models\Poll\Poll;
use app\models\PollLang\PollLang;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use yii\caching\TagDependency;
use \app\models\PollAnswer\PollAnswer;
use \app\models\PollAnswerLang\PollAnswerLang;

/**
 * PollController implements the CRUD actions for Poll model.
 */
class DefaultController extends BackendController
{
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//        ];
//    }

    /**
     * Lists all Poll models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $searchModel = new PollSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => Poll::find()->with('lang')
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poll model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Poll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Poll();
                
        $pollLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $pollLangs[$i] = new PollLang([
                'LangID' => $lang
            ]);
        }
        
        if (Yii::$app->request->isPost)
        {           
            if (Model::loadMultiple($pollLangs, Yii::$app->request->post()) && Model::validateMultiple($pollLangs))
            {
                $model->Status = "Active";
                $model->save();
                
                foreach ($pollLangs as $pmodel)
                {
                    $pmodel->PollID = $model->ID;
                    $pmodel->save();
                }
                
                TagDependency::invalidate(Yii::$app->cache, 'PollWidget');
                
                Yii::$app->session->setFlash('success', 'Poll was saved');
                
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }

        
        return $this->render('create', [
            'model' => $model,
            'pollLangs' => $pollLangs
        ]);
        
    }

    /**
     * Updates an existing Poll model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLangs = PollLang::find()->where(['PollID' => $model->ID])->all();
                
        if (Yii::$app->request->isPost)
        {            
            if (Model::loadMultiple($modelLangs, Yii::$app->request->post()) && Model::validateMultiple($modelLangs))
            {  
                $model->load(Yii::$app->request->post());
                $model->save();
                
                foreach ($modelLangs as $ail)
                {
                    $ail->PollID = $model->ID;
                    $ail->save();
                }
                
                Yii::$app->session->setFlash('success', 'Poll was saved');
                
                return $this->redirect(['update', 'id' => $model->ID]);
            }        
        }
        
        $pollLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $pollLangs[$i] = isset($model->langs[$lang]) ? $model->langs[$lang] : new PollLang([
                'LangID' => $lang,
                'PollID' => $model->ID
            ]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'pollLangs' => $pollLangs
            ]);
        }
    }

    /**
     * Deletes an existing Poll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poll::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetAnswer()
    {
        $id = (int)Yii::$app->request->post('id');
        
        $answerItem = $id > 0 ? PollAnswer::findOne($id) : new PollAnswer();
        
        $answerLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $answerLangs[$i] = isset($answerItem->langs[$lang]) ? $answerItem->langs[$lang] : new PollAnswerLang([
                'LangID' => $lang
            ]);
        }
        
        return $this->renderAjax('answer-item', [
            'answerItem' => $answerItem,
            'answerLangs' => $answerLangs
        ]);
    }
    
    public function actionSaveAnswerItem()
    {
        $id = Yii::$app->request->post('PollAnswer')['ID']; 
        $pollID = Yii::$app->request->post('PollAnswer')['PollID']; 
        
        $answerItem = $id > 0 ? PollAnswer::findOne($id) : new PollAnswer();
        
        $answerLangs = [];
        foreach (Yii::$app->params['siteLanguages'] as $i => $lang)
        {
            $answerLangs[$i] = isset($answerItem->langs[$lang]) ? $answerItem->langs[$lang] : new PollAnswerLang([
                'LangID' => $lang
            ]);
        }
        
        if ($answerItem->load(Yii::$app->request->post()))
        {                        
            $answerItem->PollID = $pollID ? $pollID : 1;            
            $answerItem->save();

            Model::loadMultiple($answerLangs, Yii::$app->request->post());

            foreach ($answerLangs as $ail)
            {
                $ail->PollAnswerID = $answerItem->ID;
                $ail->save();
            }
        }
    }
    
    public function actionDeleteAnswerItem($id)
    {
        $model = PollAnswer::findOne($id);
        
        PollAnswer::deleteAll(['ID' => $id]);
        
        $this->redirect(['/admin/poll/default/update', 'id' => $model->PollID]);
    }
}
