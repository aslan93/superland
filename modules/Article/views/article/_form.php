<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;
use app\models\Article\Article;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
?>

<br />
<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 <?= $model->Type == 'Page' ? 'hidden' : '' ?>">
            <?= $form->field($model, 'Date')->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['value' => date('d.m.Y', empty($model->Date) ? time() : strtotime($model->Date))],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Status')->widget(Select2::className(), [
                'data' => Article::getStatusList()
            ]) ?>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label"><?=Yii::t("app", 'Parents')?></label>
                <?= Select2::widget([
                    'name' => 'parents[]',
                    'data' => ArrayHelper::map($all_articles, 'ID', 'lang.Title'),
                    'value' => ArrayHelper::map($related_articles, 'ParentArticleID', 'ParentArticleID'),
                    'size' => Select2::LARGE,
                    'options' => [
                        'placeholder' => Yii::t("app", 'Select parents'),
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <?php foreach ($langModels as $key => $lmodel) { ?>
        <?php $items[] = [
            'label' => strtoupper(Yii::$app->params['siteLanguages'][$key]),
            'content' => $this->render('_lang_form', [
                'lmodel' => $lmodel,
                'form' => $form,
                'key' => $key
            ]),
            'active' => $key == 0
        ]; ?>
    <?php } ?>
    
    <?= Tabs::widget([
        'items' => $items
    ]) ?>

    <div class="form-group grup-button">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Create') : Yii::t("app", 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>

<!--        <label>-->
<!--        --><?//= Html::dropDownList('Redirect', NULL, [
//            'list' => Yii::t("app", 'Go to list'),
//            'remain' => Yii::t("app",'Remain')
//        ], [
//            'class' => 'form-control',
//            'style' => 'width: 150px; display: inline-block;'
//        ]) ?>
<!--        </label>-->
    </div>
    <?php ActiveForm::end(); ?>
</div>
