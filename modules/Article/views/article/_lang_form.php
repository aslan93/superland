<?php

    use app\core\widgets\TinyMce;

?>

<br />
<div class="col-md-6">
    <?= $form->field($lmodel, "[$key]Title")->textInput(['maxlength' => true]) ?>
</div>

<div class="col-md-12">
    <?= $form->field($lmodel, "[$key]Text")->widget(TinyMce::className(), [
        'language' => Yii::$app->language
    ]) ?>
</div>

<div class="col-md-12">
    <?= $form->field($lmodel, "[$key]SeoTitle")->textInput(['maxlength' => true]) ?>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($lmodel, "[$key]Keywords")->textarea() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($lmodel, "[$key]Description")->textarea() ?>
    </div>
</div>