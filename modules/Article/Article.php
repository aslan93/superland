<?php

namespace app\modules\Article;

use app\core\CoreModule;
use Yii;

/**
 * Article module definition class
 */
class Article extends CoreModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Article\controllers';
    
    public $name = 'Article';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    
    public function articleTypes()
    {
        return [
            'Page' => [
                'labels' => [
                    'icon' => 'fa fa-file-text-o', // fa or glyphicon full icon name
                    'singular' => Yii::t("app", 'Page'),
                    'plural' => Yii::t("app", 'Pages'),
                    'add' => Yii::t("app", 'Add new page'),
                    'wasSaved' => Yii::t("app", 'Page was saved'),
                ],
                'hierarchy' => false,
                'hasChronology' => false,
                'gridViewColumns' => [
                    'lang.Title',
                    [
                        'label'  => Yii::t("app", 'Text'),
                        'value' => function($article) {
                            return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
                        }
                    ],
                    'Status'
                ],
                'hasImages' => true,
                'hasFiles' => true,
            ],
            'News' => [
                'labels' => [
                    'icon' => 'fa fa-newspaper-o', // fa or glyphicon full icon name
                    'singular' => Yii::t("app", 'News article'),
                    'plural' => Yii::t("app", 'News articles'),
                    'add' => Yii::t("app", 'Add News article'),
                    'wasSaved' => Yii::t("app", 'News article was saved'),
                ],
                'hierarchy' => false,
                'hasChronology' => true,
                'gridViewColumns' => [
                    'lang.Title',
                    [
                        'label'  => Yii::t("app", 'Text'),
                        'value' => function($article) {
                            return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
                        }
                    ],
                    [
                        'label'  => Yii::t("app", 'Date'),
                        'value' => function($article) {
                            return date('d.m.Y', strtotime($article->Date));
                        }
                    ],
                    [
                        'attribute' => 'Status',
                    ]
                ],
                'hasImages' => true,
                'hasFiles' => true,
            ],
            'Event' => [
                'labels' => [
                    'icon' => 'fa fa-calendar-o',
                    'singular' => Yii::t("app", 'Event'),
                    'plural' => Yii::t("app", 'Events'),
                    'add' => Yii::t("app", 'Add event'),
                    'wasSaved' => Yii::t("app", 'Event was saved'),
                ],
                'hierarchy' => false,
                'hasChronology' => false,
                'gridViewColumns' => [
                    'lang.Title',
                    [
                        'label'  => Yii::t("app", 'Text'),
                        'value' => function($article) {
                            return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
                        }
                    ],
                    'Status'
                ],
                'hasImages' => true,
                'hasFiles' => true,
            ],
        ];
    }
    
    public function getArticleConfig($articleType)
    {
        $types = $this->articleTypes();
        
        if (!isset($types[$articleType]))
        {
            throw new \yii\base\InvalidConfigException("Unknown article type. Please verify configuration in " . __CLASS__ . "::articleTypes().");
        }
        
        return $types[$articleType];
    }
    
}
