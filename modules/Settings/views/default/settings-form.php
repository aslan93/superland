<?php

    use yii\helpers\Html;
    use yii\bootstrap\Tabs;

?>

<h1><?=Yii::t("app", 'Settings')?></h1>

<?php //yii\widgets\Pjax::begin() ?>

<?= Html::beginForm('', 'post', [
    'enctype' => 'multipart/form-data',
    'id' => 'settings-form',
    'data-pjax' => ''
]) ?>

    <?= Tabs::widget([
        'items' => $settingsTabs
    ]) ?>

    <div class="text-center">
        <?= Html::submitButton(Yii::t("app", 'Update settings'), [
            'class' => 'btn btn-primary'
        ]) ?>
    </div>

<?= Html::endForm() ?>

<?php //yii\widgets\Pjax::end() ?>