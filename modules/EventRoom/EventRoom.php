<?php

namespace app\modules\EventRoom;

/**
 * EventRoom module definition class
 */
class EventRoom extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\EventRoom\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
