<?php

namespace app\modules\EventRoom\models;

use Yii;

/**
 * This is the model class for table "EventRoomLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property string $LangID
 * @property integer $EventRoomID
 */
class EventRoomLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EventRoomLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'LangID', 'EventRoomID'], 'required'],
            [['Description'], 'string'],
            [['EventRoomID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
            'EventRoomID' => 'Event Room ID',
        ];
    }
}
