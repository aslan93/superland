<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\EventRoom\models\EventRoom */

$this->title = 'Create Event Room';
$this->params['breadcrumbs'][] = ['label' => 'Event Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-room-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
