<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\EventRoom\models\EventRoom */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-room-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?php
    $initialPreview = [];
    if ($model->Image)
    {
        $initialPreview[] = Html::img($model->image, ['width' => 190]);
    }
    ?>
    <div class="row">
        <div class="col-md-6">
            <label>Image</label>
            <?= \kartik\file\FileInput::widget([
                'name' => 'Logo',
                'options' => ['accept' => 'image/.*'],
                'pluginOptions' => [
                    'showRemove' => false,
                    'showUpload' => false,
                    'initialPreview' => $initialPreview,
                ]
            ]) ?>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'Price')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Type')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Capacity')->textInput() ?>
        </div>
    </div>

    <?php
    $items[0] = [
        'label' => 'Descriere',
        'content' => $this->render('_lang_form',[
            'form' => $form,
            'model' => $model,
        ])];

    echo '<br>';
    echo \yii\bootstrap\Tabs::widget([
        'items' => $items,
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>






</div>
