<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\EventRoom\models\EventRoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Rooms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-room-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Room', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID',
            [
                'label'=>'Image',
                'format'=>'raw',
                'value'=>function($model){
                    return Html::img($model->image,['width'=>200]);
                }
            ],
            'lang.Name',
            'Price',
            'Type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
