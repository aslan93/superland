<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\BuyTickets\models\BuyTickets */

$this->title = 'Update Buy Tickets: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Buy Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="buy-tickets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
