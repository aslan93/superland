<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\BuyTickets\models\BuyTickets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buy-tickets-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-md-6">

        <div class="col-md-12">
            <h2>User Info</h2>
        </div>
        <?= $form->field($model, 'UserID')->textInput() ?>

        <?= $form->field($model, 'FullName')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'UserName')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'Address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'CardID')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'Age')->textInput() ?>

        <?= $form->field($model, 'Password')->passwordInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <h2>Ticket Info</h2>

            <?= $form->field($model, 'PayMethod')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Total')->textInput() ?>

            <?= $form->field($model, 'Status')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ParTicketID')->textInput() ?>

            <?= $form->field($model, 'KidTicketID')->textInput() ?>
        </div>
    </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
