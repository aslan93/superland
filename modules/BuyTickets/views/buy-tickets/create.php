<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\BuyTickets\models\BuyTickets */

$this->title = 'Create Buy Tickets';
$this->params['breadcrumbs'][] = ['label' => 'Buy Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buy-tickets-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
