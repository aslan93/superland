<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\BuyTickets\models\BuyTicketsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buy Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buy-tickets-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--         Html::a('Create Buy Tickets', ['create'], ['class' => 'btn btn-success'])
<!--    </p>-->
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID',
//            'FullName',
        [
            'label' => 'Tickets',
            'format' =>'raw',
            'value' => function($model){
                $ids = [];
                $ids[] = $model->ParTicketID;
                $ids[] = $model->KidTicketID;
                $tickets = \app\modules\Tickets\models\Tickets::find()->where(['ID'=>$ids])->all();
                $tick ='';
                $total = 0;
                foreach ($tickets as $index => $ticket){
                    $tick = $tick.$ticket->lang->Name.' - '.$ticket->Price.'Lei'.'<br>';
                    $total = $total + $ticket->Price;
                }
                $tick = $tick .'<b>Total: '. $total.'</b><br>';
                return $tick;
            }

        ],

            'UserName',
            'Email:email',
            'Address',
            // 'CardID',
            // 'Age',
            // 'Password',
            // 'PayMethod',
            // 'Total',
            // 'Status',
            // 'UserID',
            // 'TicketsID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
