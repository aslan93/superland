<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\BuyTickets\models\BuyTicketsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buy-tickets-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'FullName') ?>

    <?= $form->field($model, 'UserName') ?>

    <?= $form->field($model, 'Email') ?>

    <?= $form->field($model, 'Address') ?>

    <?php // echo $form->field($model, 'CardID') ?>

    <?php // echo $form->field($model, 'Age') ?>

    <?php // echo $form->field($model, 'Password') ?>

    <?php // echo $form->field($model, 'PayMethod') ?>

    <?php // echo $form->field($model, 'Total') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <?php // echo $form->field($model, 'UserID') ?>

    <?php // echo $form->field($model, 'TicketsID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
