<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\Tickets\models\Tickets;
/* @var $this yii\web\View */
/* @var $model app\modules\BuyTickets\models\BuyTickets */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Buy Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buy-tickets-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="col-md-6">
        <div><h2>User INFO</h2></div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'FullName',
            'Age',
            'UserName',
            'Email:email',
            'Address',
            'CardID',


        ],
    ]) ?>
    </div>
    <div class="col-md-6">
        <div><h2>Tickets INFO</h2></div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                    [
                        'label' => 'Parent Ticket:',
                        'format' => 'raw',
                        'value' => function ($model){
                                $tik = Tickets::find()->where(['ID'=>$model->ParTicketID])->with('lang')->one();
                            return $tik->lang->Name . '  :  '.$tik->Price.' Lei';
                        }
                    ],
                [
                    'label' => 'Kid Ticket:',
                    'format' => 'raw',
                    'value' => function ($model){
                        $tik = Tickets::find()->where(['ID'=>$model->KidTicketID])->with('lang')->one();
                        return $tik->lang->Name . '  :  '.$tik->Price.' Lei';
                    }
                ],
                'Total',
                'Status',
                'PayMethod',
            ],
        ]) ?>
    </div>

</div>
