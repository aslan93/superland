<?php

namespace app\modules\BuyTickets;

/**
 * BuyTickets module definition class
 */
class BuyTickets extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\BuyTickets\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
