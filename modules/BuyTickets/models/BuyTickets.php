<?php

namespace app\modules\BuyTickets\models;

use app\modules\Tickets\models\Tickets;
use app\models\User\User;
use Yii;

/**
 * This is the model class for table "BuyTickets".
 *
 * @property integer $ID
 * @property string $FullName
 * @property string $UserName
 * @property string $Email
 * @property string $Address
 * @property string $CardID
 * @property integer $Age
 * @property string $Password
 * @property string $PayMethod
 * @property integer $Total
 * @property string $Status
 * @property integer $UserID

 *
 * @property Tickets $tickets
 * @property User $user
 */
class BuyTickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BuyTickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FullName', 'UserName', 'Email', 'Address', 'CardID', 'Age', 'Password', 'PayMethod', 'Total', 'Status', 'UserID', 'KidTicketID','ParTicketID'], 'safe'],
            [['Total', 'UserID'], 'integer'],
            [['FullName', 'UserName', 'Email', 'Address', 'CardID', 'Password', 'PayMethod', 'Status'], 'string', 'max' => 255],
//            [['TicketsID'], 'exist', 'skipOnError' => true, 'targetClass' => Tickets::className(), 'targetAttribute' => ['TicketsID' => 'ID']],
//            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FullName' => 'Full Name',
            'UserName' => 'User Name',
            'Email' => 'Email',
            'Address' => 'Address',
            'CardID' => 'Card ID',
            'Age' => 'Age',
            'Password' => 'Password',
            'PayMethod' => 'Pay Method',
            'Total' => 'Total',
            'Status' => 'Status',
            'UserID' => 'User ID',
            'ParTicketID' => 'Parent Ticket ID',
            'KidTicketID' => 'Kid Ticket ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParTickets()
    {
        return $this->hasOne(Tickets::className(), ['ID' => 'ParTicketID']);
    }


    public function getKidTickets()
    {
        return $this->hasOne(Tickets::className(), ['ID' => 'KidTicketID']);
    }

    public function getTickets()
    {   $tids = [];
        $tids[] = $this->KidTicketID;
        $tids[] = $this->ParTicketID;
        return Tickets::find()->where(['ID' => $tids])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }
}
