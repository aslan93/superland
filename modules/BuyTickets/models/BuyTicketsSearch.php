<?php

namespace app\modules\BuyTickets\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\BuyTickets\models\BuyTickets;

/**
 * BuyTicketsSearch represents the model behind the search form about `app\modules\BuyTickets\models\BuyTickets`.
 */
class BuyTicketsSearch extends BuyTickets
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Age', 'Total', 'UserID'], 'integer'],
            [['FullName', 'UserName', 'Email', 'Address', 'CardID', 'Password', 'PayMethod', 'Status','KidTicketID','ParTicketID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuyTickets::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Age' => $this->Age,
            'Total' => $this->Total,
            'UserID' => $this->UserID,
            'ParTicketID' => $this->ParTicketID,
            'KidTicketID' => $this->KidTicketID,
        ]);

        $query->andFilterWhere(['like', 'FullName', $this->FullName])
            ->andFilterWhere(['like', 'UserName', $this->UserName])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Address', $this->Address])
            ->andFilterWhere(['like', 'CardID', $this->CardID])
            ->andFilterWhere(['like', 'Password', $this->Password])
            ->andFilterWhere(['like', 'PayMethod', $this->PayMethod])
            ->andFilterWhere(['like', 'ParTicketID', $this->ParTicketID])
            ->andFilterWhere(['like', 'KidTicketID', $this->KidTicketID])
            ->andFilterWhere(['like', 'Status', $this->Status]);

        return $dataProvider;
    }
}
