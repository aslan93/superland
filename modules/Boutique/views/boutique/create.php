<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Boutique\models\Boutique */

$this->title = 'Create Boutique';
$this->params['breadcrumbs'][] = ['label' => 'Boutiques', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boutique-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
