<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Boutique\models\Boutique */

$this->title = 'Update Boutique: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Boutiques', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="boutique-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
