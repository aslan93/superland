<?php

namespace app\modules\Boutique\models;

use Yii;

/**
 * This is the model class for table "BoutiqueLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property string $LangID
 * @property integer $BoutiqueID
 *
 * @property Boutique $boutique
 */
class BoutiqueLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BoutiqueLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'LangID', 'BoutiqueID'], 'required'],
            [['Description'], 'string'],
            [['BoutiqueID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['BoutiqueID'], 'exist', 'skipOnError' => true, 'targetClass' => Boutique::className(), 'targetAttribute' => ['BoutiqueID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
            'BoutiqueID' => 'Boutique ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoutique()
    {
        return $this->hasOne(Boutique::className(), ['ID' => 'BoutiqueID']);
    }
}
