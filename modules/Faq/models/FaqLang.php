<?php

namespace app\modules\Faq\models;

use Yii;

/**
 * This is the model class for table "FaqLang".
 *
 * @property integer $ID
 * @property integer $FaqID
 * @property string $Name
 * @property string $Description
 * @property string $LangID
 *
 * @property Faq $faq
 */
class FaqLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FaqLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FaqID', 'Name', 'Description', 'LangID'], 'required'],
            [['FaqID'], 'integer'],
            [['Description'], 'string'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['FaqID'], 'exist', 'skipOnError' => true, 'targetClass' => Faq::className(), 'targetAttribute' => ['FaqID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FaqID' => 'Faq ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaq()
    {
        return $this->hasOne(Faq::className(), ['ID' => 'FaqID']);
    }
}
