<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Gallery\Gallery;

?>

<?php

Pjax::begin ( [ 
		'id' => 'gallery-image-form-pjax',
		'enablePushState' => false 
] )?>

    <?=Html::beginForm ( Url::to ( [ '/admin/gallery/gallery/upload-images' ] ), 'post', [ 'id' => 'gallery-image-form','data-pjax' => '' ] )?>
<div class="row">
            <div class="col-md-4">
		<div class="form-group">
                    <?=Html::fileInput ( 'Images[]', '', [ 'class' => 'form-control', 'multiple' => true ] )?>
                </div>
	</div>

            <div class="col-md-4">
		<div class="form-group">
                    <?=Html::textInput ( 'Video', '', [ 'class' => 'form-control', 'placeholder' => 'Youtube link' ] )?>
                </div>
	</div>

            <div class="col-md-2">
		<div class="form-group">
                    <?= Html::hiddenInput('GalleryID', $model->ID) ?>
                    <?=Html::submitButton ( Yii::t ( 'app', 'Trimite' ), [ 'class' => 'btn btn-primary' ] )?>
                </div>
	</div>
</div>
<?= Html::endForm() ?>

<?php Pjax::end(); ?>

<?php

$this->registerJs ( '
    $(document).on("pjax:success", "#gallery-image-form-pjax",  function(event){
        $.pjax.reload({container: "#gallery-item-list"});
    });
' )?>