<?php
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use app\models\Gallery\Gallery;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-md-6">
    <?=$form->field ( $model, 'EventID' )->dropDownList ( ArrayHelper::merge ( [ '0' => 'Select' ], \app\modules\Events\models\Events::getList()), [ 'class' => 'form-control' ] )->label ( 'Eveniment' )?>
    </div>
</div>

<?=$form->field ( $model, 'ParentID' )->dropDownList ( ArrayHelper::merge ( [ '0' => '-' ], ArrayHelper::map ( Gallery::find ()->with ( 'lang' )->all (), 'ID', 'lang.Title' ) ), [ 'class' => 'hidden' ] )->label ( false )?>

    <?=$form->field ( $model, 'Position' )->hiddenInput ( [ 'value' => 0 ] )->label ( false )?>

    <?=$form->field ( $model, 'Type' )->hiddenInput ( [ 'value' => empty ( $model->Type ) ? Yii::$app->request->get ( 'type', Gallery::TypeDefault ) : $model->Type ] )->label ( false )?>

    <?php foreach ($modelLangs as $key => $lmodel) { ?>
        <?php
					
    $items [] = [
							'label' => strtoupper ( Yii::$app->params ['siteLanguages'] [$key] ),
							'content' => $this->render ( '_lang_form', [ 
									'lmodel' => $lmodel,
									'form' => $form,
									'key' => $key 
							] ),
							'active' => $key == 0 
					];
					?>
    <?php } ?>

    <?=Tabs::widget ( [ 'items' => $items ] )?>

<div class="form-group group-button" style="text-align: center">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Creează') : Yii::t('app', 'Editează'), ['class' => $model->isNewRecord ? 'submit btn-primary' : 'submit btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>