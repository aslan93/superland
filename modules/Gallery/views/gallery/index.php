<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Gallery\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( 'app', 'Galerii' );
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="gallery-index">

	<h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="add">
        <?= Html::a(Yii::t('app', 'Creează galerie'), $this->context->url(['create']), ['class' => 'btn btn-primary']) ?>
    </p>
<?php Pjax::begin(); ?>    
    <?=GridView::widget ( [ 
        'dataProvider' => $dataProvider, 
        // 'filterModel' => $searchModel,
        'columns' => [ [ 'class' => 'yii\grid\SerialColumn','options' => [ 'width' => '50' ] ],
            [ 'attribute' => 'lang.Title' ],
            [ 'label' => Html::tag ( 'div', Yii::t ( 'app', 'Gallery items' ),
                [ 'class' => 'text-center' ] ),
                'value' => function ($model) {return Html::tag ( 'div', $model->countItems,
                    [ 'class' => 'text-center' ] );},
                'options' => [ 'width' => '100' ],
                'encodeLabel' => false,'format' => 'raw' ],
            [ 'class' => 'yii\grid\ActionColumn',
                'template' => '{edit}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
                'options' => [ 'width' => '150' ],
                'buttons' => [
                        'edit' => function ($url, $model) {
                    return '<a data-pjax="0" href="' . yii\helpers\Url::to ( [
                            'update',
                            'id' => $model->ID,
                            'type' => Yii::$app->request->get ( 'type' ) ] ) . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    },
//                        'delete' => function ($url, $model) {return '<a data-pjax="0" href="' . yii\helpers\Url::to ( [ 'delete','id' => $model->ID,
//                            'type' => Yii::$app->request->get ( 'type' ) ] ) . '"><span class="glyphicon glyphicon-trash text-danger"></span></a>';
//                    }
                    'delete' => function ($url, $model) {


                        $url = $this->context->url(['/admin/gallery/gallery/delete', 'id' => $model->ID], Yii::$app->request->get('type', 'Page'));
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, [
                            'title' => 'Delete',
                            'data-pjax' => 0,
                            'onclick' => "return confirm('Delete?')"
                        ]);
                    }
                            ]
            ]
        ]
    ] );?>
<?php Pjax::end(); ?></div>
