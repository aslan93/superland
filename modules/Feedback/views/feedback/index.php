<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Feedback\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'Name',
                'Phone',
                'Email:email',
                [
                    'attribute' => 'Date',
                    'value' => function($model){
                        return date('d.m.Y H:i', strtotime($model->Date));
                    },
                ],
                [
                    'attribute'=>'File',
                    'label'=>'Fisier/CV',
                    'format'=>'raw',
                    'value'=>function($model){
                        if ($model->File) {
                            return '<a href="' . \yii\helpers\Url::to(['/admin/feedback/feedback/download-file','id'=>$model->ID]) .'" target="_blank" data-pjax="0" style="text-decoration: underline">Download file</a>';
                        }else{
                            return 'Lipseste';
                        }
                    }

                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}&nbsp;&nbsp;&nbsp;&nbsp;{delete}'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>