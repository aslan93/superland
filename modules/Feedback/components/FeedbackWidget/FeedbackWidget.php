<?php

namespace app\modules\Feedback\components\FeedbackWidget;

use app\components\UploadedFile;
use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use app\models\Feedback\Feedback;


class FeedbackWidget extends Widget
{
    
    public $success = false;

    public function run()
    {
        $model = new Feedback();
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {   $files = UploadedFile::getInstanceByName('Feedback[File]');
                if ($files) {
                    $file = md5(microtime(true)) . '.' . $files->extension;

                    if ($files->saveAs(Yii::getAlias("@webroot/uploads/feedback/$file"))) {
                        $model->File = $file;
                    }
                }
                $model->save();
                Yii::$app->session->setFlash('feedbackSend', true);
            }
        }
        
        return $this->render('index', [
            'model' => $model
        ]);
    }
    
}