<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;
    use app\assets\SuperFrontAssets;

    $bundle = SuperFrontAssets::register($this);
?>


<?php Pjax::begin() ?>
    
    <?php if (Yii::$app->session->getFlash('feedbackSend', false)) { ?>
        <?= Html::tag('h2', Yii::t('app', 'Mesajul dvs. a fost trimis cu success, in scurt timp echipa Superland va lua legatura cu dumnevoastra!'), [
            'class' => 'bold text-center'
        ]) ?>
    <?php } else { ?>

        <?php $form = ActiveForm::begin([
            'id' => 'feedback-form',
            'options' => [
                'data-pjax' => ''
            ]
        ]); ?>

            <div class="row">
                <div class="col-md-3">
                    <div>
                        <label>
                            <?=$form->field($model,'Subiect')->input('text',['placeholder' =>'Subiect','class'=>'form-control'])->label(false)?>
                        </label>
                    </div>
                    <div>
                        <label class="control-label">
                            <?= $form->field($model, 'Name')->textInput(['placeholder' =>'Nume / Prenume','class'=>'form-control'])->label(false); ?>
                        </label>
                    </div>
                    <div>
                        <label class="control-label">
                            <?= $form->field($model, 'Email')->textInput(['placeholder' =>'Email','class'=>'form-control'])->label(false); ?>
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="upload-file text-center" style="background-color: #fff">
                        <?= $form->field($model, 'File')->fileInput([
                            'class' => 'btn',
                            'id'=>'input-file',
                            'style'=> 'color: transparent;
                                width: 130px;
                                height: 100px;
                                position: absolute;
                                margin-left: -33px;
                                margin-top: -10px;
                                background: transparent;
                                display:none;'

                        ])->label(false); ?>

                        <label for="input-file" class="btn" style="color: #d2d2d2;">

                            <span id="file-name">Upload files </span> <br/>
                            <img src="<?=$bundle->baseUrl?>/images/upload.png" alt="">

                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>
                        <?= $form->field($model, 'Message')->textarea([
                            'class'=>'',
                            'placeholder' =>'Mesajul DVS.',
                        ])->label(false); ?>
                    </label>
                </div>
            </div>
            <div class="text-center">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Send') : Yii::t("app", 'Update'), ['class' => $model->isNewRecord ? 'btn btn-default submit-costume text-right' : 'btn btn-default submit-costume text-right']) ?>
            </div>

        <?php ActiveForm::end(); ?>
        <?php
$this->registerJS("

    $(document).on('change','#input-file',function(){
            if($(this).val()){
                $('#file-name').text($(this).val().replace('fakepath', '...'));
            }
    });
   
    
",\yii\web\View::POS_LOAD);
        ?>
    <?php } ?>
    
<?php Pjax::end() ?>
    



