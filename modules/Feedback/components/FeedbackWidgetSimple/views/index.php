<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;
    use app\assets\SuperFrontAssets;

    $bundle = SuperFrontAssets::register($this);
?>


<?php Pjax::begin() ?>
    
    <?php if (Yii::$app->session->getFlash('feedbackSend', false)) { ?>
        <?= Html::tag('div', Yii::t('app', 'Abonat cu success!'), [
            'class' => 'bold'
        ]) ?>
    <?php } else { ?>
    
        <?php $form = ActiveForm::begin([
            'id' => 'feedback-form2',
            'options' => [
                'data-pjax' => ''
            ]
        ]);
        ?>

            <label>
                <?= $form->field($model, 'Email')->input('email',['placeholder' =>'Email','class'=>''])->label(false); ?>
                <?= Html::submitButton("<img src='".$bundle->baseUrl."/images/right-arrow.png' >", ['class' => 'btn costume ']) ?>
            </label>

        <?php ActiveForm::end(); ?>
    
    <?php } ?>
    
<?php Pjax::end() ?>
    



