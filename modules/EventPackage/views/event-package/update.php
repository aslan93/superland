<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\EventPackage\models\EventPackage */

$this->title = 'Update Event Package: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Event Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-package-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
