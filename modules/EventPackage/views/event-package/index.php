<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\EventPackage\models\EventPackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-package-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Package', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID',
            'lang.Name',
            'Price',
            [
                'label'=>'Caracteristics',
                'format'=>'raw',
                'value' => function ($model){
                    $m = '';
                        foreach ($model->packageCaracteristics as $item){
                            $m = $m .$item ;
                        }
                    return $m;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
