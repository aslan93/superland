<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\EventPackage\models\EventPackage */

$this->title = 'Create Event Package';
$this->params['breadcrumbs'][] = ['label' => 'Event Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
