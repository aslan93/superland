<?php
use yii\bootstrap\Tabs;
use kartik\datetime\DateTimePicker;
?>

            <?php
            $items = [];
            foreach ($model->langs as $langID => $langModel)
            {
                $items[] = [
                    'label' => strtoupper($langID),
                    'content' => $this->render('_desc_form',[
                        'form' => $form,
                        'langModel' => $langModel,
                        'model' => $model,
                    ]),
                ];
            }

            echo '<br>';
            echo Tabs::widget([
                'items' => $items,
            ]);
            ?>

