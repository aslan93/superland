<?php

namespace app\modules\EventPackage\models;

use Yii;

/**
 * This is the model class for table "EventPackageLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Caracteristics
 * @property string $Description
 * @property string $LangID
 * @property integer $EventPackageID
 *
 * @property EventPackage $eventPackage
 */
class EventPackageLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EventPackageLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Caracteristics', 'Description', 'LangID', 'EventPackageID'], 'required'],
            [['Caracteristics', 'Description'], 'string'],
            [['EventPackageID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['EventPackageID'], 'exist', 'skipOnError' => true, 'targetClass' => EventPackage::className(), 'targetAttribute' => ['EventPackageID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Caracteristics' => 'Caracteristics',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
            'EventPackageID' => 'Event Package ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventPackage()
    {
        return $this->hasOne(EventPackage::className(), ['ID' => 'EventPackageID']);
    }
}
