<?php

namespace app\modules\EventPackage;

/**
 * EventPackage module definition class
 */
class EventPackage extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\EventPackage\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
