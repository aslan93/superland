<?php

namespace app\modules\Games\controllers;

use yii\web\Controller;
use app\controllers\BackendController;

/**
 * Default controller for the `Games` module
 */
class DefaultController extends BackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
