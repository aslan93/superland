<?php

namespace app\modules\Games\controllers;

use Yii;
use app\models\Games\Games;
use app\models\Games\GamesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\BackendController;
use yii\web\UploadedFile;
use yii\imagine\Image;
use app\models\Games\GamesImage;
/**
 * GamesController implements the CRUD actions for Games model.
 */
class GamesController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Games models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GamesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Games model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Games model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Games();

        if ($model->load(Yii::$app->request->post())) {
            $model->Logo = $this->saveLogo($model);
            $model->save();
            $this->saveImage($model);
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Games model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if (UploadedFile::getInstanceByName('Logo')) {
                $model->Logo = $this->saveLogo($model);
            }
            $model->save();
            $this->saveImage($model);
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Games model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Games model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Games the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Games::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $images = UploadedFile::getInstancesByName('GamesImages');

        if($images){

            foreach ($images as $image)
            {
                $file = md5(microtime(true)) . '.' . $image->extension;
                $thumb = 'thumb_' . $file;

                if ($image->saveAs(Yii::getAlias("@webroot/uploads/games/$file")))
                {   Image::thumbnail(Yii::getAlias("@webroot/uploads/games/$file"), 200, 200)->save(Yii::getAlias("@webroot/uploads/games/$thumb"));

                    $imageModel = new GamesImage([
                        'GameID' => $model->ID,
                        'Thumb' => $thumb,
                        'Image' => $file,
                        'IsMain' => 1,
                    ]);
                    $imageModel->save();
                }
            }
        }
    }
    public function saveLogo($model){

        $images = UploadedFile::getInstanceByName('Logo');

        if($images){
            $fileName = md5(microtime(true)) . '.' . $images->extension;
            if ($images->saveAs(Yii::getAlias('@webroot/uploads/games/' . $fileName)))
            {
                return $fileName;
            }
        }else{
            $model->Logo;
        }
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        GamesImage::findOne($key)->delete();
        return '{}';
    }
    public function actionSetMainImage(){
        $key = Yii::$app->request->post('id');
        $imgModel = GamesImage::findOne($key);
        GamesImage::updateAll(['IsMain'=> 0],['GameID'=>$imgModel->GameID]);
        $imgModel->IsMain = 1;
        $rs = $imgModel->save();

        return "{}";
    }
    public function actionGetGame($id){
        $game = Games::find()->where(['ID' => $id])->with('lang')->one();
        return $this->renderPartial('game', [
            'game' => $game,
        ]);
    }
}
