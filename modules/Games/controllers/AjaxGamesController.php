<?php
/**
 * Created by PhpStorm.
 * User: dennix
 * Date: 05.09.17
 * Time: 14:21
 */

namespace app\modules\Games\controllers;

use app\controllers\FrontController;
use app\models\Games\Games;

class AjaxGamesController extends FrontController
{
    public function actionGetGame($id){
        $game = Games::find()->where(['ID' => $id])->with('lang')->one();
        return $this->renderPartial('game', [
            'game' => $game,
        ]);
    }
}