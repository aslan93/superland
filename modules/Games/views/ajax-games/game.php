<?php
use app\assets\SuperFrontAssets;

$bundle = SuperFrontAssets::register($this);
?>


<div class="ajax-games">
    <div class="img-box">
        <div class="title">
            <?=$game->lang->Title?>
        </div>

        <?php
        if ($game->mainImage){


        ?>
        <?=\yii\bootstrap\Html::img($game->mainImage->imagePath,['class' => 'img-responsive','width' => 500])?>
        <?php
        }else{
            ?>
            <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/ajax-games-img.png" alt="">
        <?php
        }
        ?>
    </div>
    <div class="description-ajax-games">
        <a class="close-ajax-post close-games-post" href="#">
            <span class="fa fa-close"></span>
        </a>
        <div>
            <?=$game->lang->Description?>
        </div>
        <div class="row">
            <?php
            foreach ($game->images as $image) {
                ?>
                <div class="col-md-4">
                    <div class="img-box">
                        <?php
                        if ($image->imagePath){

                            ?>
                            <?=\yii\bootstrap\Html::img($image->imagePath,['class' => 'img-responsive','width' => 500])?>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</div>