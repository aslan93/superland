<?php
 use app\modules\Category\models\Category;
 use kartik\datetime\DateTimePicker;
 use yii\bootstrap\Tabs;
?>

<?php
$items = [];
foreach ($model->langs as $key => $langModel)
{
    $items[] = [

        'label' => strtoupper($key),
        'content' => $this->render('_desc_form',[
            'form' => $form,
            'langModel' => $langModel,
            'model' => $model,
        ]),
    ];
}
echo '<br>';
echo Tabs::widget([
    'items' => $items,
]);
?>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'AgeFrom')->input('number')?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'AgeTo')->input('number')?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'KGFrom')->input('number')?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'KGTo')->input('number') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'LocationID')->dropDownList(\app\models\Locations\Locations::getList()) ?>
    </div>
</div>





