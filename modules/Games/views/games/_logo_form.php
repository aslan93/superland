<?php
use yii\helpers\Html;
use kartik\file\FileInput;


$initialPreview = [];
if ($model->Logo)
{
    $initialPreview[] = Html::img($model->logo, ['width' => 190]);
}
?>

<div class="row">
    <div class="col-md-6">
        <label>Logo</label>
        <?= FileInput::widget([
            'name' => 'Logo',
            'options' => ['accept' => 'image/.png'],
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview,
            ]
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'Video')->input('text')?>
    </div>
</div>



