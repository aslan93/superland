<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?php

    $items[0] = [
        'label' => 'Descriere',
        'content' => $this->render('_lang_form',[
            'form' => $form,
            'model' => $model,

        ])];
    $items[1] = [
        'label' => 'Imagini',
        'content' => $this->render('_image_form',[
            'form' => $form,
            'model' => $model,


        ])];
    $items[2] = [
        'label' => 'Logo & Video',
        'content' => $this->render('_logo_form',[
            'form' => $form,
            'model' => $model,
        ])];

    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);

    ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
