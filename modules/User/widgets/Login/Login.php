<?php

namespace app\modules\User\widgets\Login;

use app\models\User\LoginForm;
use app\models\User\User;
use Yii;
use yii\base\Widget;
use yii\helpers\Url;
use yii\rest\ActiveController;


class Login extends Widget
{
    public $url = '';

    public function run()
    {
        $model = new LoginForm();
        if (Yii::$app->request->isPost){
            if ($model->load(Yii::$app->request->post()) && $model->login()){
                return $this->render('index', [
                    'model' => $model,
                    'success' => true,
                ]);
            }
        }
            return $this->render('index', [
                'model' => $model,
            ]);


    }
}
