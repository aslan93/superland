<?php

use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;

$bundle = \app\assets\SuperFrontAssets::register($this);

?>
<div class="left-box" data-mh="3">
    <?php
    if (!$success) {
        ?>
        <?php $form = ActiveForm::begin([
            'action' => '',
            'method' => 'post',
            'id' => 'login',
            'fieldConfig' => [
                'options' => [
                    'tag' => false,
                ]
            ],

        ]); ?>
        <div class="big-title">
            Login
        </div>
        <div>
            <label>
                <?= $form->field($model, 'Email')->input('email', ['maxlength' => true, 'class' => '', 'placeholder' => 'Email'])->label(false) ?>
            </label>
        </div>

        <div>
            <label>
                <?= $form->field($model, 'Password')->passwordInput(['maxlength' => true, 'class' => '', 'placeholder' => 'Password'])->label(false) ?>
            </label>
        </div>
        <div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <label class="confirm-terms">
                        <input type="checkbox" class="style-input">
                        Remember me
                    </label>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right">

                </div>
            </div>
        </div>
        <div class="text-center mt40">
            <?= Html::SubmitButton('Login', ['class' => 'btn btn-default']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php
    }
    ?>
</div>




