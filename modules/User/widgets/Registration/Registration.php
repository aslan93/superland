<?php

namespace app\modules\User\widgets\Registration;

use app\models\User\User;
use Yii;
use yii\base\Widget;




class Registration extends Widget
{

    public function run()
    {
        $model = new User(['scenario' => 'add']);
        $model->Type = 'User';
        $model->Status = 'Active';
        $success = false;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $success = true;
            return $this->render('index', [
                'model' => $model,
                'success' => $success,
            ]);
        }

            return $this->render('index', [
                'model' => $model,
                'success' => $success,
            ]);

    }
}
