<?php
use kartik\popover\PopoverX;
use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;

$bundle = \app\assets\SuperFrontAssets::register($this);

?>
<div class="left-box" data-mh="3">
    <?php Pjax::begin([
        'id'=> 'register-pjax',
        'enablePushState' => false,
        'enableReplaceState'=>false,
    ])?>
    <?php
    if (!$success) {
        ?>
        <?php $form = ActiveForm::begin([
            'action' => '',
            'method' => 'post',
            'id' => 'register',
            'fieldConfig' => [
                'options' => [
                    'tag' => false,
                ]
            ],
            'options' => [

                'class' => 'register-pjax',
                'data-pjax' => true,
            ]
        ]); ?>
    <div class="big-title">
        Join us
    </div>
        <div>
            <label>
                <?= $form->field($model, 'Name')->input('text',['maxlength' => true,'class'=>'','placeholder'=>'Full Name'])->label(false) ?>
            </label>
        </div>
        <div>
            <label>
                <?= $form->field($model, 'Email')->input('email',['maxlength' => true,'class'=>'','placeholder'=>'Email'])->label(false) ?>
            </label>
        </div>
        <div>
            <label>
                <?= $form->field($model, 'BirthDate')->input('text',['maxlength' => true,'class'=>'','placeholder'=>'Birth Date'])->label(false) ?>
            </label>
        </div>
        <div>
            <label>
                <?= $form->field($model, 'CardID')->input('number',['maxlength' => true,'class'=>'','placeholder'=>'Card ID'])->label(false) ?>

            </label>
        </div>
        <div>
            <label>
                <?= $form->field($model, 'Address')->input('text',['maxlength' => true,'class'=>'','placeholder'=>'Delivery Address'])->label(false) ?>

            </label>
        </div>
        <div>
            <label>
                <?= $form->field($model, 'Password')->passwordInput(['maxlength' => true,'class'=>'','placeholder'=>'Password'])->label(false) ?>
            </label>
        </div>
        <div>
            <label class="confirm-terms">
                <input type="checkbox" class="style-input check" onchange="resetBtn(this)">
                By accepting this I agree <a href="<?=\yii\helpers\Url::to(['/info/rules-regulations'])?>" target="_blank" data-pjax="0" >Terms & Conditions</a>
            </label>
        </div>
        <div class="text-center mt40">
            <?= Html::SubmitButton('JOIN US', ['class' => 'btn btn-default save','disabled'=>'true']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php
    }else{?>
        <div>Inregistrat cu success</div><br>
        <div>Acum puteti sa v-a logati cu email-ul <?=$model->Email?> si parola!</div>
        <?php
    }
    ?>
    <?php $this->registerJs(
        "   
          
            function resetBtn(ob){
          
                if($(ob).is( ':checked' )){
                    $('.save').prop('disabled',false);
                }else{
                     $('.save').prop('disabled',true);
                }
            }
            
            "
        , \yii\web\View::POS_END);
    ?>

    <?php Pjax::end()?>
</div>




