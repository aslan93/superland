<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User\User */

$this->title = Yii::t("app", 'Updating User').': ' . $model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t("app", 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t("app", 'Updating');
?>
<div class="user-update">

    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
