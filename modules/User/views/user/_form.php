<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\grid\GridView;
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <span class="hidden userID" data-attribute="<?=$model->ID?>"></span>
        <div class="col-md-6">
            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
        </div>
        <?php if ($model->scenario == 'add') { ?>
        <div class="col-md-6">
            <?= $form->field($model, 'Password')->passwordInput(['maxlength' => true]) ?>
        </div>
        <?php } ?>
        <div class="col-md-6">
            <?= $form->field($model, 'CardID')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'Status')->widget(Select2::className(), [
                'data' => \app\models\User\User::getStatusList(),
                'hideSearch' => true
            ]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Create') : Yii::t("app", 'Update'), ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
if($model->isNewRecord){

}else {
    ?>
    <div>
        <?= Html::button('<i class="fa fa-plus"></i> ' . Yii::t("app", 'Add Child'), [
            'class' => 'btn btn-primary edit-child-item',
            'slider-item-id' => 0
        ]) ?>
    </div>

    <div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'options' => [
                        'width' => '50px'
                    ]
                ],
                [
                    'label' => 'Name',
                    'value' => function ($model) {
                        return $model->Name;
                    }
                ],
                [
                    'label' => 'Age',
                    'value' => function ($model) {
                        return $model->Age;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => [
                        'width' => '140px'
                    ],
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-pencil"></i>',Url::to('#'), [
                                'class' => ' edit-child-item',
                                'child-item-id' => $model->ID,
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="fa fa-trash"></i>', Url::to(['delete-child-item', 'id' => $model->ID]), [
                                //'class' => 'btn btn-danger btn-xs',
                            ]);
                        }
                    ],
                    'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>'
                ],
            ],
        ]); ?>
    </div>

    <?php Modal::begin([
        'id' => 'edit-slider-item-modal',
        'header' => Yii::t("app", 'Editing child item'),
        'size' => Modal::SIZE_LARGE
    ]); ?>

    <?php Modal::end(); ?>

    <?php $this->registerJs("
    $(document).on('click', '.edit-child-item', function(){
        $('#edit-slider-item-modal .modal-body').empty();
        $.post('" . Url::to(['get-item']) . "', {id: $(this).attr('child-item-id'),userID:$('.userID').attr('data-attribute')}, function(html){
            $('#edit-slider-item-modal .modal-body').html(html);
            $('#edit-slider-item-modal').modal();
        });
    });
") ?>

    <?php
}
?>
