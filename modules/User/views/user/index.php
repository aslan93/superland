<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use app\models\User\User;
/* @var $this yii\web\View */
/* @var $searchModel app\models\User\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("app", 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="add">
        <?= Html::a(Yii::t("app", 'Create User'), ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
<?php Pjax::begin(); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Email:email',
            'Name',
            [
                'attribute' => 'Status',
                'filter' => Html::activeDropDownList($searchModel, 'Status', User::getStatusList(), [
                    'class' => 'form-control'
                ]),
                'format' => 'raw',
                'value'=> function($model){
                    return User::getStatusLabel($model->Status);
                }
            ],
            [
                'attribute' => 'CardID',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'permissions' => function ($url, $model, $key) {
                        return Html::a ('<span class="glyphicon glyphicon glyphicon-lock" aria-hidden="true"></span> ', yii\helpers\Url::to(['/admin/user/user/get-item', 'userID' => $model->id]));
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a ('<span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true"></span> ', yii\helpers\Url::to(['delete', 'id' => $model->ID]));
                    },
                ],
                'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;{view}&nbsp;&nbsp;&nbsp;{permissions}&nbsp;&nbsp;&nbsp;{delete}</div>'
            ],
        ],
        'responsive'=>true,
        'hover'=>true
    ]); ?>
<?php Pjax::end(); ?></div>
