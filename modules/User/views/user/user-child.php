<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin([
    'id' => 'save-child-item-form',
    'action' => Url::to(['save-child-item'])
]); ?>

<div class="row">
    <?= $form->field($child, 'UserID')->hiddenInput(['value'=>$userID])->label(false) ?>
    <div class="col-md-6">
        <?= $form->field($child, 'Name')->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($child, 'Age')->input('number') ?>
    </div>
</div>
    <div class="form-group">
        <?= Html::button($child->isNewRecord ? Yii::t("app", 'Adauga') : Yii::t("app", 'Update'), ['class' =>  'btn btn-primary', 'id' => 'save-child-item-button']) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php $this->registerJs("
    $('#save-child-item-button').click(function(e){
        $.ajax({
            url: '" . Url::to(['save-child-item']) . "',
            data: new FormData($('#save-child-item-form')[0]),
            type: 'post',
            processData: false,
            contentType: false,
            success: function(){
            
                window.location.reload();
            }
        });
    });
") ?>
