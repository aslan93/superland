<?php

namespace app\modules\User\controllers;

use app\models\User\UserChild;
use Yii;
use app\models\User\User;
use app\models\User\UserSearch;
use app\controllers\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
        ];
    }
    
    public function permissions()
    {
        return [
//            'index' => [
//                'permission' => 'list_users',
//                'label' => Yii::t("app", 'View users list')
//            ],
//            'delete' => [
//                'permission' => 'delete_user',
//                'label' => Yii::t("app", 'Delete users')
//            ],
//            'create' => [
//                'permission' => 'create_user',
//                'label' => Yii::t("app",'Create users')
//            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {      // die();
        $model = new User(['scenario' => 'add']);
        $dataProvider = new ActiveDataProvider([
            'query' => UserChild::find()->where(['UserID' => $model->ID]),
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
               // 'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => UserChild::find()->where(['UserID' => $model->ID]),
        ]);
        $model->scenario = 'edit';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $this->redirect(['index']);

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetItem()
    {
        $id = (int)Yii::$app->request->post('id');
        $userID = (int)Yii::$app->request->post('userID');
        $child = $id > 0 ? UserChild::findOne($id) : new UserChild();

        return $this->renderAjax('user-child', [
            'child' => $child,
            'userID' => $userID,
        ]);
    }

    public function actionSaveChildItem()
    {
        $id = 0;
        if (isset(Yii::$app->request->post('UserChild')['ID'])){
            $id = Yii::$app->request->post('UserChild')['ID'];
        }

        $childItem = $id > 0 ? UserChild::findOne($id) : new UserChild();

        if ($childItem->load(Yii::$app->request->post())  )
        {

            $childItem->save();

        }
    }

    public function actionDeleteChildItem($id)
    {
        $model = UserChild::findOne($id);

        UserChild::deleteAll(['ID' => $id]);

        $this->redirect(['/admin/user/user/update', 'id' => $model->UserID]);
    }
}
