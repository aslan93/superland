<?php

namespace app\modules\Hotel\models;

use Yii;

/**
 * This is the model class for table "HotelLang".
 *
 * @property integer $ID
 * @property integer $HotelID
 * @property string $LangID
 * @property string $Name
 * @property string $Description
 *
 * @property Hotel $hotel
 */
class HotelLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'HotelLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['HotelID', 'LangID', 'Name', 'Description'], 'required'],
            [['HotelID'], 'integer'],
            [['Description'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Name'], 'string', 'max' => 255],
            [['HotelID'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['HotelID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'HotelID' => 'Hotel ID',
            'LangID' => 'Lang ID',
            'Name' => 'Name',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['ID' => 'HotelID']);
    }
}
