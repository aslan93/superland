<?php

namespace app\modules\Hotel\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Hotel\models\Hotel;

/**
 * HotelSearch represents the model behind the search form about `app\modules\Hotel\models\Hotel`.
 */
class HotelSearch extends Hotel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Stars'], 'integer'],
            [['Address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hotel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Stars' => $this->Stars,
        ]);

        $query->andFilterWhere(['like', 'Address', $this->Address]);

        return $dataProvider;
    }
}
