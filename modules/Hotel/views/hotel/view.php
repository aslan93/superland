<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\Hotel */

$this->title = $model->lang->Name;
$this->params['breadcrumbs'][] = ['label' => 'Hotels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-view">

    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>

    <p class="add">
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="text-center">
    <?=Html::img($model->mainImage->imagePath,['width'=>250])?>
    </div>
    <br>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'ID',
            'lang.Name',
            [
                'format' => 'raw',
                'label' => 'Description',
                'value' => function($model){
                     return $model->lang->Description;
                },
            ],
            'Address',
            'Stars',
        ],
    ]) ?>

</div>
