<?php

use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;


?>

<?= $form->field($langModel, "[$langModel->LangID]Name")->textInput() ?>
<?= $form->field($langModel, "[$langModel->LangID]Description")->textArea()->widget(TinyMce::className(),[]) ?>

