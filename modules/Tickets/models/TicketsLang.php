<?php

namespace app\modules\Tickets\models;

use Yii;

/**
 * This is the model class for table "TicketsLang".
 *
 * @property integer $ID
 * @property integer $TicketsID
 * @property string $LangID
 * @property string $Description
 * @property string $Name
 *
 * @property Tickets $tickets
 */
class TicketsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TicketsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TicketsID', 'LangID', 'Description', 'Name'], 'required'],
            [['TicketsID'], 'integer'],
            [['Description'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Name'], 'string', 'max' => 255],
            [['TicketsID'], 'exist', 'skipOnError' => true, 'targetClass' => Tickets::className(), 'targetAttribute' => ['TicketsID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TicketsID' => 'Tickets ID',
            'LangID' => 'Lang ID',
            'Description' => 'Description',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasOne(Tickets::className(), ['ID' => 'TicketsID']);
    }
}
