<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("app", 'Sliders');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="slider-index">

    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>

    <p class="add">
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t("app",'Create Slider'), ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => [
                    'width' => '50px'
                ]
            ],
            [
                'label'  => Yii::t("app",'Name'),
                'value' => function($model){
                    return $model->Name;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => [
                    'width' => '100px'
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => 'Edit'
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, [
                            'title' => 'Delete',
                            'onclick' => "return confirm('Delete?')"
                        ]);
                    }
                ],
                'template' => '<div class="text-center">{update}{delete}</div>'
            ],
        ],
    ]); ?>

</div>