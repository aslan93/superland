<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\Modal;
    use yii\helpers\Url;
    use kartik\grid\GridView;

?>

<?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($sliderModel, 'Name') ?>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label style="display: block;" class="control-label">&nbsp;</label>
                <?= Html::submitButton($sliderModel->isNewRecord ? Yii::t("app", 'Create') : Yii::t("app", 'Update'), ['class' => $sliderModel->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<hr />

<div>
    <?= Html::button('<i class="fa fa-plus"></i> '.Yii::t("app", 'Add slider item'), [
        'class' => 'btn btn-primary edit-slider-item',
        'slider-item-id' => 0
    ]) ?>
</div>
    <br><br>
    <div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'options' => [
                        'width' => '50px'
                    ]
                ],
                [
                    'label'  => 'Name',
                    'value' => function($model){
                        return $model->lang->Title;
                    }
                ],
                [
                    'label'  => 'Image',
                    'value' => function($model){
                        return Html::img('@web/uploads/slider/' . $model->Image, ['height' => '50px;']);
                    },
                    'format' => 'raw'
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => [
                        'width' => '140px'
                    ],
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::button('<i class="fa fa-pencil"></i>', [
                                'class' => 'edit-slider-item',
                                'slider-item-id' => $model->ID
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="fa fa-trash"></i>', Url::to(['delete-slider-item', 'id' => $model->ID]), [

                            ]);
                        }
                    ],
                    'template' => '<div class="text-center">{delete}</div>'
                ],
            ],
        ]); ?>
    </div>

<?php Modal::begin([
    'id' => 'edit-slider-item-modal',
    'header' => Yii::t("app", 'Editing slider item'),
    'size' => Modal::SIZE_LARGE
]); ?>

<?php Modal::end(); ?>

<?php $this->registerJs("
    $(document).on('click', '.edit-slider-item', function(){
        $('#edit-slider-item-modal .modal-body').empty();
        $.post('" . Url::to(['get-item']) . "', {id: $(this).attr('slider-item-id')}, function(html){
            $('#edit-slider-item-modal .modal-body').html(html);
            $('#edit-slider-item-modal').modal();
        });
    });
") ?>