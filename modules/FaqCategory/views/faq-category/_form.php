<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Faq\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Status')->dropDownList(\app\modules\FaqCategory\models\FaqCategory::getStatusList()) ?>

    <?php
    $items[0] = [
        'label' => 'Descriere',
        'content' => $this->render('_lang_form',[
            'form' => $form,
            'model' => $model,
        ])];

    echo '<br>';
    echo \yii\bootstrap\Tabs::widget([
        'items' => $items,
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
