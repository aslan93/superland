<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\FaqCategory\models\FaqCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Faq Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-category-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('Create Faq Category', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

    //            'ID',

                'lang.Name',
                'Status',

                [
                        'class' => 'yii\grid\ActionColumn',
                         'template' => '{update}&nbsp;&nbsp;{delete}'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
