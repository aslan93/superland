<?php

namespace app\modules\FaqCategory;

/**
 * FaqCategory module definition class
 */
class FaqCategory extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\FaqCategory\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
