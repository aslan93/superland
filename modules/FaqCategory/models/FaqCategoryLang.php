<?php

namespace app\modules\FaqCategory\models;

use Yii;

/**
 * This is the model class for table "FaqCategoryLang".
 *
 * @property integer $ID
 * @property integer $FaqCategoryID
 * @property string $LangID
 * @property string $Name
 */
class FaqCategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FaqCategoryLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FaqCategoryID', 'LangID', 'Name'], 'required'],
            [['FaqCategoryID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FaqCategoryID' => 'Faq Category ID',
            'LangID' => 'Lang ID',
            'Name' => 'Name',
        ];
    }
}
