<?php

namespace app\modules\Events\models;

use Yii;

/**
 * This is the model class for table "EventsLang".
 *
 * @property integer $ID
 * @property string $LangID
 * @property string $Name
 * @property string $Description
 * @property integer $EventID
 *
 * @property Events $event
 */
class EventsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EventsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Name', 'Description', 'EventID'], 'required'],
            [['Description'], 'string'],
            [['EventID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Name'], 'string', 'max' => 255],
            [['EventID'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['EventID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LangID' => 'Lang ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'EventID' => 'Event ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['ID' => 'EventID']);
    }
}
