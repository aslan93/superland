<?php

namespace app\modules\Events\controllers;

use app\components\UploadedFile;
use app\modules\Events\models\EventImage;
use app\modules\Events\models\PlanEvent;
use app\modules\Events\models\PlanEventSearch;
use Yii;
use app\modules\Events\models\Events;
use app\modules\Events\models\EventsSearch;
use app\controllers\BackendController;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionPlanifiedEvents()
    {   $searchModel = new PlanEventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('planified', [
            'dataProvider'=>$dataProvider,
            'searchModel' => $searchModel
        ]);
    }
    public function actionViewPlanEvent($id)
    {
        return $this->render('planEvent', [
            'model' => PlanEvent::find()->where(['ID'=>$id])->one(),
        ]);
    }
    public function actionDeletePlanEvent($id)
    {
        PlanEvent::find()->where(['ID'=>$id])->one()->delete();
        return $this->redirect(['planified-events']);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Events();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $images = \yii\web\UploadedFile::getInstancesByName('EventImages');

        if($images){

            foreach ($images as $image)
            {
                $file = md5(microtime(true)) . '.' . $image->extension;
                $thumb = 'thumb_' . $file;

                if ($image->saveAs(Yii::getAlias("@webroot/uploads/events/$file")))
                {   Image::thumbnail(Yii::getAlias("@webroot/uploads/events/$file"), 200, 200)->save(Yii::getAlias("@webroot/uploads/events/$thumb"));

                    if ($model->mainImage) {
                        $imageModel = new EventImage([
                            'EventID' => $model->ID,
                            'Thumb' => $thumb,
                            'Image' => $file,
                            'IsMain' => 0,
                        ]);
                    }else{
                        $imageModel = new EventImage([
                            'EventID' => $model->ID,
                            'Thumb' => $thumb,
                            'Image' => $file,
                            'IsMain' => 1,
                        ]);
                    }
                    $imageModel->save();
                }
            }
        }
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        EventImage::findOne($key)->delete();
        return '{}';
    }
    public function actionSetMainImage(){
        $key = Yii::$app->request->post('id');
        $imgModel = EventImage::findOne($key);

        EventImage::updateAll(['IsMain'=> 0],['EventID'=>$imgModel->EventID]);

        $imgModel->IsMain = 1;
        $rs = $imgModel->save();

        return "$rs";
    }
}
