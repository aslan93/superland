<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\Tickets\models\Tickets;
/* @var $this yii\web\View */
/* @var $model app\modules\BuyTickets\models\BuyTickets */

$this->title = 'Planified Event';
$this->params['breadcrumbs'][] = ['label' => 'Planified Events', 'url' => ['planified-events']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buy-tickets-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

        <?= Html::a('Delete', ['delete-plan-event', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="col-md-6">
        <div><h2>Packages INFO</h2></div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'ID',
//            'FullName',
//            'Age',
//            'UserName',
//            'Email:email',
//            'Address',
//            'CardID',
            [
                'label' => 'Package: ',
                'format' => 'raw',
                'value' => function ($model){
                    $pack = \app\modules\EventPackage\models\EventPackage::find()->where(['ID'=>$model->PackageID])->one();
                    $ret = '';

                    if ($pack) {

//                        $ret = 'Pack ID: '.$pack->ID.'<br>';
                        $ret = $ret. '<b>Pack Name:'.$pack->lang->Name.'</b><br>';


                            foreach ($pack->packageCaracteristics as $caracteristic) {

                                $ret = $ret.'-'.$caracteristic;

                            }


                    }
                    return $ret;
                }
            ],
            [
                'label' => 'Extra Options: ',
                'format' => 'raw',
                'value' => function ($model){
                    $eo = $model->eo;
                    $ret = '';
                    if ($eo) {

                        foreach ($eo as $o) {
                            $ret = $ret . ' ' . $o->lang->Name . '<br>';
                        }
                    }
                    return $ret;
                }
            ],
            [
                'label' => 'Menus:',
                'format' => 'raw',
                'value' => function ($model){
                    $eo = $model->em;
                    $ret = '';
                    if ($eo) {

                        foreach ($eo as $o) {
                            $ret = $ret . ' ' . $o->lang->Name . '<br>';
                        }
                    }
                    return $ret;
                }
            ],

        ],
    ]) ?>
    </div>
    <div class="col-md-6">
        <div><h2>Options INFO</h2></div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                'KidsNumber',
                [
                    'label' => 'Date & Time:',
                    'format' => 'raw',
                    'value' => function ($model){
                        $ret = '';
                        if ($model->Date) {
                            $ret = $model->Date . ' ' . $model->Time;
                        }
                        return $ret;
                    }
                ],

                'Status',

            ],
        ]) ?>
    </div>

</div>
