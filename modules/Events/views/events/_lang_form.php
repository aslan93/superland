<?php
use yii\bootstrap\Tabs;
use kartik\datetime\DateTimePicker;
?>

            <?php
            $items = [];
            foreach ($model->langs as $langID => $langModel)
            {
                $items[] = [
                    'label' => strtoupper($langID),
                    'content' => $this->render('_desc_form',[
                        'form' => $form,
                        'langModel' => $langModel,
                        'model' => $model,
                    ]),

                ];
            }
            echo '<br>';
            echo Tabs::widget([
                'items' => $items,
            ]);
            ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'Type')->dropDownList(\app\modules\Events\models\Events::getTypeList()) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'Capacity')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'LocationID')->dropDownList(\app\models\Locations\Locations::getList('Select')) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'DateTime')->widget(DateTimePicker::className()) ?>
            </div>
        </div>






