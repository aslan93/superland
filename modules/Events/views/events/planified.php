<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\BuyTickets\models\BuyTicketsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Planified Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buy-tickets-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--         Html::a('Create Buy Tickets', ['create'], ['class' => 'btn btn-success'])
<!--    </p>-->
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Date',
            'Time',
        [
            'label' => 'Package',
            'format' =>'raw',
            'value' => function($model){
                $tick = '';
                $room =\app\modules\EventPackage\models\EventPackage::find()->where(['ID'=>$model->PackageID])->one();
                if ($room) {

                    $total = 0;

                    $tick = 'Pack.'.$room->ID.' - ' . $room->lang->Name . ' - ' . $room->Price . 'Lei' . '<br>';

                }
                return $tick;
            }
        ],


            [
                'class' => 'yii\grid\ActionColumn',
                'options' => [
                    'width' => '100px'
                ],
                'buttons' => [
                    'view' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', \yii\helpers\Url::to(['/admin/events/events/view-plan-event','id'=>$model->ID]), [
                            'title' => 'Edit',
                            'data-pjax' => '',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', \yii\helpers\Url::to(['/admin/events/events/delete-plan-event','id'=>$model->ID]), [
                            'title' => 'Delete',
                            'onclick' => "return confirm('Delete?')",
                            'data-pjax' => '',
                        ]);
                    }
                ],
                'template' => '<div class="text-center">{view}{delete}</div>'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
