<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Locations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="locations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $items = [];
    foreach ($model->langs as $key => $langModel)
    {
        $items[] = [

            'label' => strtoupper($key),
            'content' => $this->render('_desc_form',[
                'form' => $form,
                'langModel' => $langModel,
                'model' => $model,
            ]),
        ];
    }
    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>
    <?= $form->field($model, 'Etaj')->textInput() ?>

    <div class="form-group" style="text-align: center;">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
