<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\ExtraOptions\models\ExtraOptions */

$this->title = 'Update Extra Options: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Extra Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="extra-options-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
