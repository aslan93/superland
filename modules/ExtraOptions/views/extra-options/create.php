<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\ExtraOptions\models\ExtraOptions */

$this->title = 'Create Extra Options';
$this->params['breadcrumbs'][] = ['label' => 'Extra Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extra-options-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
