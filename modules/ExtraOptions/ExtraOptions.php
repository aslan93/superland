<?php

namespace app\modules\ExtraOptions;

/**
 * ExtraOptions module definition class
 */
class ExtraOptions extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\ExtraOptions\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
