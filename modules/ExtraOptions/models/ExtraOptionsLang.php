<?php

namespace app\modules\ExtraOptions\models;

use Yii;

/**
 * This is the model class for table "ExtraOptionsLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property string $LangID
 * @property integer $ExtraOptionID
 *
 * @property ExtraOptions $extraOption
 */
class ExtraOptionsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ExtraOptionsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'LangID', 'ExtraOptionID'], 'required'],
            [['Description'], 'string'],
            [['ExtraOptionID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['ExtraOptionID'], 'exist', 'skipOnError' => true, 'targetClass' => ExtraOptions::className(), 'targetAttribute' => ['ExtraOptionID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
            'ExtraOptionID' => 'Extra Option ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtraOption()
    {
        return $this->hasOne(ExtraOptions::className(), ['ID' => 'ExtraOptionID']);
    }
}
