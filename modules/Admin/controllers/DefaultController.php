<?php

namespace app\modules\Admin\controllers;

use Yii;
use yii\web\Controller;
use app\models\ArticleFile\ArticleFile;
use app\models\ArticleFileLang\ArticleFileLang;

/**
 * Default controller for the `Admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
