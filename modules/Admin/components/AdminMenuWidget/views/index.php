<?php

    use yii\bootstrap\NavBar;
    use yii\bootstrap\Nav;
    
    $mainItems = [];
    $quickAddItems = [];
//    foreach ($articleTypes as $type => $articleType)
//    {
//        $mainItems[] = [
//            'label' => $articleType['labels']['plural'],
//            'url' => ['/admin/article', 'type' => $type],
//            'options' =>[
//
//            ],
//        ];
//        $quickAddItems[] = [
//            'label' => '<i style="width:20px;text-align: center" class="' . $articleType['labels']['icon'] . '" aria-hidden="true"></i> ' . $articleType['labels']['add'],
//            'url' => [
//                '/admin/article/article/create',
//                'type' => $type,
//                'hash' => sha1(microtime())
//            ],
//            'options' =>[
//
//            ],
//            'encode' => false
//        ];
//    }
    
//    $mainItemsGroup[] = [
//        'label' => Yii::t("app", 'Content'),
//        'items' => $mainItems,
//        'options'=>[
//                'class' => 'content-icon',
//        ]
//    ];


    if (Yii::$app->user->isGuest)
    {
        $menuItems = [
            ['label' => 'Login', 'url' => ['/admin/user/login']]
        ];
    }
    else
    {
        $leftItems = [
//            ['label' => Yii::t("app", 'Main'), 'url' => ['/admin/dashboard'],'options'=>['class'=>'home']],
//            [
//                'label' => Yii::t("app", 'Add article'),
//                'items' => $quickAddItems,
//                'options' =>[
//                    'class' => 'plus',
//                ],
//
//            ]
        ];
        $rightItems = [
            [
                'label' => Yii::t("app", 'Superland'),
                'options' =>[
                  'class' => 'slider',
                ],
                'items' => [
                    ['label' => Yii::t("app", 'Locations'), 'url' => ['/admin/locations']],
                    ['label' => Yii::t("app", 'Games'), 'url' => ['/admin/games']],
                    ['label' => Yii::t("app", 'Jobs'), 'url' => ['/admin/jobs']],
                    ['label' => Yii::t("app", 'Blog'), 'url' => ['/admin/blog']],
                ]
            ],
            [
                'label' => Yii::t("app", 'Shop'),
                'options' =>[
                    'class' => 'sites',
                ],
                'items' => [
                    ['label' => Yii::t("app", 'Food&Relax'), 'url' => ['/admin/food']],
                    ['label' => Yii::t("app", 'Food&Relax Categories'), 'url' => ['/admin/food-category']],
                    ['label' => Yii::t("app", 'Boutiques'), 'url' => ['/admin/boutique']],
                    ['label' => Yii::t("app", 'Boutique Categories'), 'url' => ['/admin/boutique-category']],
                ]
            ],
            [
                'label' => Yii::t("app", 'Harta Superland'),
                'url' => ['/admin/map/map-markers'],
                'options' =>[
                    'class' => 'content-icon',
                     ],
            ],
//            [
//                'label' => Yii::t("app", 'Rezervari'),
//                'options' =>[
//                    'class' => 'sites',
//                ],
//                'items' => [
//                    ['label' => Yii::t("app", 'Hoteluri partenere'), 'url' => ['/admin/hotel']],
//                    ['label' => Yii::t("app", 'Lista cu rezervari'), 'url' => ['/admin/reservation']],
//                    ['label' => Yii::t("app", 'Creare rezervare'), 'url' => ['/admin/reservation/reservation/create']],
//                ]
//            ],
            [
                'label' => Yii::t("app", 'Tickete'),
                'options' =>[
                    'class' => 'sites',
                ],
                'items' => [
                    ['label' => Yii::t("app", 'Tickete'), 'url' => ['/admin/tickets']],
                    ['label' => Yii::t("app", 'Comenzi de Tickete'), 'url' => ['/admin/buy-tickets']],
                ]
            ],
            [
                'label' => Yii::t("app", 'Events'),
                'options' =>[
                    'class' => 'sites',
                ],
                'items' => [
                    ['label' => Yii::t("app", 'Events'), 'url' => ['/admin/events']],
                    ['label' => Yii::t("app", 'Event Menus'), 'url' => ['/admin/event-menu']],
                    ['label' => Yii::t("app", 'Event Rooms'), 'url' => ['/admin/event-room']],
                    ['label' => Yii::t("app", 'Event Packages'), 'url' => ['/admin/event-package']],
                    ['label' => Yii::t("app", 'Event Extra Options'), 'url' => ['/admin/extra-options']],
                    ['label' => Yii::t("app", 'Comenzi de Evenimente'), 'url' => ['/admin/events/events/planified-events']],
                ]
            ],
            [
                'label' => Yii::t("app", 'Clienti'),
                'options' =>[
                    'class' => 'users',
                ],
                'items' => [
                    ['label' => Yii::t("app", 'Clienti'), 'url' => ['/admin/user']],
                    ['label' => Yii::t("app", 'Creare client nou'), 'url' => ['/admin/user/user/create']],
                ]
            ],
            [
                'label' => Yii::t("app", 'Multimedia'),
                'options' =>[
                    'class' => 'slider',
                ],
                'items' => [
                    ['label' => Yii::t("app", 'Slider'), 'url' => ['/admin/slider']],
                    ['label' => Yii::t("app", 'Gallery'), 'url' => ['/admin/gallery']],
                ]
            ],
            [
                'label' => Yii::t("app", 'Settings'),
                'options' => [
                        'class' => 'settings',
                ],
                'items' => [
                    ['label' => Yii::t("app", 'Settings'), 'url' => ['/admin/settings']],
                    ['label' => Yii::t("app", 'Menus'), 'url' => ['/admin/menu']],
                ]
            ],

            ['label' => Yii::t("app", 'Feedback'), 'url' => ['/admin/feedback'],'options' =>[
                'class' => 'feedback',
            ],
                ],
            ['label' => Yii::t("app", 'Quit'), 'url' => ['/admin/user/login/logout']],

        ];
        
        $menuItems = yii\helpers\ArrayHelper::merge($leftItems,$rightItems);
    }
    
    echo Nav::widget([
        'options' => ['class' => ''],
        'items' => $menuItems
    ]);

?>

