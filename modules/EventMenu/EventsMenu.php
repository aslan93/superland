<?php

namespace app\modules\EventMenu;

/**
 * EventMenu module definition class
 */
class EventsMenu extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\EventMenu\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
