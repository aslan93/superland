<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\EventMenu\models\EventMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Menu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID',
            [   'label'=>'Image',
                'format'=>'raw',
                'value'=>function ($model){

                    if ($model->Image){
                        return Html::img($model->image,['width'=>150]);
                    }else{
                        return '';
                    }
                }

            ],
            'lang.Name',
            'Price',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
