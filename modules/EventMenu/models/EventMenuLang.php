<?php

namespace app\modules\EventMenu\models;

use Yii;

/**
 * This is the model class for table "EventMenuLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property string $LangID
 * @property integer $EventMenuID
 *
 * @property EventMenu $eventMenu
 */
class EventMenuLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EventMenuLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'LangID', 'EventMenuID'], 'required'],
            [['Description'], 'string'],
            [['EventMenuID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['EventMenuID'], 'exist', 'skipOnError' => true, 'targetClass' => EventMenu::className(), 'targetAttribute' => ['EventMenuID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
            'EventMenuID' => 'Event Menu ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventMenu()
    {
        return $this->hasOne(EventMenu::className(), ['ID' => 'EventMenuID']);
    }
}
