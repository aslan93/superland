<?php

namespace app\modules\Jobs\models;

use Yii;

/**
 * This is the model class for table "JobsLang".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Description
 * @property integer $JobsID
 * @property string $LangID
 *
 * @property Jobs $jobs
 */
class JobsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'JobsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Description', 'JobsID', 'LangID'], 'required'],
            [['Description'], 'string'],
            [['JobsID'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['JobsID'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['JobsID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'JobsID' => 'Jobs ID',
            'LangID' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasOne(Jobs::className(), ['ID' => 'JobsID']);
    }
}
