<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;

?>


<div class="food-category-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?php
    $items[0] = [
        'label' => 'Descriere',
        'content' => $this->render('_lang_form',[
            'form' => $form,
            'model' => $model,
        ])];


    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>
    <?= $form->field($model, "Save")->textInput(['value'=>'true','class'=>'hidden'])->label(false) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
