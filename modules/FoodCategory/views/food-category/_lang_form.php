<?php
 use app\modules\Category\models\Category;
 use kartik\datetime\DateTimePicker;
 use yii\bootstrap\Tabs;
?>

<?php
$items = [];
foreach ($model->langs as $key => $langModel)
{
    $items[] = [

        'label' => strtoupper($key),
        'content' => $this->render('_desc_form',[
            'form' => $form,
            'langModel' => $langModel,
            'model' => $model,
        ]),
    ];
}
echo '<br>';
echo Tabs::widget([
    'items' => $items,
]);
?>






