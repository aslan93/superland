<?php

namespace app\modules\FoodCategory;

/**
 * FoodCategory module definition class
 */
class FoodCategory extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\FoodCategory\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
