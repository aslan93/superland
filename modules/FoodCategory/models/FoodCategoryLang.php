<?php

namespace app\modules\FoodCategory\models;

use Yii;

/**
 * This is the model class for table "FoodCategoryLang".
 *
 * @property integer $ID
 * @property integer $FoodCategoryID
 * @property string $Name
 * @property string $Description
 * @property string $LangID
 *
 * @property FoodCategory $foodCategory
 */
class FoodCategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FoodCategoryLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FoodCategoryID'], 'required'],
            [['FoodCategoryID'], 'integer'],
            [['Description'], 'string'],
            [['Name'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['FoodCategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => FoodCategory::className(), 'targetAttribute' => ['FoodCategoryID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FoodCategoryID' => 'Food Category ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'LangID' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodCategory()
    {
        return $this->hasOne(FoodCategory::className(), ['ID' => 'FoodCategoryID']);
    }
}
