<?php

namespace app\models\PollVotes;

use Yii;
use app\models\PollAnswer\PollAnswer;
use app\models\Poll\Poll;

/**
 * This is the model class for table "PollVotes".
 *
 * @property integer $ID
 * @property integer $PollID
 * @property integer $PollAnswerID
 * @property string $UserIP
 *
 * @property Poll $poll
 * @property PollAnswer $pollAnswer
 */
class PollVotes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PollVotes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PollID', 'PollAnswerID', 'UserIP'], 'required'],
            [['PollID', 'PollAnswerID'], 'integer'],
            [['UserIP'], 'string', 'max' => 255],
            [['PollID'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['PollID' => 'ID']],
            [['PollAnswerID'], 'exist', 'skipOnError' => true, 'targetClass' => PollAnswer::className(), 'targetAttribute' => ['PollAnswerID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PollID' => Yii::t('app', 'Poll ID'),
            'PollAnswerID' => Yii::t('app', 'Poll Answer ID'),
            'UserIP' => Yii::t('app', 'User Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Poll::className(), ['ID' => 'PollID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollAnswer()
    {
        return $this->hasOne(PollAnswer::className(), ['ID' => 'PollAnswerID']);
    }
}
