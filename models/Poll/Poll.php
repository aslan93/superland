<?php

namespace app\models\Poll;

use Yii;
use app\models\PollLang\PollLang;
use app\models\PollAnswer\PollAnswer;
use app\models\PollVotes\PollVotes;

/**
 * This is the model class for table "Poll".
 *
 * @property integer $ID
 * @property string $Status
 *
 * @property PollAnswer[] $pollAnswers
 * @property PollLang[] $pollLangs
 * @property PollVotes[] $pollVotes
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Poll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollAnswers()
    {
        return $this->hasMany(PollAnswer::className(), ['PollID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        return $this->hasMany(PollLang::className(), ['PollID' => 'ID'])->indexBy('LangID');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(PollLang::className(), ['PollID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollVotes()
    {
        return $this->hasMany(PollVotes::className(), ['PollID' => 'ID']);
    }
}
