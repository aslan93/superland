<?php

namespace app\models\Locations;

use Yii;

/**
 * This is the model class for table "LocationsLang".
 *
 * @property integer $ID
 * @property integer $LocationID
 * @property string $LangID
 * @property string $Title
 * @property string $Description
 *
 * @property Locations $location
 */
class LocationsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LocationsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LocationID', 'LangID', 'Title', 'Description'], 'required'],
            [['LocationID'], 'integer'],
            [['Description'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['LocationID'], 'exist', 'skipOnError' => true, 'targetClass' => Locations::className(), 'targetAttribute' => ['LocationID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LocationID' => 'Location ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::className(), ['ID' => 'LocationID']);
    }
}
