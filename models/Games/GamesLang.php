<?php

namespace app\models\Games;

use Yii;

/**
 * This is the model class for table "GamesLang".
 *
 * @property integer $ID
 * @property integer $GameID
 * @property string $LangID
 * @property string $Title
 * @property string $Description
 *
 * @property Games $game
 */
class GamesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GamesLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['ID', 'GameID'], 'integer'],
            [['Description'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['GameID'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['GameID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'GameID' => 'Game ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['ID' => 'GameID']);
    }
}
