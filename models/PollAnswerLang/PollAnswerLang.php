<?php

namespace app\models\PollAnswerLang;

use Yii;
use app\models\PollAnswer\PollAnswer;

/**
 * This is the model class for table "PollAnswerLang".
 *
 * @property integer $ID
 * @property integer $PollAnswerID
 * @property string $LangID
 * @property string $Title
 *
 * @property PollAnswer $pallAnswer
 */
class PollAnswerLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PollAnswerLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PollAnswerID', 'LangID', 'Title'], 'required'],
            [['PollAnswerID'], 'integer'],
            [['LangID', 'Title'], 'string', 'max' => 255],
            [['PollAnswerID'], 'exist', 'skipOnError' => true, 'targetClass' => PollAnswer::className(), 'targetAttribute' => ['PollAnswerID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PollAnswerID' => Yii::t('app', 'Pall Answer ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPallAnswer()
    {
        return $this->hasOne(PollAnswer::className(), ['ID' => 'PollAnswerID']);
    }
}
