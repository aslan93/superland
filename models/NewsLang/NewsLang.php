<?php

namespace app\models\NewsLang;

use Yii;

/**
 * This is the model class for table "NewsLang".
 *
 * @property integer $ID
 * @property integer $NewsID
 * @property integer $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property News $news
 */
class NewsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'NewsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Title', 'Text'], 'required'],
            [['NewsID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'SeoTitle'], 'string', 'max' => 255],
            [['Keywords', 'Description'], 'string', 'max' => 500],
            [['NewsID'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t("app", 'ID'),
            'NewsID' => Yii::t("app", 'News ID'),
            'LangID' => Yii::t("app", 'Lang ID'),
            'Title' => Yii::t("app", 'Title'),
            'Text' => Yii::t("app", 'Text'),
            'SeoTitle' => Yii::t("app", 'Seo Title'),
            'Keywords' => Yii::t("app", 'Keywords'),
            'Description' => Yii::t("app", 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['ID' => 'NewsID']);
    }
}
