<?php

namespace app\models\Gallery;

use app\modules\Events\models\Events;
use Yii;
use app\models\GalleryItem\GalleryItem;
use app\models\GalleryLang\GalleryLang;
use yii\helpers\Url;

/**
 * This is the model class for table "Gallery".
 *
 * @property integer $ID
 * @property integer $ParentID
 * @property integer $Position
 *
 * @property GalleryItem[] $items
 * @property GalleryLang $lang
 * @property GalleryLang[] $langs
 */
class Gallery extends \yii\db\ActiveRecord {
	const TypeDefault = 'Default';
	const TypeVideo = 'Video';
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Gallery';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Type' 
						],
						'required' 
				],
				[ 
						[ 
								'ParentID',
								'Position',
                                'EventID',
                            'Visualisations',
						],
						'integer' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => Yii::t ( 'app', 'ID' ),
				'ParentID' => Yii::t ( 'app', 'Părinte ID' ),
				'Position' => Yii::t ( 'app', 'Poziție' ) ,
                'EventID' => Yii::t ( 'app', 'Event ID' ),
                'Visualisations'=> Yii::t ( 'app', 'Visualisations'),

		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getItems() {
		return $this->hasMany ( GalleryItem::className (), [ 
				'GalleryID' => 'ID' 
		] )->with ( 'langs' )->orderBy ( 'Position' );
	}
	
	/**
	 *
	 * @return intiger
	 */
	public function getCountItems() {
		return count ( $this->items );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLang() {
		return $this->hasOne ( GalleryLang::className (), [ 
				'GalleryID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLangs() {
		return $this->hasMany ( GalleryLang::className (), [ 
				'GalleryID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getParent() {
		return $this->hasOne ( Gallery::className (), [ 
				'ParentID' => 'ID' 
		] )->with ( 'lang' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getThumb() {
		return $this->hasOne ( GalleryItem::className (), [ 
				'GalleryID' => 'ID' 
		] )->orderBy ( 'Position' )->one ()->Thumb;
	}

	public function getImage() {
        $path = $this->hasOne ( GalleryItem::className (), [
            'GalleryID' => 'ID'
        ])->orderBy ( 'Position' )->one();
        $val = $path->Value;
        return Url::to('/uploads/gallery/'.$val);
	}

    public function getEvent() {
        return $this->hasOne (Events::className (), [
            'ID' => 'EventID'
        ])->with ( 'lang' );
    }
}
