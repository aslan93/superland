<?php

namespace app\models\Feedback;

use Yii;

/**
 * This is the model class for table "Feedback".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Phone
 * @property string $Email
 * @property string $Message
 * @property string $Date
 */
class Feedback extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'Email'], 'required'],
            [['Message','Subiect'], 'string'],
            [['Date'], 'safe'],
            [['Name', 'Phone', 'Email','File','Subiect'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t("app", 'ID'),
            'Name' => Yii::t("app", 'Name'),
            'Phone' => Yii::t("app", 'Phone'),
            'Email' => Yii::t("app", 'Email'),
            'Message' => Yii::t("app", 'Message'),
            'Date' => Yii::t("app", 'Date'),
            'File'=> Yii::t("app", 'File'),
            'Subiect'=> Yii::t("app", 'Subiect'),
        ];
    }

    public function getdMYDate()
    {
        return date('d M Y', strtotime($this->Date));
    }
    public function getShortText($length = 200)
    {
        return mb_substr(strip_tags(html_entity_decode($this->Message)), 0, $length);
    }
}
