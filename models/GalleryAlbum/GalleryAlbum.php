<?php

namespace app\models\GalleryAlbum;

use app\models\GalleryAlbumLang\GalleryAlbumLang;
use app\models\GalleryItem\GalleryItem;
use Yii;

/**
 * This is the model class for table "GalleryAlbum".
 *
 * @property integer $ID
 * @property integer $Position
 * @property string $Image
 *
 * @property GalleryAlbumLang[] $galleryAlbumLangs
 */
class GalleryAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GalleryAlbum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Position'], 'integer'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Position' => Yii::t('app', 'Position'),
            'Image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs() {
        return $this->hasMany ( GalleryAlbumLang::className (), [
            'GalleryAlbumID' => 'ID'
        ] );
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLang() {
        return $this->hasOne ( GalleryAlbumLang::className (), [
            'GalleryAlbumID' => 'ID'
        ] )->where ( [
            'LangID' => Yii::$app->language
        ] );
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItems() {
        return $this->hasMany ( GalleryItem::className (), [
            'GalleryID' => 'ID'
        ] )->with ( 'langs' )->orderBy ( 'Position' );
    }

    /**
     *
     * @return intiger
     */
    public function getCountItems() {
        return count ( $this->items );
    }
}
