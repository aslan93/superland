<?php

namespace app\models\User;

use Yii;

/**
 * This is the model class for table "UserChild".
 *
 * @property integer $ID
 * @property integer $UserID
 * @property string $Name
 * @property integer $Age
 *
 * @property User $user
 */
class UserChild extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserChild';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'Name', 'Age'], 'required'],
            [['UserID', 'Age'], 'integer'],
            [['Name'], 'string', 'max' => 255],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'Name' => 'Name',
            'Age' => 'Age',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }
}
