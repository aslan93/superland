<?php

namespace app\models\CategoryLang;

use Yii;
use app\models\Category\Category;

/**
 * This is the model class for table "CategoryLang".
 *
 * @property integer $ID
 * @property integer $CategoryID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property Category $category
 */
class CategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CategoryLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Title', 'Text'], 'required'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'SeoTitle'], 'string', 'max' => 255],
            [['Keywords', 'Description'], 'string', 'max' => 500],
            [['SeoTitle', 'Keywords', 'Description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CategoryID' => Yii::t("app", 'Category ID'),
            'LangID' => Yii::t("app", 'Lang ID'),
            'Title' => Yii::t("app", 'Title'),
            'Text' => Yii::t("app", 'Text'),
            'SeoTitle' => Yii::t("app", 'Seo Title'),
            'Keywords' => Yii::t("app", 'Keywords'),
            'Description' => Yii::t("app", 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['ID' => 'CategoryID']);
    }
}
