<?php

namespace app\models\PollAnswer;

use Yii;
use \app\models\PollAnswerLang\PollAnswerLang;
use app\models\Poll\Poll;
use app\models\PollVotes\PollVotes;
/**
 * This is the model class for table "PollAnswer".
 *
 * @property integer $ID
 * @property integer $PollID
 *
 * @property Poll $poll
 * @property PollAnswerLang[] $pollAnswerLangs
 * @property PollVotes[] $pollVotes
 */
class PollAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PollAnswer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PollID'], 'required'],
            [['PollID'], 'integer'],
            [['PollID'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['PollID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PollID' => Yii::t('app', 'Poll ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Poll::className(), ['ID' => 'PollID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollVotes()
    {
        return $this->hasMany(PollVotes::className(), ['PollAnswerID' => 'ID']);
    }
    
    public function getPollVotesCount()
    {
        return count($this->pollVotes);
    }

        public function getLangs()
    {
        return $this->hasMany(PollAnswerLang::className(), ['PollAnswerID' => 'ID'])->indexBy('LangID');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(PollAnswerLang::className(), ['PollAnswerID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

}
