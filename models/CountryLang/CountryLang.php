<?php

namespace app\models\CountryLang;

use app\models\Country\Country;
use Yii;

/**
 * This is the model class for table "CountryLang".
 *
 * @property integer $ID
 * @property integer $CountryID
 * @property string $LangID
 * @property string $Name
 *
 * @property Country $country
 */
class CountryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CountryLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'required'],
            [['Name'], 'string', 'max' => 255],
            [['LangID', 'CountryID'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t("app", 'ID'),
            'CountryID' => Yii::t("app", 'Country ID'),
            'LangID' => Yii::t("app", 'Lang ID'),
            'Name' => Yii::t("app", 'Name'),
        ];
    }
    
}