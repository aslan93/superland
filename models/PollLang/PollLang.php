<?php

namespace app\models\PollLang;

use Yii;
use app\models\Poll\Poll;
/**
 * This is the model class for table "PollLang".
 *
 * @property integer $ID
 * @property integer $PollID
 * @property string $LangID
 * @property string $Title
 *
 * @property Poll $poll
 */
class PollLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PollLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Title'], 'required'],
            [['PollID'], 'integer'],
            [['LangID', 'Title'], 'string', 'max' => 255],
            [['PollID'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['PollID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PollID' => Yii::t('app', 'Poll ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Poll::className(), ['ID' => 'PollID']);
    }
    
}
