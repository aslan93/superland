<?php

namespace app\assets;

use yii\web\AssetBundle;



class SuperFrontAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/superland/assets';
    
    public $css = [
//        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/swiper.css',
//        'css/javascript.fullPage.css',
        "css/jquery.formstyler.css",
        'css/animate.css',
//        'css/bootstrap-datepicker.css',
//        'css/select2.min.css',
        "css/ekko-lightbox.css",

        "css/clockpicker.css",
        "css/animsition.min.css",
        "css/timepicki.css",
        "css/bootstrap-datepicker.css",
        'css/main.css',
        'css/site.css',

    ];
    
    public $js = [

//        "js/jquery.matchHeight-min.js",
//        "https://maps.googleapis.com/maps/api/js?key=AIzaSyD5pXp7Lol5kF9oPCrWOukTc5k3mNImjwI",
        "js/jquery.formstyler.min.js",
        "js/jquery.matchHeight-min.js",
        "js/swiper.js",
        "js/select2.min.js",
        "js/massonry.js",
        "js/jquery.flip.min.js",
        "js/ekko-lightbox.js",

//        "js/clockpicker.js",
        "js/animsition.min.js",
        "js/jquery.scrollme.js",
        "js/timepicki.js",
        "js/bootstrap-datepicker.min.js",
        "js/scripts.js",
        "js/site.js",


    ];
    
    public $depends = [
       'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
    
}
?>
