<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
//        'css/admin/stylesheet.css',
//        'css/admin/theme.css',
   //     "css/admin/css/bootstrap.min.css" ,
        "css/admin/css/font-awesome.min.css" ,
        "css/admin/css/swiper.min.css" ,
        "css/admin/css/animate.css" ,
        "css/admin/css/bootstrap-datepicker.css" ,
        "css/admin/css/select2.min.css" ,
        "css/admin/css/fileinput-rtl.min.css" ,
        "css/admin/css/fileinput.min.css" ,
        "css/admin/css/jquery.formstyler.css" ,
        "css/admin/css/main.css" ,
    ];
    public $js = [
 //   "css/admin/js/jquery.js",
    "https://code.jquery.com/ui/1.12.1/jquery-ui.js",
    "css/admin/js/jquery.matchHeight-min.js",
  //  "css/admin/js/bootstrap.min.js",
    "css/admin/js/jquery.matchHeight-min.js",
    "css/admin/js/swiper.min.js",
    "css/admin/js/jquery.mousewheel.js",
    "css/admin/js/bootstrap-datepicker.min.js",
    "css/admin/js/select2.min.js",
    "css/admin/js/fileinput.min.js",
    "css/admin/js/jquery.formstyler.min.js",
    "css/admin/js/scripts.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}
