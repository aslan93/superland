<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\growl\Growl;
use app\modules\Admin\components\AdminMenuWidget\AdminMenuWidget;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script>function baseUrl(url){ return '<?=\yii\helpers\Url::to(['/'])?>' + url }</script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="side-bar-nav">
        <div class="open-nav">
            <div class="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="logo">
            <a href="<?=\yii\helpers\Url::to(['/admin'])?>">
                <span class="strong">NIXAP</span>CMS
            </a>
        </div>
        <div class="nav-list-item">
            <?= AdminMenuWidget::widget() ?>
        </div>
    </div>
    <div class="content">
        <header>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                    <div class="custom-search-input">
<!--                        <div class="input-group col-md-12">-->
<!--                            <input type="text" class="search-query form-control" placeholder="Search" />-->
<!--                            <span class="input-group-btn">-->
<!--                                    <button class="btn btn-danger" type="button">-->
<!--                                        <span class=" glyphicon glyphicon-search"></span>-->
<!--                                    </button>-->
<!--                                </span>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="user">
                        <span class="text">Salutare Admin!</span>
                        <span>
                                <a href="<?=\yii\helpers\Url::to(['#'])?>">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 299.997 299.997" style="enable-background:new 0 0 299.997 299.997;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M149.996,0C67.157,0,0.001,67.158,0.001,149.997c0,82.837,67.156,150,149.995,150s150-67.163,150-150
                                                C299.996,67.156,232.835,0,149.996,0z M150.453,220.763v-0.002h-0.916H85.465c0-46.856,41.152-46.845,50.284-59.097l1.045-5.587
                                                c-12.83-6.502-21.887-22.178-21.887-40.512c0-24.154,15.712-43.738,35.089-43.738c19.377,0,35.089,19.584,35.089,43.738
                                                c0,18.178-8.896,33.756-21.555,40.361l1.19,6.349c10.019,11.658,49.802,12.418,49.802,58.488H150.453z"/>
                                        </g>
                                    </g>
                                </svg>
                                </a>
                            </span>
                        <span>
                                <a href="<?=\yii\helpers\Url::to(['/admin/user/login/logout'])?>" class="rotate">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 297 297" style="enable-background:new 0 0 297 297;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <g>
                                                    <path d="M155,6.5c-30.147,0-58.95,9.335-83.294,26.995c-2.789,2.023-3.547,5.853-1.739,8.787L92.83,79.374
                                                        c0.962,1.559,2.53,2.649,4.328,3.004c1.796,0.354,3.661-0.054,5.145-1.129c14.23-10.323,31.069-15.78,48.698-15.78
                                                        c45.783,0,83.03,37.247,83.03,83.03c0,45.783-37.247,83.03-83.03,83.03c-17.629,0-34.468-5.456-48.698-15.78
                                                        c-1.484-1.076-3.349-1.486-5.145-1.129c-1.798,0.355-3.366,1.444-4.328,3.004l-22.863,37.093
                                                        c-1.808,2.934-1.05,6.763,1.739,8.787C96.05,281.165,124.853,290.5,155,290.5c78.299,0,142-63.701,142-142S233.299,6.5,155,6.5z"
                                                    />
                                                    <path d="M90.401,201.757c1.147-2.142,1.021-4.74-0.326-6.76l-15.463-23.195h93.566c12.849,0,23.302-10.453,23.302-23.302
                                                        s-10.453-23.302-23.302-23.302H74.612l15.463-23.195c1.348-2.02,1.473-4.618,0.326-6.76c-1.146-2.141-3.377-3.478-5.806-3.478
                                                        H40.019c-2.201,0-4.258,1.1-5.479,2.933L1.106,144.847c-1.475,2.212-1.475,5.093,0,7.306l33.433,50.149
                                                        c1.221,1.832,3.278,2.933,5.479,2.933h44.577C87.025,205.235,89.256,203.898,90.401,201.757z"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </span>
                    </div>
                </div>
            </div>
        </header>
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Dashboard',
                'url' => ['/admin']
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
        <footer>
            <div class="copyright">
                &copy;Nixap 2017
            </div>
            <div class="powered">
                POWERED BY YII FRAMEWORK
            </div>
            <div class="clearfix"></div>
        </footer>
    </div>
</div>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
