<div class="result-box">
    <a href="#">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="result-box-img">
                    <?php
                    echo \yii\bootstrap\Html::img($model->mainImage->imagePath,['class' => 'img-responsive']);
                    ?> </div>
            </div>


            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="description-box">
                    <div class="title-box">
                        <?=$model->lang->Name?>
                    </div>
                    <div class="text">
                        <?=$model->lang->Description?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="price">
                    <sup>$</sup><span class="number">319</span><sup>00</sup><sub>USD</sub>
                    <span class="fa fa-chevron-right"></span>
                </div>
            </div>
        </div>
    </a>

</div>