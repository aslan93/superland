<?php
use yii\bootstrap\Html;
use yii\widgets\ListView;
?>
<div class="container">

    <div class="result-group">

        <?php
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_hotel',
            'pager' => [
                'prevPageLabel' => 'previous',
                'nextPageLabel' => 'next',
                'maxButtonCount' => 5,
            ],

        ]);
        ?>


    </div>

</div>

