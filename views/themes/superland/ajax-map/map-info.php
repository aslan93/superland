

<a onclick="MallMaper.closeMarkerInfo();"  class="close-mall-map-info">
    <span class="fa fa-close"></span>
</a>
<div class="container">
    <div class="title">
        <?php
        if (isset($marker)){
            ?>
            <?=$marker->lang->Title?>
            <?php
        }
        ?>
    </div>
    <div>
        <?php
        if (isset($marker)){
            ?>
            <?=$marker->lang->Description?>
            <?php
        }
        ?>
    </div>
    <div class="location">
       <b><span>Locatie: </span></b>
        <?php
        if (isset($marker->location)){


            ?>
            <?=$marker->location->lang->Title?>
            <?php
        }
        ?>
    </div>
</div>

