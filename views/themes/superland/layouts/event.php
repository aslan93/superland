<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\SuperFrontAssets;
use yii\helpers\Url;
use app\modules\Settings\Settings;

$bundle = SuperFrontAssets::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" style="position:fixed; width: 100vw; height: 100vh" >
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1 , user-scalable=1">
        <!-- FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900;subset=cyrillic-ext,greek-ext,latin-ext" rel="stylesheet">
        <?= Html::csrfMetaTags() ?>
        <link rel="shortcut icon" href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>" type="image/ico">
        <title><?= Html::encode($this->title) ?></title>
        <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>';</script>
        <script>
            function url(url = '') {
                return '<?=Url::to(['/'])?>/'+url;
            }
        </script>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v2.11&appId=500372786991688&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <?php $this->head() ?>
    </head>
    <body >
    <?php $this->beginBody() ?>

    <?=$content?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>