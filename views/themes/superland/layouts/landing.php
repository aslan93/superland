<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\SuperFrontAssets;
use yii\helpers\Url;
use app\modules\Settings\Settings;

$bundle = SuperFrontAssets::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" id="landing">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1 , user-scalable=1">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900;subset=cyrillic-ext,greek-ext,latin-ext" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>
    <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>';</script>
    <script>
        function url(url = '') {
            return '<?=Url::to(['/'])?>/'+url;
        }
    </script>
    <?php $this->head() ?>
</head>
<body class="">
<?php $this->beginBody() ?>

<?=$content?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
