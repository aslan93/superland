<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\SuperFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use modernkernel\flagiconcss\Flag;
use app\models\Article\Article;
use yii\widgets\Menu;
use app\modules\Hotel\models\Hotel;

$bundle = SuperFrontAssets::register($this);

$contact_page = Article::find()->with(['lang', 'link'])->where(['ID' => 34])->one();
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1 , user-scalable=1">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900;subset=cyrillic-ext,greek-ext,latin-ext" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>" type="image/ico">
    <title><?= Html::encode($this->title) ?></title>

    <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>';</script>
    <script>
        function url(url = '') {
            return '<?=Url::to(['/'])?>/'+url;
        }
    </script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v2.11&appId=500372786991688&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <?php $this->head() ?>
</head>
<body class="">
<?php $this->beginBody() ?>

<header>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-4 md-text-left">
                    <ul class="xs-hide">
                        <li>
                            <a href="<?=Url::to(['/contact'])?>">
                                <?=\Yii::t('app','Talk to us')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?=Url::to(['/join-us'])?>"><?=\Yii::t('app','Sign up')?></a> /
                            <a href="<?=Url::to(['/login'])?>"><?=\Yii::t('app','Log in')?></a>
                        </li>
                        <li>
                            <a  href="<?=Url::to(['/'])?>"><?=\Yii::t('app','Landing Page')?></a>
                        </li>
                    </ul>
                    <div class="hidden-lg hidden-md hidden-sm mobile-logo">
                        <a href="<?=Url::to(['/'])?>">
                            <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/fixed-header-logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-8 md-text-right">
                    <ul>
                        <li>
                            <a href="#" class="open-search">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 250.313 250.313" style="enable-background:new 0 0 250.313 250.313;" xml:space="preserve">
                                        <g id="Search">
                                            <path style="fill-rule:evenodd;clip-rule:evenodd;" d="M244.186,214.604l-54.379-54.378c-0.289-0.289-0.628-0.491-0.93-0.76
                                                c10.7-16.231,16.945-35.66,16.945-56.554C205.822,46.075,159.747,0,102.911,0S0,46.075,0,102.911
                                                c0,56.835,46.074,102.911,102.91,102.911c20.895,0,40.323-6.245,56.554-16.945c0.269,0.301,0.47,0.64,0.759,0.929l54.38,54.38
                                                c8.169,8.168,21.413,8.168,29.583,0C252.354,236.017,252.354,222.773,244.186,214.604z M102.911,170.146
                                                c-37.134,0-67.236-30.102-67.236-67.235c0-37.134,30.103-67.236,67.236-67.236c37.132,0,67.235,30.103,67.235,67.236
                                                C170.146,140.044,140.043,170.146,102.911,170.146z"/>
                                        </g>
                                    </svg>
                            </a>
                            <div class="search-box">
                                <form action="<?=Url::to(["/result"])?>" method="get">
                                    <label>
                                        <?=  \yii\jui\AutoComplete::widget([
                                            'options'=>[
                                                'placeholder'=>Yii::t('app','Cautare'),
                                            ],
                                            'clientOptions' => [
                                                'source' => \yii\helpers\Url::to(['/ajax/search/']),
                                                'autoFill'=>true,
                                                'minLength'=>'3',
                                                'select' => new \yii\web\JsExpression("function( event, ui ) {
                                                    $('#search-slug').val(ui.item.id);
                                                 }")
                                            ],
                                        ]);
                                        ?>
                                        <div class="hidden">
                                            <input type="text" id="search-slug" name="slug" required="required">
                                        </div>

                                        <div class="">
                                            <button type="submit">
                                                <span class="fa fa-check"></span>
                                            </button>
                                        </div>
                                    </label>
                                </form>
                            </div>
                        </li>
                        <li>
                            <?php foreach (Yii::$app->params['siteLanguages'] as $key) { ?>
                                <a href="<?= Url::current(['language' => $key]) ?>">
                                  <?=strtoupper($key)?>
                                </a>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <div class="row remove-padding">
                <div class="col-md-2 col-sm-2 hidden-xs">
                    <div class="logo">
                        <a href="<?=Url::to(['/site'])?>">
                            <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/superland_logo_castel.png" alt="">
                        </a>
                    </div>
                    <div class="different-logo">
                        <a href="<?=Url::to(['/site'])?>">
                        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/fixed-header-logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <div class="open-menu hidden-lg hidden-md hidden-sm">
                        <a href="#">
                            <span class="fa fa-navicon"></span>
                        </a>
                    </div>
                    <nav class="hide-on-mobile">
                        <ul>
                            <li class="hidden-lg hidden-md hidden-sm">
                                <a href="<?=Url::to(['/contact'])?>">
                                    <?=\Yii::t('app','Talk to us')?>
                                </a>
                            </li>
                            <li class="hidden-lg hidden-md hidden-sm">
                                <a href="<?=Url::to(['/join-us'])?>"><?=\Yii::t('app','Sign up')?></a> /
                                <a href="<?=Url::to(['/login'])?>"><?=\Yii::t('app','Log in')?></a>
                            </li>
                            <li class="hidden-lg hidden-md hidden-sm">
                                <a  href="<?=Url::to(['/'])?>"><?=\Yii::t('app','Landing Page')?></a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['/site/attractions'])?>">
                                    <?=\Yii::t('app','Attractions')?>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['/site/events'])?>">
                                    <?=\Yii::t('app','Events')?>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['/site/food'])?>">
                                    <?=\Yii::t('app','Food&Relax')?>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Url::to(['/site/boutique'])?>">
                                    <?=\Yii::t('app','Boutique Shopping Center')?>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <a href="<?=Url::to(['/buy/ticket1'])?>" class="ticket" style="background-image: none">
                        <img class="by-ticket-bg" src="<?=$bundle->baseUrl?>/images/ticket.png" alt="">
                        <span class="up"><?=\Yii::t('app','Buy ticket')?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<?=$content?>
<footer>
    <div class="container">
        <div class="row remove-padding">
            <div class="col-md-6">
                <div class="company-info" style="background-image: url('<?=$bundle->baseUrl?>/images/map-adress.png'); background-position: center right">
                    <div class="row remove-padding">
                        <div class="col-md-6">
                            <div class="graphic-work">
                                <div class="row remove-padding">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="simple-border hidden-sm hidden-xs">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="mb10">
                                                    <div class="strong-text">
                                                        <?=\Yii::t('app','Location')?>
                                                    </div>
                                                    <div class="light-text">
                                                        <?=Settings::getByName('address')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="simple-border hidden-sm hidden-xs">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="mb10">
                                                    <div class="strong-text">
                                                        <?=\Yii::t('app',"Phone")?>
                                                    </div>
                                                    <div class="light-text">
                                                        <?=Settings::getByName('phone')?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="simple-border hidden-sm hidden-xs">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="mb10">
                                                    <div class="strong-text">
                                                        <?=\Yii::t('app','Working program')?>
                                                    </div>
                                                    <div class="light-text">
                                                        <?=Settings::getByName('grafic',true)?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 hidden-sm hidden-xs">
                            <div class="overlay-address">
                                <a href="#">

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row remove-padding">
                    <div class="col-md-5">
                        <div class="other-links">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="simple-border hidden-sm hidden-xs">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="mb10">
                                        <div class="strong-text">
                                            <?=\Yii::t('app','More links')?>
                                        </div>
                                        <div class="light-text">
                                            <div>
                                                <a href="<?=Url::to(['/contact'])?>">
                                                    <?=\Yii::t('app','Contacts')?>
                                                </a>
                                            </div>
                                            <div>
                                                <a href="<?=Url::to(['/faq'])?>">
                                                    <?=\Yii::t('app','Faq')?>
                                                </a>
                                            </div>
                                            <div>
                                                <a href="<?=Url::to(['/info/blog'])?>">
                                                    <?=\Yii::t('app','Blog')?>
                                                </a>
                                            </div>
                                            <div>
                                                <a href="<?=Url::to(['/info/jobs'])?>">
                                                    <?=\Yii::t('app','Jobs/Careers')?>
                                                </a>
                                            </div>
                                            <div>
                                                <a href="<?=Url::to(['/info/privacy-policy'])?>">
                                                    <?=\Yii::t('app','Provacy Policy')?>
                                                </a>
                                            </div>
                                            <div>
                                                <a href="<?=Url::to(['/info/rules-regulations'])?>">
                                                    <?=\Yii::t('app','Rules & Regulations')?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="social-links">
                            <div class="row remove-padding">
                                <div class="col-md-12">
                                    <div class="row remove-padding">
                                        <div class="col-md-4">
                                            <div class="simple-border hidden-sm hidden-xs">
                                                &nbsp;
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="mb10">
                                                <div class="strong-text">
                                                    <?=\Yii::t('app','Follow us:')?>
                                                </div>
                                                <div>
                                                    <ul>
                                                        <li>
                                                            <a href="<?=Settings::getByName('facebookLink')?>">
                                                            <span class="fa fa-facebook">

                                                            </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?=Settings::getByName('twitterLink')?>">
                                                            <span class="fa fa-twitter">

                                                            </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?=Settings::getByName('instagramLink')?>">
                                                            <span class="fa fa-instagram">

                                                            </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?=Settings::getByName('youtubeLink')?>">
                                                            <span class="fa fa-youtube">

                                                            </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row remove-padding">
                                        <div class="col-md-4">
                                            <div class="simple-border hidden-sm hidden-xs">
                                                &nbsp;
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="strong-text">
                                                <?=\Yii::t('app','Join our newsletter')?>
                                            </div>
                                            <div class="subscribe">
                                                <?=\app\modules\Feedback\components\FeedbackWidgetSimple\FeedbackWidgetSimple::widget()?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="text-center">
                <div class="short-info">
                    <?=\Yii::t('app','Acest site foloseste cookies. Prin navigarea pe acest site, va
                    exprimati acordul asupra folosirii cookie-urilor.')?>

                    <a href="#"><?=\Yii::t('app','Citeste mai mult')?></a>
                </div>
                <div class="copyright-text">
                    © Copyright - 2017 <br />
                    Superland
                </div>
            </div>
        </div>
    </div>
</footer>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
