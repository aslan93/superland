$(document).ready(function(){
    /* CALLBACK FOR TOP VIDEO AND TOP VIDEO */
    var video = $(".parent-presentation video").get(0);
    $(".play-video").click(function() {
        $("#modal-video").modal("show");
        video.pause();
        return false;
    });
    $("#modal-video .close").click(function(){
        video.play();
        return true;
    });
    /* FIXED HEADER  ON SCROLL TOP*/
    $(document).scroll(function() {
        if ($(window).scrollTop() >= 34) {
            $(".header-menu").css({
                "position" : "fixed",
                "top" : "0px",
                "left" : "0px" ,
                "width" : "100%" ,
                "z-index" : "400"
            });
            $(".header-menu").find(".logo").fadeOut();
            $(".header-menu").find(".different-logo").fadeIn().addClass("animated bounceIn");
        } else {
            $(".header-menu").css({
                "position" : "static"
            })
            $(".header-menu").find(".logo").fadeIn().addClass("animated flipInY");
            $(".header-menu").find(".different-logo").fadeOut().removeClass("animated bounceIn");
        }
    });
    /* POST SLIDER */
    var swiper = new Swiper('.posts-slider.slider', {
        slidesPerView: 'auto',
        freeMode: true,
        spaceBetween: 30,
        autoplay: true,
        speed: 600,
        pagination: {
            el: '.posts-pagination',
            clickable: true
        },
        breakpoints: {
            480: {
              slidesPerView: 1,
              spaceBetween: 10
            },
            // when window width is <= 640px
            992: {
              slidesPerView: 2,
              spaceBetween: 10
            },
            1100: {
                slidesPerView: 'auto',
                freeMode: true,
                spaceBetween: 30,
            }
        }
    });

    /* SELECT 2 INIT */
    $(".style-select").select2();

    /* TOGGLE ANSWER FAQ PAGE */
    $(".questions-group").click(function(){
        $(".answer").slideUp();
        $(this).find(".answer").slideToggle();
    });

    /* STYLE CHECKBOX & RADIO BOX */
    $(".style-input").styler();

    /* CALENDAR SLIDER */
    var swiper = new Swiper(".calendar", {
        slidesPerView: 31,
        breakpoints: {
            568: {
                slidesPerView: 6,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 12,
                spaceBetween: 10
            },
            1080: {
                slidesPerView: 15,
                spaceBetween: 0
            }
        }
    });

    $("a[data-date]").click(function(e){
        e.preventDefault();
        var index =  $(".calendar .swiper-slide").index($(".calendar .swiper-slide[data-date='" +  $(this).attr("data-date") + "']"));
        swiper.slideTo(index , 1000);
    });

    if($('.calendar').length) {
        var index = $(".calendar .swiper-slide").index($(".calendar .swiper-slide[data-date='" + $(".calendar-tabs .active a").attr("data-date") + "']"));
        swiper.slideTo(index, 1000);
    }

    //modificat de Dev

    $(".swiper-slide .day").click(function(){
        $(".swiper-slide .day").removeClass("checked");
        $(this).addClass("checked");
    });


    //*modificat de Dev

    $(".calendar-tabs li a").click(function(){
        $(".calendar-tabs li").removeClass("active");
        $(this).closest("li").addClass("active");
    });

    /* AJAX POST MODAL */
    $("#post-modal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });

    $(".category-posts li").click(function(e){
        e.preventDefault();
        $(".category-posts li").removeClass("active");
        $(this).addClass("active");
    });

    $(".sort-attractions li").click(function(e){
        e.preventDefault();
        $(".sort-attractions li").removeClass("active");
        $(this).addClass("active");
    });


    var $grid = $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 285,
        transitionDuration: '0.6s'
    });

    $(window).resize(function(){
        if($(window).width() < 601){
            $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: 200,
                transitionDuration: '0.6s'
            });
        }
        else if($(window).width() < 505){
            $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-item',
                transitionDuration: '0.6s'
            });
        }
    });

    if($(window).width() < 601){
        $grid.masonry({
            itemSelector: '.grid-item',
            columnWidth: 200,
            transitionDuration: '0.6s'
        });
    }
    else if($(window).width() < 505){
        $grid.masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-item',
            transitionDuration: '0.6s'
        });
    }

    $grid.on( 'click', '.grid-item', function(e) {
        $grid.masonry({
            itemSelector: '.grid-item',
            columnWidth: 285,
            transitionDuration: '0.6s'
        });
        $(window).resize(function(){
            if($(window).width() < 601){
                $grid.masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 200,
                    transitionDuration: '0.6s'
                });
            }
            else if($(window).width() < 505){
                $grid.masonry({
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item',
                    transitionDuration: '0.6s'
                });
            } 
        });
        if($(window).width() < 601){
            $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: 200,
                transitionDuration: '0.6s'
            });
        }
        else if($(window).width() < 505){
            $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-item',
                transitionDuration: '0.6s'
            });
        }
    });


    function flipBox(){
        $(".grid-item--width2").addClass("animated flipInY");
        $(".grid-item--width2").find(".attraction-box").find(".front").fadeOut().slideUp();
        $(".grid-item--width2").find(".attraction-box").find(".back").fadeIn().slideDown();
    }
    $(document).on("click",".grid-item .front",function(){

        /* ALL BLOCK */
        $(this).closest(".grid-item").removeClass("animated zoomInUp");
        $(".grid-item").css({
            "opacity" : "0.5"
        });

        /* CURENT BLOCK */
        $(".grid-item").addClass("inactive");
        $(this).closest(".grid-item").removeClass("inactive");
        $(this).addClass("fliped");
        $(this).closest(".grid-item").css({
            "opacity" : "1"
        });
        $(this).closest(".attraction-box").find(".img-box").find(".small-img").css({
            width: "100%",
            height: "auto"
        });
        $(this).closest(".attraction-box").find(".name-box").hide();
        $(this).closest(".attraction-box").css({"display" : "flex" ,  "flex-wrap" : "wrap", "justify-content" : "center" , "align-items": "center" , "text-align" : "center"})
            .find(".img-box").css({"height" : "auto"});
        $(this).closest(".grid-item").addClass('grid-item--width2 grid-item--height2');
        $(this).closest(".attraction-box").css({
            "height" : "100%"
        });
        setTimeout(flipBox , 600);
        $(window).resize(function(){
            if($(window).width() < 601){
                $grid.masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 200,
                    transitionDuration: '0.6s'
                });
            }
            else if($(window).width() < 505){
                $grid.masonry({
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item',
                    transitionDuration: '0.6s'
                });
            }
        });
        if($(window).width() < 601){
                $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: 200,
                transitionDuration: '0.6s'
            });
        }
        else if($(window).width() < 505){
            $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-item',
                transitionDuration: '0.6s'
            });
        }
        $grid.masonry({
            itemSelector: '.grid-item',
            columnWidth: 285,
            transitionDuration: '0.6s'
        });
    });

    $(document).on("click",".close-flip",function(){
        $(this).closest(".grid-item").removeClass("animated flipInY");
        $(".grid-item--width2").find(".attraction-box").find(".back").fadeOut().slideUp();
        $(".grid-item--width2").find(".attraction-box").find(".front").fadeIn().slideDown();
        $(".grid-item").css({
            "opacity": "1"
        });
        $(".grid-item").find(".img-box").css({
            "height" : "173px"
        });
        $(".grid-item--width2").find(".attraction-box").find(".img-box").find(".small-img").css({
            width: "50%",
            height: "auto"
        });
        $(".grid-item--width2").find(".attraction-box").css({"height" : "100%"}).closest(".grid-item").removeClass("grid-item--width2 grid-item--height2");
        $(this).closest(".attraction-box").find(".name-box").fadeIn().slideDown();
        $(this).closest(".grid-item").addClass("animated zoomInUp");
        $(".grid-item").removeClass("inactive");
        $grid.masonry({
            itemSelector: '.grid-item',
            columnWidth: 285,
            transitionDuration: '0.6s'
        });

        $(window).resize(function(){
            if($(window).width() < 601){
                $grid.masonry({
                    itemSelector: '.grid-item',
                    columnWidth: 200,
                    transitionDuration: '0.6s'
                });
            }
            else if($(window).width() < 505){
                $grid.masonry({
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-item',
                    transitionDuration: '0.6s'
                });
            }
        });

        if($(window).width() < 601){
                $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: 200,
                transitionDuration: '0.6s'
            });
        }
        else if($(window).width() < 505){
            $grid.masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-item',
                transitionDuration: '0.6s'
            });
        }

    });


    /* SET VALUE FOR TYPE PACKAGE */
    $(".big-check-box[data-value='kids']").click(function(){
       $(this).find("input[type='text']").val("kids");
    });
    $(".big-check-box[data-value='parent']").click(function(){
        $(this).find("input[type='text']").val("parent");
    });
    /* SET ACTIVE CLASS FOR BIG-CHECK-BOX */
    $(".big-check-box").click(function(){
       $(".big-check-box").removeClass("active");
       $(this).addClass("active");
    });

    /* DATE PICKER BOOTSTRAP */
    if($("#sandbox-container").length > 0){
        var date = new Date();
        date.setDate(date.getDate());
        $('#sandbox-container div').datepicker({
            todayHighlight: true,
            startDate: date,
            format: 'yyyy-mm-dd',
        });
    }

    if($(".date").length > 0){
        $(window).trigger('resize');
        $('input.date').datepicker({
            todayHighlight: true
        });
    }

    /* TIME PICKER */
    if($(".clockpicker").length > 0){
        $(window).trigger('resize');
        $('.clockpicker').clockpicker({
            placement: 'top',
            align: 'left',
            donetext: 'Done',

        });
    }

    $("#open-clock").trigger('click');

    $(".room-wrap").not(".room-wrap .disabled").click(function(){
       $(".room-wrap").removeClass("active");
        $(".room-wrap.disabled").removeClass("active");
       $(this).addClass("active");
    });

    /* CHOOSE MENU ADD CLASS CHECKED FOR MENU ITEM IF INPUT IS CHECKED */
    $(".item-menu label").click(function(){
       $(this).closest(".item-menu").toggleClass("checked");
    });

    /* MINUS PLUS PLUGIN */
    $('.btn-number').click(function(e){
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {

                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });

    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });

    $('.input-number').change(function() {

        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });

    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(window).resize(function(){
       if($(window).width() < 992){
           $(".more-info .animate-img.first").css({
               "transform" : "none",
               "position" : "static"
           });

           $(".more-info .animate-img.last").css({
               "transform" : "none",
               "position" : "static"
           });
       }
    });

    if($(window).width() < 992){
        $(".more-info .animate-img.first").css({
            "transform" : "none",
            "position" : "static"
        });

        $(".more-info .animate-img.last").css({
            "transform" : "none",
            "position" : "static"
        });
    }

    $(".by-ticket-bg").addClass("animated pulse");

    $(".big-check-box-bg").mousemove(function(e){
        var change;
        var xpos=e.clientX;
        var ypos=e.clientY;
        var left= change * 30;
        var  xpos= xpos * 2 ;
        ypos = ypos * 2;
        $(this).find(".category-box-big-bg").css('top',((0+(ypos/55))+"px"));
        $(this).find(".category-box-big-bg").css('left',((0+(xpos/75))+"px"));
    });

    $(".category-box-big").mousemove(function(e){
        var change;
        var xpos=e.clientX;
        var ypos=e.clientY;
        var left= change * 30;
        var  xpos= xpos * 2 ;
        ypos = ypos * 2;
        $(this).find(".category-box-big-bg").css('top',((0+(ypos/55))+"px"));
        $(this).find(".category-box-big-bg").css('left',((0+(xpos/75))+"px"));
    });

    $(".big-check-box").mousemove(function(e){
        var change;
        var xpos=e.clientX;
        var ypos=e.clientY;
        var left= change * 30;
        var  xpos= xpos * 2 ;
        ypos = ypos * 2;
        $(this).find(".big-check-box-bg").css('top',((0+(ypos/55))+"px"));
        $(this).find(".big-check-box-bg").css('left',((0+(xpos/75))+"px"));
    });

    $(".category-wrap.parent").mousemove(function(e){
        var change;
        var xpos=e.clientX;
        var ypos=e.clientY;
        var left= change * 30;
        var  xpos= xpos * 2 ;
        ypos = ypos * 2;
        $(this).find(".wrap-parallax").css('top',((-20+(ypos/55))+"%"));
        $(this).find(".wrap-parallax").css('right',((-62+(xpos/105))+"%"));
    });

    $(".content-fixed").mousemove(function(e){
        var change;
        var xpos=e.clientX;
        var ypos=e.clientY;
        var left= change * 30;
        var  xpos= xpos * 2 ;
        ypos = ypos * 2;
        $(this).find(".left-parallax-wrap .first-elem").css('bottom',((-18+(ypos/85))+"px"));
        $(this).find(".left-parallax-wrap .first-elem").css('left',((-40+(xpos/97))+"%"));
        $(this).find(".left-parallax-wrap .second-elem").css('bottom',((0+(ypos/68))+"px"));
        $(this).find(".left-parallax-wrap .second-elem").css('left',((-10+(xpos/83))+"%"));
        $(this).find(".left-parallax-wrap .third-elem").css('bottom',((20+(ypos/48))+"px"));
        $(this).find(".left-parallax-wrap .third-elem").css('left',((-45+(xpos/63))+"%"));
        $(this).find(".left-parallax-wrap .fourth-elem").css('bottom',((70+(ypos/62))+"px"));
        $(this).find(".left-parallax-wrap .fourth-elem").css('left',((-32+(xpos/88))+"%"));
        $(this).find(".left-parallax-wrap .fifth-elem").css('bottom',((47+(ypos/68))+"px"));
        $(this).find(".left-parallax-wrap .fifth-elem").css('left',((-8+(xpos/81))+"%"));
        $(this).find(".left-parallax-wrap .sixth-elem").css('bottom',((75+(ypos/71))+"px"));
        $(this).find(".left-parallax-wrap .sixth-elem").css('left',((-10+(xpos/57))+"%"));
        $(this).find(".left-parallax-wrap .seventh-elem").css('bottom',((8+(ypos/46))+"px"));
        $(this).find(".left-parallax-wrap .seventh-elem").css('left',((6+(xpos/78))+"%"));

        $(this).find(".right-parallax-wrap .first-elem").css('bottom',((-25+(ypos/85))+"%"));
        $(this).find(".right-parallax-wrap .first-elem").css('right',((-80+(xpos/97))+"%"));
        $(this).find(".right-parallax-wrap .second-elem").css('bottom',((0+(ypos/78))+"%"));
        $(this).find(".right-parallax-wrap .second-elem").css('right',((-60+(xpos/83))+"%"));
        $(this).find(".right-parallax-wrap .third-elem").css('bottom',((-50+(ypos/48))+"%"));
        $(this).find(".right-parallax-wrap .third-elem").css('right',((-75+(xpos/73))+"%"));
        $(this).find(".right-parallax-wrap .fourth-elem").css('bottom',((0+(ypos/62))+"%"));
        $(this).find(".right-parallax-wrap .fourth-elem").css('right',((-32+(xpos/88))+"%"));
        $(this).find(".right-parallax-wrap .fifth-elem").css('bottom',((11+(ypos/68))+"%"));
        $(this).find(".right-parallax-wrap .fifth-elem").css('right',((-5+(xpos/81))+"%"));
        $(this).find(".right-parallax-wrap .sixth-elem").css('bottom',((25+(ypos/91))+"%"));
        $(this).find(".right-parallax-wrap .sixth-elem").css('right',((10+(xpos/67))+"%"));
        $(this).find(".right-parallax-wrap .seventh-elem").css('bottom',((-5+(ypos/56))+"%"));
        $(this).find(".right-parallax-wrap .seventh-elem").css('right',((-5+(xpos/88))+"%"));
    });

    $(".category-wrap.children").mousemove(function(e){
        var change;
        var xpos=e.clientX;
        var ypos=e.clientY;
        var left= change * 30;
        var  xpos= xpos * 2 ;
        ypos = ypos * 2;
        $(this).find(".wrap-parallax").css('top',((-20+(ypos/55))+"%"));
        $(this).find(".wrap-parallax").css('left',((-60+(xpos/105))+"%"));
    });

    $(".big-check-box").mouseenter(function(){
        $(this).find(".number-package").addClass("animated pulse");
    });

    $(".big-check-box").mouseout(function(){
       $(this).find(".number-package").removeClass("animated pulse");
    });

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $(".attraction-box").hover(
        function(){
            $(this).find(".front .img-box").removeClass("bounceOut").addClass("animated pulse")
        },
        function () {
            $(this).find(".front .img-box").removeClass("animated pulse");
        }
    );

    $(".stars-wrap").each(function(){
        $( '.stars-img' ).each(function( index ) {
            $(this).css({
                position: "absolute",
                left : ((Math.random() * $('.stars-wrap').width())),
                top : ((Math.random() * $('.stars-wrap').height()))
            });
        });
    });
    $(".package-big-box").click(function(){
        $(".stars-wrap").each(function(){
            $( '.stars-img' ).each(function( index ) {
                $(this).css({
                    position: "absolute",
                    left : ((Math.random() * $('.stars-wrap').width())),
                    top : ((Math.random() * $('.stars-wrap').height()))
                });
            });
        });
       $(".package-big-box").find(".stars-wrap").removeClass("animated bounceIn").css({"display" : "none"});
       $(this).find(".stars-wrap").addClass("animated bounceIn").css({"display": "block"});
    });


    $(".open-search").click(function(event){
        event.preventDefault();
        $(this).closest("li").find(".search-box").toggleClass("animated flipInX active");
    });


    $("input.clock").timepicki({
        min_hour_value: 0,
        max_hour_value: 23,
        show_meridian: false
    });
    $("input.clock").focus();

    $('[data-toggle="tooltip"]').tooltip();

    $(".submenu-hide").click(function(e){
        e.preventDefault();
        $(this).closest(".top-header").find("ul.xs-hide").slideToggle();
    });

    $(".header-menu .open-menu").click(function(e){
        e.preventDefault();
        $(this).closest(".header-menu").find("nav.hide-on-mobile").slideToggle();
    });

    $(window).resize(function(){
        if($(window).width() < 992){
            $("div.remove-padding").removeClass("remove-padding");
        }
    });

    if($(window).width() < 992){
        $("div.remove-padding").removeClass("remove-padding");
    }

    $(document).on("hidden.bs.modal" , function(){
        $("#landingbody ,#landing").toggleClass("no-scroll");
    });

    if($(window).width() < 992){
        $(".info-wrap.coockies").find(".elem-wrap").css({
           "justify-content" : "center",
        });
    }

    $(window).resize(function(){
        if($(window).width() < 992){
            $(".info-wrap.coockies").find(".elem-wrap").css({
                "justify-content" : "center",
            });
        }
    });

    $(document).on('click' , ".img-thumb" ,function(){
       var imgsrc =$(this).find("img").attr('src'); 
       $(this).closest(".post-info").find(".img-post a").attr('href' , imgsrc);   
       $(this).closest(".post-info").find(".img-post img").attr('src' , imgsrc);   
    });

});

 function removeElement(ob) {
    $(ob).hide();
}