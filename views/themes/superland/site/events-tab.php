<?php
    use yii\helpers\Html;
    use app\assets\SuperFrontAssets;
    use yii\widgets\Pjax;
    use yii\widgets\ListView;

    $bundle = SuperFrontAssets::register($this);
$this->title = 'Superland Events Tab';
?>


<section class="front-presentation">
    <div class="parent-presentation">
        <div class="video-overlay">

        </div>
        <video controls poster="<?=$bundle->baseUrl?>/images/presentation-bg.jpg" class="banner__video" autoplay="false" muted="muted">
            <source src="<?=$bundle->baseUrl?>/images/video/happy-kids.mp4" type="video/mp4">
            Your browser doesn't support HTML5 video tag.
        </video>
        <div class="content">
            <div class="big-text">
                Create
            </div>
            <div class="small-text">
                your
            </div>
            <div class="big-text">
                dream!
            </div>
            <a href="#" class="play-video" data-target="#modal-video">
                <svg class="spin" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M477.606,128.055C443.431,68.863,388.251,26.52,322.229,8.83C256.208-8.862,187.25,0.217,128.055,34.394
                                        C68.861,68.57,26.52,123.75,8.83,189.772c-17.69,66.021-8.611,134.981,25.564,194.173
                                        C68.568,443.137,123.75,485.48,189.771,503.17c22.046,5.908,44.417,8.83,66.646,8.83c44.339,0,88.101-11.629,127.529-34.393
                                        c59.192-34.175,101.535-89.355,119.225-155.377C520.862,256.207,511.781,187.249,477.606,128.055z M477.429,315.333
                                        c-15.848,59.146-53.78,108.581-106.81,139.197c-53.028,30.617-114.806,38.749-173.952,22.903
                                        c-59.147-15.848-108.581-53.78-139.198-106.81c-30.616-53.028-38.749-114.807-22.9-173.954
                                        C50.418,137.523,88.35,88.09,141.379,57.472c35.325-20.395,74.524-30.812,114.249-30.812c19.91,0,39.959,2.618,59.702,7.909
                                        c59.146,15.848,108.581,53.78,139.197,106.81C485.144,194.408,493.278,256.186,477.429,315.333z"/>
                                </g>
                            </g>
                    <g>
                        <g>
                            <path d="M413.303,134.44c-31.689-40.938-79.326-68.442-130.698-75.461c-7.283-0.997-14.009,4.106-15.006,11.399
                                        c-0.995,7.291,4.108,14.009,11.399,15.006c44.512,6.081,85.783,29.909,113.232,65.369c2.626,3.392,6.565,5.168,10.546,5.168
                                        c2.849,0,5.72-0.909,8.146-2.789C416.741,148.628,417.807,150.259,413.303,134.44z"/>
                        </g>
                    </g>
                        </svg>
                <button class="play-button" data-toggle="modal" data-target="#modal-video">
                    <svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                          viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <path d="M378.778,231.852l-164.526-94.99c-8.731-5.041-19.155-5.039-27.886-0.001c-8.731,5.04-13.944,14.069-13.944,24.15v189.98
                                        c0,10.081,5.212,19.109,13.944,24.15c4.365,2.521,9.152,3.78,13.941,3.78c4.79,0,9.579-1.262,13.944-3.781l164.528-94.989
                                        c8.73-5.042,13.941-14.07,13.941-24.151C392.72,245.92,387.508,236.892,378.778,231.852z M365.452,257.074l-164.527,94.989
                                        c-0.201,0.117-0.62,0.358-1.236,0c-0.618-0.357-0.618-0.839-0.618-1.071v-189.98c0-0.232,0-0.714,0.618-1.071
                                        c0.242-0.14,0.453-0.188,0.633-0.188c0.28,0,0.482,0.117,0.605,0.188l164.526,94.99c0.201,0.116,0.618,0.357,0.618,1.071
                                        C366.071,256.716,365.652,256.958,365.452,257.074z"/>
                            </g>
                            </svg>
                </button>
            </a>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="video-overlay">

            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 54 54" enable-background="new 0 0 54 54" xml:space="preserve">
                              <path fill="#FFFFFF" d="M27,4c12.7,0,23,10.3,23,23S39.7,50,27,50S4,39.7,4,27S14.3,4,27,4 M27,2C13.2,2,2,13.2,2,27s11.2,25,25,25 s25-11.2,25-25S40.8,2,27,2L27,2z"/>
                              <line fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.1" y1="19.9" x2="19.9" y2="34.1"/>
                              <line fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.1" y1="34.1" x2="19.9" y2="19.9"/>
                        </svg>
                    </span></button>
            <video controls poster="<?=$bundle->baseUrl?>/images/presentation-bg.jpg" class="banner__video" autoplay muted="muted">
                <source src="<?=$bundle->baseUrl?>/images/video/happy-kids.mp4" type="video/mp4">
                Your browser doesn't support HTML5 video tag.
                <div id="video-controls" class="controls" data-state="hidden">
                    <button id="playpause" type="button" data-state="play">Play/Pause</button>
                    <button id="stop" type="button" data-state="stop">Stop</button>
                    <div class="progress">
                        <progress id="progress" value="0" min="0">
                            <span id="progress-bar"></span>
                        </progress>
                    </div>
                    <button id="mute" type="button" data-state="mute">Mute/Unmute</button>
                    <button id="volinc" type="button" data-state="volup">Vol+</button>
                    <button id="voldec" type="button" data-state="voldown">Vol-</button>
                    <button id="fs" type="button" data-state="go-fullscreen">Fullscreen</button>
                </div>
            </video>
        </div>
    </div>
</section>

<section class="category_interesting">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="<?=\yii\helpers\Url::to(['/attractions'])?>">
                <div class="category-box-big red-border" data-mh="1"
                     style="background-image: url(<?=$bundle->baseUrl?>/images/children.png) ; background-repeat: no-repeat ; background-size: auto auto ; background-position: 20px -25px">
                    <div class="name-category md-text-right">
                        <?=Yii::t('app','Entertainment')?>
                    </div>
                </div>
                </a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="<?=\yii\helpers\Url::to(['/food'])?>">
                <div class="category-box-big blue-border" data-mh="1"
                     style="background-image: url(<?=$bundle->baseUrl?>/images/parent-and-children.png) ; background-repeat: no-repeat ; background-size: auto auto ; background-position: 20px 5px">
                    <div class="name-category  md-text-right">
                        <?=Yii::t('app','Shop')?>
                    </div>
                </div>
                </a>
            </div>
        </div>
    </div>
</section>
<?php

// Month & Days
$months = [];
$days = [];
$curYear = (int) date("Y");
$curMonth = (int) date("m");
$search_dates = isset($_GET['date']) ? explode(",", $_GET['date']) : [date("d.m.Y")];
$search_returns = isset($_GET['return']) ? explode(",", $_GET['return']) : [];
$searchMonth = (count($search_dates) > 0) ? date("m", strtotime($search_dates[0])) : $curMonth;
$searchMonthReturn = (count($search_returns) > 0) ? date("m", strtotime($search_returns[0])) : $curMonth;

for($i=$curMonth-5;$i<=$curMonth + 5;$i++){

    if($i <= 12 && $i > 0){
        $month = $i;
        $year = $curYear;
    }elseif((int)$i <= 0){
        $month = $i + 12;
        $year = $curYear-1;
    }else{
        $month = $i - 12;
        $year = $curYear + 1;
    }

    $months[] = $month;
    for($d=1; $d<=31; $d++)
    {
        $time=mktime(12, 0, 0, $month, $d, $year);
        if (date('m', $time)==$month){

            $event = \app\modules\Events\models\Events::getDb()->cache(function ($db)use($time) {
                return  \app\modules\Events\models\Events::find()->where(['Date'=>date('Y-m-d', $time)])->all();
            },3600, new \yii\caching\TagDependency(['tags' => \app\modules\Events\models\Events::className()]));

            $days[]= [
                'specialDate'=>date('Y-m-d', $time),
                'event' => $event ? 'event' : '',
                'date' => date('d.m.Y', $time),
                'slide' => date('m.d', $time),
                'd' => date('d', $time),
                'D' => date('D', $time),
                'check' => in_array(date('d.m.Y', $time), $search_dates),
                'check-return' => in_array(date('d.m.Y', $time), $search_returns),
            ];
        }
    }
}

?>
<?php $form = \kartik\form\ActiveForm::begin([
    'action'=>'',
    'method' => 'post',
    'id'=>'main-form',
]); ?>

<?= $form->field($searchModel, "Date")->input('text',['class'=>'date-input hidden'])->label(false)?>
<?= $form->field($searchModel, "Type")->input('text',['class'=>'type-input hidden'])->label(false)?>
<?= $form->field($searchModel, "ID")->input('text',['class'=>'id-input hidden','value'=>''])->label(false)?>
<section class="post-filter">
    <div class="container">
        <div class="calendar-tabs" >
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <?php foreach($months as $month){ ?>
                    <li role="presentation" <?php if($month==$searchMonth){ ?>class="active"<?php } ?>>
                        <a href="#" data-date="<?= (int)$month ?>">
                            <?= \Yii::t('app', "month_" . $month)?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="calendar-checkbox" >
            <div role="tabpanel" class="tab-pane active" id="ianuarie">
                <div class="swiper-container calendar first">
                    <div class="swiper-wrapper">
                        <?php foreach($days as $day){ ?>
                            <div class="swiper-slide" data-date="<?= (int)$day['slide'] ?>" data-value="<?= $day['specialDate'] ?>" onclick="submitForm('<?=$day['specialDate']?>','Data')">
                                <div class="day <?= $day['event'] ?>"><?= $day['d'] ?></div>
                                <div class="name-day"><?= \Yii::t('app', "cal_" . strtolower($day['D']))?></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br>
        <div class="text-center" >
            <div class="category-posts">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" id="view">
                    <li role="presentation">
                        <a href="" onclick="submitForm('','Type')">
                            <?=Yii::t('app','ALL')?>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#" onclick="submitForm('<?=\app\modules\Events\models\Events::TypeKids?>','Type')" >
                            <?=Yii::t('app','FOR KIDS')?>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#" onclick="submitForm('<?=\app\modules\Events\models\Events::TypeParents?>','Type')">
                            <?=Yii::t('app','FOR PARENTS')?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php \kartik\form\ActiveForm::end(); ?>
<section class="result-posts">
    <!-- AJAX POST MODAL-->
    <div class="modal fade" id="post-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog full-screen" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                    </button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <!-- AJAX gallery MODAL-->
    <div class="modal fade" id="gallery-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog full-screen" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                    </button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <?php
            Pjax::begin([
                'timeout' => 10000,
                'id' => 'events',
            ])
            ?>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                'itemView' => '_event_item',

            ]);
            ?>
            <?php
            Pjax::end();
            ?>
        </div>
    </div>
</section>
<section class="gallery">
    <div class="container">
        <div class="name-section">
            <?=Yii::t('app','Gallery from past events')?>
        </div>
        <div class="gallery-grid">
            <div class="row">
                <?php
                foreach ($galleries as $event) {
                    ?>
                    <div class="col-md-2 col-sm-6 col-xs-6">
                        <div class="gallery-item">
                            <a class="open-modal-gallery" onclick="getElem('<?=\yii\helpers\Url::to(['/ajax/get-gallery','id'=>$event->ID])?>')" >
                                <?php
                                if ($event->image) {
                                    ?>
                                    <img src="<?= $event->image ?>" alt="">
                                    <?php
                                }
                                ?>
                                <div class="date-post">
                                    <?php
                                    if ($event->event){
                                        ?>
                                        <?= $event->event->niceDate ?>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>

<?php $this->registerJs(
    "   function submitForm(data,type){
            if(type == 'Type'){
                $('.type-input').val(data);
              }else{
                $('.date-input').val(data);
              }
                var form = $('#main-form');
                var formData = form.serialize();
                $.pjax.reload({container: '#events',data: formData, push:false,replace:false,timeout:10000});
            }
            
         function resetCat(){
            $('.categories a').removeClass('active');
            submitForm('','Category',null);
         }
         function resetBrand(){
            $('.brands a').removeClass('active');
            $('.brand').val('');
            submitForm('','Brand',null);
            
         }
         function resetPrice(){
            
            $('#keyboard').val($('#keyboard').attr('data-min'));
            $('#keyboard2').val($('#keyboard2').attr('data-max'));
            submitForm();
            
         }
         $(document).on('pjax:end','#catalog',function(){
            $('.product-box').matchHeight();
         });
         
         if($('.summary').length > 0){
            $('.result-product').html($('.summary').html());
            }else{
            $('.result-product').html('');
         }    
         
         function getElem(url){
             $('#gallery-modal .modal-body').empty();                 
             $.ajax({
                  url: url,
                }).done(function(data) {
                 $('#gallery-modal .modal-body').html(data);
                 $('#gallery-modal').modal();
                 $('#gallery-modal').on('shown.bs.modal', function(){
                         var galleryTop = new Swiper('#gallery-modal .gallery-top', {
                              spaceBetween: 10,
                              navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                              },
                            });
                            var galleryThumbs = new Swiper('#gallery-modal .gallery-thumbs', {
                              spaceBetween: 10,
                              centeredSlides: true,
                              slidesPerView: 'auto',
                              touchRatio: 0.2,
                              slideToClickedSlide: true,
                            });
                            galleryTop.controller.control = galleryThumbs;
                            galleryThumbs.controller.control = galleryTop;
                    });
                   
                }); 
         }
            
         $(document).on('click','.more-details',function(){
                 $('.modal-body').empty(); 
                     
            });
         
         
            "
    , \yii\web\View::POS_END);
?>

