<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;
use kartik\form\ActiveForm;

$bundle = SuperFrontAssets::register($this);
$this->title = 'Superland Login';
?>

<div class="name-page" style="min-height: 544px">
    <img class="parallax-img" src='<?=$bundle->baseUrl?>/images/name-page-bg.jpg' alt="" style="top: -160%">
</div>

<section class="log-in">
    <div class="container">
        <div class="join-form">
            <div class="row remove-padding">
                <div class="col-md-6">
                    <div class="left-box" data-mh="3">
                        <?php

                            ?>
                            <?php $form = ActiveForm::begin([
                                'action' => '',
                                'method' => 'post',
                                'id' => 'login',
                                'fieldConfig' => [
                                    'options' => [
                                        'tag' => false,
                                    ]
                                ],

                            ]); ?>
                            <div class="big-title">
                                Login
                            </div>
                            <div>
                                <label>
                                    <?= $form->field($model, 'Email')->input('email', ['maxlength' => true, 'class' => '', 'placeholder' => 'Email'])->label(false) ?>
                                </label>
                            </div>

                            <div>
                                <label>
                                    <?= $form->field($model, 'Password')->passwordInput(['maxlength' => true, 'class' => '', 'placeholder' => 'Password'])->label(false) ?>
                                </label>
                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="confirm-terms">
                                            <input type="checkbox" class="style-input">
                                            Remember me
                                        </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 text-right">

                                    </div>
                                </div>
                            </div>
                            <div class="text-center mt40">
                                <?= Html::SubmitButton('Login', ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-box" data-mh="3">
                        <div class="more-info">
                            <div class="swiper-container form-slider">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <img  src="<?=$bundle->baseUrl?>/images/heart-rate.png" alt="">
                                        <div class="small-title">
                                            Superland bracelet
                                        </div>
                                        <div class="short-description">
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                            Phasellus hendrerit. Pellentesque aliquet nibh nec urna.
                                            In nisi neque, aliquet vel, dapibus id, mattis vel, nisi.
                                            Sed pretium, ligula sollicitudin laoreet viverra, tortor
                                            libero sodales leo, eget blandit nunc tortor eu nibh. Nullam
                                            mollis. Ut justo. Suspendisse potenti.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>