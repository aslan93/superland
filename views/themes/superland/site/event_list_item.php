<div>
    <div class="anounce-item" style="margin-bottom: 0; padding: 0;">
        <h4><?= $model->lang->Title ?></h4>
        <div>
            <?= $model->shortText ?>
        </div>
        <div style="line-height: 3.5    ">
            <div class="pull-left">
                <time>  
                    <i class="fa fa-calendar"></i> <?= $model->dMYDate ?>
                </time>
            </div>
            <div class="pull-right">
                <a data-pjax="0" href="<?= $model->seoLink ?>" class="btn btn-block btn-default"><?=Yii::t("app", 'See details')?></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <hr />
</div>