<?php
use yii\widgets\Pjax;
use app\assets\SuperFrontAssets;
use yii\helpers\Url;
$bundle = SuperFrontAssets::register($this);
?>

<div class="grid-item">
    <div class="attraction-box">
        <div class="card">
            <div class="front face">
                <div class="img-box">
                    <img class="img-responsive small-img" src="<?=$model->logo?>" alt="" style="width: 50% ; height: auto">
                </div>
                <div class="name-box">
                    <?=$model->lang->Title?>
                </div>
            </div>
            <div class="back face">
                <button class="close-flip">
                    <img src="<?=$bundle->baseUrl?>/images/cancel-small.png" alt="">
                </button>
                <div class="inline-gallery">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="img-box">
                                <a href="<?=$model->logo?>" data-toggle="lightbox" data-gallery="mixedgallery">
                                    <img class="img-responsive" src="<?=$model->logo?>" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="img-box">
                                <a class="video-border" href="<?=$model->Video?>"  data-toggle="lightbox" data-gallery="mixedgallery">
                                    <span class="fa fa-play"></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="img-box">
                                <a href="<?=$model->mainImage->imagePath?>" data-toggle="lightbox" data-gallery="mixedgallery" >
                                    <img class="img-responsive" src="<?=$model->mainImage->imagePath?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="description">
                    <?=$model->shortDescription?>
                </div>
                <div class="parameters">
                    <ul>
                        <li>
                            <div>
                                <?=$model->AgeFrom?>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span class="fa fa-arrows-h"></span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>
                                   <?=$model->KGTo?> KG
                                </span>
                            </div>
                        </li>
                        <?php
                        if ($model->AgeFrom > 10) {
                            ?>
                            <li>
                                <div>
                                    <span class="fa fa-eye"></span>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="mt20">
                    <a href="<?=\yii\helpers\Url::to(['/ajax-games/get-game','id'=>$model->ID])?>" class="more-details" data-remote="false" data-toggle="modal" data-target="#post-modal">
                        <?=Yii::t('app','View more')?>
                    </a>
                </div>
                <div class="footer-attraction-box">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a class="ticket">
                                <?=Yii::t('app','Buy ticket')?>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="<?=Url::to(['/info/rules-regulations'])?>" class="rules"
                                data-toggle="tooltip" data-placement="top" title="Rules">
                                <?=Yii::t('app','Rules &amp; Regulations')?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

