<?php

    use yii\helpers\Html;
    use app\assets\SuperFrontAssets;
    use app\models\SliderItem\SliderItem;
    use app\models\Article\Article;
    use yii\caching\TagDependency;
    use app\modules\Settings\Settings;
    $bundle = SuperFrontAssets::register($this);
$this->title = 'Superland Join Us';
?>

<div class="name-page" style="min-height: 544px">
    <img class="parallax-img" src='<?=$bundle->baseUrl?>/images/name-page-bg.jpg' alt="" style="top: -160%">
</div>

<section class="join-us">
    <div class="container">
        <div class="join-form">
            <div class="row remove-padding">
                <div class="col-md-6">
                    <?=\app\modules\User\widgets\Registration\Registration::widget()?>
                </div>
                <div class="col-md-6">
                    <div class="right-box" data-mh="3">
                        <div class="more-info">
                            <img class="animate-img first" src="<?=$bundle->baseUrl?>/images/heart-rate.png" alt="">
                            <div class="small-title">
                                Superland bracelet
                            </div>
                            <div class="short-description">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                Phasellus hendrerit. Pellentesque aliquet nibh nec urna.
                                In nisi neque, aliquet vel, dapibus id, mattis vel, nisi.
                                Sed pretium, ligula sollicitudin laoreet viverra, tortor
                                libero sodales leo, eget blandit nunc tortor eu nibh. Nullam
                                mollis. Ut justo. Suspendisse potenti.
                            </div>
                        </div>
                        <div class="more-info">
                            <img class="animate-img last" src="<?=$bundle->baseUrl?>/images/credit-card.png" alt="">
                            <div class="small-title">
                                Superland card
                            </div>
                            <div class="short-description">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus
                                hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque,
                                aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula
                                sollicitudin laoreet viverra, tortor libero sodales leo, eget
                                blandit nunc tortor eu nibh. Nullam mollis. Ut justo.
                                Suspendisse potenti.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>