<?php
use yii\widgets\Pjax;
use app\assets\SuperFrontAssets;
use yii\helpers\Url;
$bundle = SuperFrontAssets::register($this);
?>
<style>
    .attraction-box .img-box .small-img{
        height: 170px;
    }
</style>
<div class="grid-item">
    <div class="attraction-box">
        <div class="card">
            <div class="front face">
                <div class="img-box">
                    <img class="img-responsive small-img" src="<?=$model->image?>" alt="" style="width: 50% ; height: auto">
                </div>
                <div class="name-box">
                    <?=$model->lang->Name?>
                </div>
            </div>
            <div class="back face">
                <button class="close-flip">
                    <img src="<?=$bundle->baseUrl?>/images/cancel-small.png" alt="">
                </button>
                <div class="inline-gallery">
                    <div class="row">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-6">
                            <div class="img-box">
                                <a href="<?=$model->image?>" data-toggle="lightbox" data-gallery="mixedgallery">
                                    <img class="img-responsive" src="<?=$model->image?>" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">

                        </div>
                    </div>
                </div>
                <div class="description">
                    <?=$model->shortDescription?>
                </div>

                <div class="mt20">
                    <a href="<?=\yii\helpers\Url::to(['/ajax/get-food','id'=>$model->ID])?>" class="more-details" data-remote="false" data-toggle="modal" data-target="#post-modal">
                        <?=Yii::t('app','View more')?>
                    </a>
                </div>
                <div class="footer-attraction-box">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a class="ticket">
                                <?=Yii::t('app','Buy ticket')?>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="<?=Url::to(['/info/rules-regulations'])?>" class="rules" 
                                data-toggle="tooltip" data-placement="top" title="Rules">
                                <?=Yii::t('app','Rules & Regulations')?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

