<?php

    use app\modules\Feedback\components\FeedbackWidget\FeedbackWidgetSimple;
    use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
    use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
    use app\modules\Settings\Settings;
$this->title = 'Superland Contacts';
?>

<div class="container">
    <h1><?= $article->lang->Title ?></h1>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= $article->lang->Text ?>
            <hr />
            <?= \app\modules\Feedback\components\FeedbackWidget\FeedbackWidget::widget() ?>
            <hr />
            <?= Settings::getByName('address', true) ?>
            <hr />
            <?= Settings::getByName('grafic', true) ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?= Settings::getByName('map') ?>
        </div>
    </div>
    
    <?= ArticleImageWidget::widget([
        'article' => $article
    ]) ?>
    
</div>