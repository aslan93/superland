<?php
use yii\widgets\Pjax;
use yii\helpers\Url;
?>

<?php

    if ($index==0) {
        ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="post-box big-box">
                <div class="img-post" style="background-image: url('<?=$model->mainImage->imagePath?>') ;
                    background-size: cover ; background-repeat: no-repeat">
                    <a href="<?=\yii\helpers\Url::to(['/ajax/get-event','id'=>$model->ID])?>" class="more-details" data-remote="false" data-toggle="modal" data-target="#post-modal">
                        <div class="posted">
                            <div class="posted-content">
                                <div class="hour">
                                    <?=$model->niceHour?>
                                </div>
                                <div class="date">
                                    <?=$model->niceDate?>
                                </div>
                                <?php
                                if ($model->Type == \app\modules\Events\models\Events::TypeParents) {
                                    ?>
                                    <div class="post-for-parent">
                                        <?= $model->Type ?>
                                    </div>
                                    <?php
                                }else{
                                    ?>
                                    <div class="post-for-kids">
                                        <?= $model->Type ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="footer-post">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="short-title">
                                <?=$model->lang->Name?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <a href="#">
                                <?=Yii::t('app','BOOK')?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }else{
        ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="post-box small-box">
                <div class="img-post" style="background-image: url('<?= $model->mainImage->imagePath  ?>') ;
                    background-size: cover ; background-repeat: no-repeat">
                    <a href="<?=\yii\helpers\Url::to(['/ajax/get-event','id'=>$model->ID])?>" class="more-details" data-remote="false" data-toggle="modal" data-target="#post-modal">
                        <div class="posted">
                            <div class="posted-content">
                                <div class="hour">
                                    <?=$model->niceHour?>
                                </div>
                                <div class="date">
                                    <?=$model->niceDate?>
                                </div>
                                <?php
                                if ($model->Type == \app\modules\Events\models\Events::TypeParents) {
                                    ?>
                                    <div class="post-for-parent">
                                        <?= $model->Type ?>
                                    </div>
                                    <?php
                                }else{
                                    ?>
                                    <div class="post-for-kids">
                                        <?= $model->Type ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="footer-post">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="short-title">
                                <?=$model->lang->Name?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <a href="#">
                                <?=Yii::t('app','BOOK')?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
?>

