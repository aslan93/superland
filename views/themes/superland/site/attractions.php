<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;

$bundle = SuperFrontAssets::register($this);
$this->title = 'Superland Attractions';
?>


<section class="front-presentation">
    <div class="parent-presentation">
        <div class="video-overlay">

        </div>
        <video controls poster="<?=$bundle->baseUrl?>/images/presentation-bg.jpg" class="banner__video" autoplay="false" muted="muted">
            <source src="<?=$bundle->baseUrl?>/images/video/happy-kids.mp4" type="video/mp4">
            Your browser doesn't support HTML5 video tag.
        </video>
        <div class="content">
            <div class="big-text">
                Create
            </div>
            <div class="small-text">
                your
            </div>
            <div class="big-text">
                dream!
            </div>
            <a href="#" class="play-video" data-target="#modal-video">
                <svg class="spin" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <g>
                                <path d="M477.606,128.055C443.431,68.863,388.251,26.52,322.229,8.83C256.208-8.862,187.25,0.217,128.055,34.394
                                    C68.861,68.57,26.52,123.75,8.83,189.772c-17.69,66.021-8.611,134.981,25.564,194.173
                                    C68.568,443.137,123.75,485.48,189.771,503.17c22.046,5.908,44.417,8.83,66.646,8.83c44.339,0,88.101-11.629,127.529-34.393
                                    c59.192-34.175,101.535-89.355,119.225-155.377C520.862,256.207,511.781,187.249,477.606,128.055z M477.429,315.333
                                    c-15.848,59.146-53.78,108.581-106.81,139.197c-53.028,30.617-114.806,38.749-173.952,22.903
                                    c-59.147-15.848-108.581-53.78-139.198-106.81c-30.616-53.028-38.749-114.807-22.9-173.954
                                    C50.418,137.523,88.35,88.09,141.379,57.472c35.325-20.395,74.524-30.812,114.249-30.812c19.91,0,39.959,2.618,59.702,7.909
                                    c59.146,15.848,108.581,53.78,139.197,106.81C485.144,194.408,493.278,256.186,477.429,315.333z"/>
                            </g>
                        </g>
                    <g>
                        <g>
                            <path d="M413.303,134.44c-31.689-40.938-79.326-68.442-130.698-75.461c-7.283-0.997-14.009,4.106-15.006,11.399
                                    c-0.995,7.291,4.108,14.009,11.399,15.006c44.512,6.081,85.783,29.909,113.232,65.369c2.626,3.392,6.565,5.168,10.546,5.168
                                    c2.849,0,5.72-0.909,8.146-2.789C416.741,148.628,417.807,150.259,413.303,134.44z"/>
                        </g>
                    </g>
                    </svg>
                <button class="play-button" data-toggle="modal" data-target="#modal-video">
                    <svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                          viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <path d="M378.778,231.852l-164.526-94.99c-8.731-5.041-19.155-5.039-27.886-0.001c-8.731,5.04-13.944,14.069-13.944,24.15v189.98
                                    c0,10.081,5.212,19.109,13.944,24.15c4.365,2.521,9.152,3.78,13.941,3.78c4.79,0,9.579-1.262,13.944-3.781l164.528-94.989
                                    c8.73-5.042,13.941-14.07,13.941-24.151C392.72,245.92,387.508,236.892,378.778,231.852z M365.452,257.074l-164.527,94.989
                                    c-0.201,0.117-0.62,0.358-1.236,0c-0.618-0.357-0.618-0.839-0.618-1.071v-189.98c0-0.232,0-0.714,0.618-1.071
                                    c0.242-0.14,0.453-0.188,0.633-0.188c0.28,0,0.482,0.117,0.605,0.188l164.526,94.99c0.201,0.116,0.618,0.357,0.618,1.071
                                    C366.071,256.716,365.652,256.958,365.452,257.074z"/>
                        </g>
                        </svg>
                </button>
            </a>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="video-overlay">

            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 54 54" enable-background="new 0 0 54 54" xml:space="preserve">
                          <path fill="#FFFFFF" d="M27,4c12.7,0,23,10.3,23,23S39.7,50,27,50S4,39.7,4,27S14.3,4,27,4 M27,2C13.2,2,2,13.2,2,27s11.2,25,25,25 s25-11.2,25-25S40.8,2,27,2L27,2z"/>
                          <line fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.1" y1="19.9" x2="19.9" y2="34.1"/>
                          <line fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.1" y1="34.1" x2="19.9" y2="19.9"/>
                    </svg>
                </span></button>
            <video controls poster="<?=$bundle->baseUrl?>/images/presentation-bg.jpg" class="banner__video" autoplay muted="muted">
                <source src="<?=$bundle->baseUrl?>/images/video/happy-kids.mp4" type="video/mp4">
                Your browser doesn't support HTML5 video tag.
                <div id="video-controls" class="controls" data-state="hidden">
                    <button id="playpause" type="button" data-state="play">Play/Pause</button>
                    <button id="stop" type="button" data-state="stop">Stop</button>
                    <div class="progress">
                        <progress id="progress" value="0" min="0">
                            <span id="progress-bar"></span>
                        </progress>
                    </div>
                    <button id="mute" type="button" data-state="mute">Mute/Unmute</button>
                    <button id="volinc" type="button" data-state="volup">Vol+</button>
                    <button id="voldec" type="button" data-state="voldown">Vol-</button>
                    <button id="fs" type="button" data-state="go-fullscreen">Fullscreen</button>
                </div>
            </video>
        </div>
    </div>
</section>

<div class="attractions">
    <div class="container">
        <div class="description">
            Praesent dapibus, neque id cursus faucibus, tortor neque egestas
            augue, eu vulputate magna eros eu erat. Aliquam erat volutpat.
            Nam dui mi, tincidunt quis, accumsan porttitor, facilisis
            luctus, metus.
        </div>
        <div class="text-center">
            <a href="<?=\yii\helpers\Url::to(['/site/join-us'])?>" class="btn btn-default">
                <?=Yii::t('app','JOIN US')?>
            </a>
        </div>
        <div class="sort-attractions">
            <?php $form = \kartik\form\ActiveForm::begin([
                'action'=>'',
                'method' => 'post',
                'id'=>'main-form',
            ]); ?>
            <?= $form->field($searchModel, "AgeFrom")->input('text',['class'=>'fromAge hidden'])->label(false)?>
            <?= $form->field($searchModel, "AgeTo")->input('text',['class'=>'toAge hidden'])->label(false)?>
            <?= $form->field($searchModel, "ID")->input('text',['class'=>'id hidden','value'=>''])->label(false)?>
            <ul class="nav nav-tabs" id="view">
                <li role="presentation">
                    <a href="#" onclick="submitForm()">
                        <?=Yii::t('app','All')?>
                    </a>
                </li>
                <li role="presentation" >
                    <a href="#" onclick="submitForm(1,3)" >
                        1 - 3 <?=Yii::t('app','ANI')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#" onclick="submitForm(3,5)">
                        3 - 5 <?=Yii::t('app','ANI')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#" onclick="submitForm(5,12)">
                        5 - 12 <?=Yii::t('app','ANI')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#" onclick="submitForm(12,17)">
                        12 - 17 <?=Yii::t('app','ANI')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#" onclick="submitForm(18,19)">
                        18+ <?=Yii::t('app','ANI')?>
                    </a>
                </li>
            </ul>
            <?php \kartik\form\ActiveForm::end(); ?>

        </div>
        <div class="result-attractions">
            <!-- AJAX POST MODAL-->
            <div class="modal fade" id="post-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog full-screen" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                            </button>
                        </div>
                        <div class="modal-body">

                        </div>
                    </div>
                </div>
            </div>
            <div class="grid">
                <?php
                \yii\widgets\Pjax::begin([
                    'timeout' => 10000,
                    'id' => 'games',
                ]);
                ?>

                <?=
                \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                    'itemView' => '_game_item',

                ]);
                ?>
                <?php
                \yii\widgets\Pjax::end();
                ?>

            </div>
        </div>
    </div>
</div>

<?php $this->registerJs(
    "   function submitForm(from,to){
            
                $('.fromAge').val(from);
                $('.toAge').val(to);
                var form = $('#main-form');
                var formData = form.serialize();
                $.pjax.reload({container: '#games',data: formData, push:false,replace:false,timeout:10000});
            }
            $(document).on('click','.more-details',function(){
                 $('.modal-body').empty();       
            });
            
            "
    , \yii\web\View::POS_END);
            ?>