<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;

$bundle = SuperFrontAssets::register($this);
?>
<script>
    var levels = [

        {
            background: "url(/images/map/harta1.png)",
            text: "<div class='text'><span><img src='/images/map/layer.svg' /></span> Etajul 1</div>" ,
            markers: <?=$markers1?>
        } ,

        {
            background: "url(/images/map/harta2.png)",
            text: "<div class='text'><span><img src='/images/map/layer.svg' /></span> Etajul 2</div> " ,
            markers: <?=$markers2?>
        }
    ];
</script>
<!-- ACTIVE PAGE -->
<div id="mall-page"></div>
<!-- NO DELETE BLOCK -->
<div class="nav-mall-map">
    <a href="#" class="all-level" onclick="MallMaper.moveToSelectLevel()">
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="54.849px" height="54.849px" viewBox="0 0 54.849 54.849" style="enable-background:new 0 0 54.849 54.849;"
             xml:space="preserve">
                <g>
                    <g>
                        <g>
                            <path d="M54.497,39.614l-10.363-4.49l-14.917,5.968c-0.537,0.214-1.165,0.319-1.793,0.319c-0.627,0-1.254-0.104-1.79-0.318
                                l-14.921-5.968L0.351,39.614c-0.472,0.203-0.467,0.524,0.01,0.716L26.56,50.81c0.477,0.191,1.251,0.191,1.729,0L54.488,40.33
                                C54.964,40.139,54.969,39.817,54.497,39.614z"/>
                            <path d="M54.497,27.512l-10.364-4.491l-14.916,5.966c-0.536,0.215-1.165,0.321-1.792,0.321c-0.628,0-1.256-0.106-1.793-0.321
                                l-14.918-5.966L0.351,27.512c-0.472,0.203-0.467,0.523,0.01,0.716L26.56,38.706c0.477,0.19,1.251,0.19,1.729,0l26.199-10.479
                                C54.964,28.036,54.969,27.716,54.497,27.512z"/>
                            <path d="M0.361,16.125l13.662,5.465l12.537,5.015c0.477,0.191,1.251,0.191,1.729,0l12.541-5.016l13.658-5.463
                                c0.477-0.191,0.48-0.511,0.01-0.716L28.277,4.048c-0.471-0.204-1.236-0.204-1.708,0L0.351,15.41
                                C-0.121,15.614-0.116,15.935,0.361,16.125z"/>
                        </g>
                    </g>
                </g>
            </svg>
    </a>
</div>

<section class="wrap-mall">
    <div class="row">
        <div class="col-md-12">
            <div class="mall-maps">
                <div class="levels">

                </div>
                <div class="mall-map-info">
                    <div class="container">
                        <div class="title">
                            Etajul 1
                        </div>
                        <div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Adipisci, beatae consectetur corporis culpa cum dolorem est
                            explicabo illo inventore maiores mollitia nemo nisi, omnis
                            perferendis quaerat quas quisquam ratione voluptatum?
                        </div>
                        <ul>
                            <li>
                                Item1
                            </li>
                            <li>
                                Item2
                            </li>
                            <li>
                                Item3
                            </li>
                        </ul>
                        <div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam
                            beatae debitis natus pariatur. Cumque, deserunt dolore dolorum eos
                            fuga in, minima nesciunt provident quam quidem, quis quos rem saepe.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>