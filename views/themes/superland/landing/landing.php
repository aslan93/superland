<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;
$bundle = SuperFrontAssets::register($this);
$this->title = 'Superland';
?>
<div id="no-scroll-id"></div>
<div class="full-page-wrap">
    <div class="costum-video-payer">
        <div id="set-height"></div>
        <div id="time"></div>
        <div id="scroll"></div>
    </div>
    <div class="content-fixed">
        <div class="row remove-padding">
            <div class="col-md-4 hidden-lg hidden-md">
                <div class="right-content">
                    <div class="animation-blob-water">
                        <svg>
                            <defs>
                                <filter id="filter3">
                                    <feGaussianBlur in="SourceGraphic" stdDeviation="18" result="blur" />
                                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 28 -10" result="filter" />
                                    <feComposite in="SourceGraphic" in2="filter" operator="atop" />
                                </filter>
                            </defs>
                        </svg>
                    </div>
                    <div class="logo">
                        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/superland_logo_castel2.png" alt="">
                    </div>
                    <div class="slogan">
                        Create your dream...
                    </div>
                    <div class="short-description">
                        <div>
                            <?=Yii::t('app','Superland este o destinație diversificată, dedicată atât copiilor
                            cât și părinților, un spațiu sigur și interactiv destinat
                            petrecerii timpului liber!')?>
                        </div>
                        <div>
                            <?=Yii::t('app','Îmbarcați-vă într-o călătorie fascinantă și împărtășiți momente
                            speciale împreună!')?>
                        </div>
                    </div>
                    <div class="join">
                        <a href="#" class="btn btn-info">
                            <img src="<?=$bundle->baseUrl?>/images/papaer.png" alt="">
                             <?=Yii::t('app','Inscrie-te Aici!')?>
                        </a>
                    </div>
                    <div class="terms">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 text-center">
                                <a href="#" data-toggle="modal" data-target="#terms">
                                    <?=Yii::t('app','Termeni si Conditii')?>
                                </a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 text-center">
                                <a href="#" data-toggle="modal" data-target="#contact-us">
                                    <?=Yii::t('app','Contactati-ne!')?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="video">
                    <div class="back-face-box">

                    </div>
                    <div class="overflow-hidden">
                        <video id="v0" tabindex="0" autobuffer="autobuffer" preload="preload"><source type="video/webm; codecs=&quot;vp8, vorbis&quot;" src="https://www.html5rocks.com/tutorials/video/basics/Chrome_ImF.webm"></source>
                                <source type="video/ogg; codecs=&quot;theora, vorbis&quot;" src="https://www.html5rocks.com/tutorials/video/basics/Chrome_ImF.ogv"></source>
                                <source type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;" src="https://www.html5rocks.com/tutorials/video/basics/Chrome_ImF.mp4"></source>
                                <p>Sorry, your browser does not support the &lt;video&gt; element.</p>
                            </video>
                    </video>
                    </div>
                    <div class="mouse-elem">
                        <ul>
                            <li>
                                <span class="shape"></span>
                            </li>
                            <li>
                                <span class="shape"></span>
                            </li>
                            <li>
                                <span class="shape"></span>
                            </li>
                            <li>
                                <span class="shape"></span>
                            </li>
                            <li>
                                    <span class="mouse">
                                        <img src="<?=$bundle->baseUrl?>/images/mouse.png" alt="">
                                        <span class="before-mouse">
                                             <?=Yii::t('app','Incepe povestea')?><br /> <?=Yii::t('app','prin scroll')?>
                                        </span>
                                    </span>
                            </li>
                            <li>
                                <span class="shape"></span>
                            </li>
                            <li>
                                <span class="shape"></span>
                            </li>
                            <li>
                                <span class="shape"></span>
                            </li>
                            <li>
                                <span class="shape"></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="select-category">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="category-wrap children">
                                <a href="#">
                                    <div class="wrap-parallax" style="background-image: url('<?=$bundle->baseUrl?>/images/kids-parallax.png'); background-repeat: no-repeat;"></div>
                                    <div class="content">
                                        <!-- <div class="img">
                                            <img src="<?=$bundle->baseUrl?>/images/kids-category.png" alt="">
                                        </div> -->
                                        <div class="text">
                                            <div class="big-text">
                                                <?=Yii::t('app','Kids')?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="category-wrap parent">
                                <a href="<?=\yii\helpers\Url::to(['/site'])?>">
                                    <div class="wrap-parallax" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-parallax.png'); background-repeat: no-repeat;"></div>
                                    <div class="content">
                                        <!-- <div class="img">
                                            <img src="<?=$bundle->baseUrl?>/images/parent-category.png" alt="">
                                        </div> -->
                                        <div class="text">
                                            <div class="big-text">
                                                <?=Yii::t('app','Parent')?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-sm hidden-xs">
                <div class="right-content">
                    <div class="animation-blob-water">
                        <div class="wrap-parallax">
                            <div class="left-parallax-wrap">
                                <div class="first-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem1.png')"></div> 
                                <div class="second-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-2.png')"></div> 
                                <div class="third-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-3.png')"></div>  
                                <div class="fourth-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-7.png')"></div>  
                                <div class="fifth-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-6.png')"></div>
                                <div class="sixth-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-9.png')"></div> 
                                <div class="seventh-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-8.png')"></div>                 
                            </div>
                            <div class="right-parallax-wrap">
                                <div class="first-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-right-elem-1.png')"></div> 
                                <div class="second-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-right-elem-2.png')"></div> 
                                <div class="third-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-right-elem-4.png')"></div>  
                                <div class="fourth-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-4.png')"></div> 
                                <div class="fifth-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-8.png')"></div>  
                                <div class="sixth-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-6.png')"></div> 
                                <div class="seventh-elem" style="background-image: url('<?=$bundle->baseUrl?>/images/parent-left-elem-5.png')"></div> 
                            </div>
                        </div>
                        <svg>
                            <defs>
                                <filter id="filter3">
                                    <feGaussianBlur in="SourceGraphic" stdDeviation="18" result="blur" />
                                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 28 -10" result="filter" />
                                    <feComposite in="SourceGraphic" in2="filter" operator="atop" />
                                </filter>
                            </defs>
                        </svg>
                    </div>
                    <div class="logo">
                        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/superland_logo_castel2.png" alt="">
                    </div>
                    <div class="slogan">
                        Create your dream...
                    </div>
                    <div class="short-description">
                        <div>

                            <?=Yii::t('app','Superland este o destinație diversificată, dedicată atât copiilor
                            cât și părinților, un spațiu sigur și interactiv destinat
                            petrecerii timpului liber!')?>
                        </div>
                        <div>
                            <?=Yii::t('app','Îmbarcați-vă într-o călătorie fascinantă și împărtășiți momente
                            speciale împreună!')?>
                        </div>
                    </div>
                    <div class="join">
                        <a href="#" class="btn btn-info">
                            <img src="<?=$bundle->baseUrl?>/images/papaer.png" alt="">
                             <?=Yii::t('app','Inscrie-te Aici!')?>
                        </a>
                    </div>
                    <div class="terms">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <a href="#" data-toggle="modal" data-target="#terms">
                                    <?=Yii::t('app','Termeni si Conditii')?>
                                </a>
                            </div>
                            <div class="col-md-6 text-center">
                                <a href="#" data-toggle="modal" data-target="#contact-us">
                                    <?=Yii::t('app','Contactati-ne!')?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info-wrap coockies">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <div class="elem-wrap">
                        <?=Yii::t('app','Acest website folosește cookie-uri proprii și de la terți
                        pentru a furniza vizitatorilor o experiență mult mai bună
                        de navigare și servicii adaptate nevoilor și interesului
                        fiecăruia.')?>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12 text-right">
                    <div class="elem-wrap" style="justify-content: flex-end">
                        <button class="btn btn-info blue" onclick="removeElement($('.coockies'))">
                            <?=Yii::t('app','ACCEPT')?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="contact-us" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog full-screen" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                    </button>
                    <div class="modal-title" id="myModalLabel"><?=Yii::t('app','Contact us')?></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div>
                        <?= \app\modules\Feedback\components\FeedbackWidget\FeedbackWidget::widget() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog full-screen" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                    </button>
                    <div class="modal-title" id="myModalLabel"><?=Yii::t('app','Termeni si conditii')?></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="long-description-wrap mCustomScrollbar" data-mcs-theme="dark">
                        <?=Settings::getByName('terms',true)?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJS(' 
    if($("#no-scroll-id").length > 0){
        $("body").css({
            background: "#f7f7f7"
    });
    
    $(".right-content .join  a img").addClass("animated pulse");
   
    }

    var frameNumber = 0, // start video at frame 0
    // lower numbers = faster playback
    playbackConst = 500, 
    // get page height from video duration
    setHeight = document.getElementById("set-height"), 
    // select video element         
    vid = document.getElementById("v0"); 
    // var vid = $("#v0")[0]; // jquery option

    // dynamically set the page height according to video length
    vid.addEventListener("loadedmetadata", function() {
      setHeight.style.height = Math.floor(vid.duration) * playbackConst + "px";
    });


    // Use requestAnimationFrame for smooth playback
    function scrollPlay(){  
      var frameNumber  = window.pageYOffset/playbackConst;
      vid.currentTime  = frameNumber;
      window.requestAnimationFrame(scrollPlay);
    }

    window.requestAnimationFrame(scrollPlay);    

',\yii\web\View::POS_READY);
?>
