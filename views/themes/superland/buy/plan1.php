<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;
$bundle = SuperFrontAssets::register($this);
$this->title = 'Plan Your Event';
?>

<section class="front-presentation">
    <div class="parent-presentation">
        <div class="video-overlay">

        </div>
        <video controls poster="<?=$bundle->baseUrl?>/images/presentation-bg.jpg" class="banner__video" autoplay="false" muted="muted">
            <source src="<?=$bundle->baseUrl?>/images/video/happy-kids.mp4" type="video/mp4">
            Your browser doesn't support HTML5 video tag.
        </video>
        <div class="content">
            <div class="big-text">
                Create
            </div>
            <div class="small-text">
                your
            </div>
            <div class="big-text">
                dream!
            </div>
            <a href="#" class="play-video" data-target="#modal-video">
                <svg class="spin" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <g>
                                <path d="M477.606,128.055C443.431,68.863,388.251,26.52,322.229,8.83C256.208-8.862,187.25,0.217,128.055,34.394
                                    C68.861,68.57,26.52,123.75,8.83,189.772c-17.69,66.021-8.611,134.981,25.564,194.173
                                    C68.568,443.137,123.75,485.48,189.771,503.17c22.046,5.908,44.417,8.83,66.646,8.83c44.339,0,88.101-11.629,127.529-34.393
                                    c59.192-34.175,101.535-89.355,119.225-155.377C520.862,256.207,511.781,187.249,477.606,128.055z M477.429,315.333
                                    c-15.848,59.146-53.78,108.581-106.81,139.197c-53.028,30.617-114.806,38.749-173.952,22.903
                                    c-59.147-15.848-108.581-53.78-139.198-106.81c-30.616-53.028-38.749-114.807-22.9-173.954
                                    C50.418,137.523,88.35,88.09,141.379,57.472c35.325-20.395,74.524-30.812,114.249-30.812c19.91,0,39.959,2.618,59.702,7.909
                                    c59.146,15.848,108.581,53.78,139.197,106.81C485.144,194.408,493.278,256.186,477.429,315.333z"/>
                            </g>
                        </g>
                    <g>
                        <g>
                            <path d="M413.303,134.44c-31.689-40.938-79.326-68.442-130.698-75.461c-7.283-0.997-14.009,4.106-15.006,11.399
                                    c-0.995,7.291,4.108,14.009,11.399,15.006c44.512,6.081,85.783,29.909,113.232,65.369c2.626,3.392,6.565,5.168,10.546,5.168
                                    c2.849,0,5.72-0.909,8.146-2.789C416.741,148.628,417.807,150.259,413.303,134.44z"/>
                        </g>
                    </g>
                    </svg>
                <button class="play-button" data-toggle="modal" data-target="#modal-video">
                    <svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                          viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <path d="M378.778,231.852l-164.526-94.99c-8.731-5.041-19.155-5.039-27.886-0.001c-8.731,5.04-13.944,14.069-13.944,24.15v189.98
                                    c0,10.081,5.212,19.109,13.944,24.15c4.365,2.521,9.152,3.78,13.941,3.78c4.79,0,9.579-1.262,13.944-3.781l164.528-94.989
                                    c8.73-5.042,13.941-14.07,13.941-24.151C392.72,245.92,387.508,236.892,378.778,231.852z M365.452,257.074l-164.527,94.989
                                    c-0.201,0.117-0.62,0.358-1.236,0c-0.618-0.357-0.618-0.839-0.618-1.071v-189.98c0-0.232,0-0.714,0.618-1.071
                                    c0.242-0.14,0.453-0.188,0.633-0.188c0.28,0,0.482,0.117,0.605,0.188l164.526,94.99c0.201,0.116,0.618,0.357,0.618,1.071
                                    C366.071,256.716,365.652,256.958,365.452,257.074z"/>
                        </g>
                        </svg>
                </button>
            </a>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="video-overlay">

            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 54 54" enable-background="new 0 0 54 54" xml:space="preserve">
                          <path fill="#FFFFFF" d="M27,4c12.7,0,23,10.3,23,23S39.7,50,27,50S4,39.7,4,27S14.3,4,27,4 M27,2C13.2,2,2,13.2,2,27s11.2,25,25,25 s25-11.2,25-25S40.8,2,27,2L27,2z"/>
                          <line fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.1" y1="19.9" x2="19.9" y2="34.1"/>
                          <line fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="34.1" y1="34.1" x2="19.9" y2="19.9"/>
                    </svg>
                </span></button>
            <video controls poster="<?=$bundle->baseUrl?>/images/presentation-bg.jpg" class="banner__video" autoplay muted="muted">
                <source src="<?=$bundle->baseUrl?>/images/video/happy-kids.mp4" type="video/mp4">

                Your browser doesn't support HTML5 video tag.
                <div id="video-controls" class="controls" data-state="hidden">
                    <button id="playpause" type="button" data-state="play">Play/Pause</button>
                    <button id="stop" type="button" data-state="stop">Stop</button>
                    <div class="progress">
                        <progress id="progress" value="0" min="0">
                            <span id="progress-bar"></span>
                        </progress>
                    </div>
                    <button id="mute" type="button" data-state="mute">Mute/Unmute</button>
                    <button id="volinc" type="button" data-state="volup">Vol+</button>
                    <button id="voldec" type="button" data-state="voldown">Vol-</button>
                    <button id="fs" type="button" data-state="go-fullscreen">Fullscreen</button>
                </div>
            </video>
        </div>
        <!-- AJAX hotel MODAL-->
        <div class="modal fade" id="hotel-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                        </button>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="category_interesting">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="<?=\yii\helpers\Url::to(['/buy/plan1'])?>">
                    <div class="category-box-big red-border" data-mh="1">
                        <div class="box-bg first">
                            <img class="category-box-big-bg" src="<?= $bundle->baseUrl ?>/images/party.png" alt="">
                        </div>
                        <div class="name-category md-text-right">
                            Plan <br />
                            Your Party
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="<?=\yii\helpers\Url::to(['/site/events-tab'])?>">
                    <div class="category-box-big blue-border" data-mh="1">
                        <div class="box-bg last">
                            <img class="category-box-big-bg" src="<?= $bundle->baseUrl ?>/images/event.png" alt="">
                        </div>
                        <div class="name-category  md-text-left">
                            Join <br />
                            Our Party <br />
                            & Events
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="plan-party">

    <div class="container">
        <div class="party-big-box">
            <?php $form = \kartik\form\ActiveForm::begin([
                'id' => 'buyer-form',
                'options' => [
                    'data-pjax' => '',
                ],
                'fieldConfig' => [
                    'template' => "{input}",
                    'options' => [
                        'tag' => false,
                    ],
                ],
            ]); ?>
                <div class="plan-party-box no-padding">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="big-title">
                               <?=Yii::t('app',' Plan Your Party')?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="ticket-page-active">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="number-page active">
                                            1
                                        </div>
                                        <div class="name">
                                            <?=Yii::t('app','Date & Room')?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="number-page">
                                            2
                                        </div>
                                        <div class="name">
                                            <?=Yii::t('app','Packages')?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="number-page">
                                            3
                                        </div>
                                        <div class="name">
                                            <?=Yii::t('app','Options')?>
                                        </div>
                                    </div>
                                </div>
                                <div class="border"></div>
                            </div>
                        </div>
                    </div>
                    <div class="mt70">
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="name-section">
                                    <?=Yii::t('app','CHOOSE A DATE')?>
                                </div>
                                <?= $form->field($model, 'Date',[
                                    'template' => ' {input}',
                                ])->input('date',['class'=>'hidden','id'=>'date-pick'])->label(false) ?>
                                <div class="choose-date-party" id="sandbox-container" data-mh="7">
                                    <div></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="name-section">
                                    <?=Yii::t('app','CHOOSE HOUR')?>
                                </div>
                                <div class="choose-hour-party" data-mh="7">
                                    <div class="custom-clock">
                                        <div class='time-text-wrap'>
                                            <div class="text">
                                                Hours
                                            </div>
                                            <div class="text">
                                                Minutes
                                            </div>
                                        </div>
                                        <?= $form->field($model, 'Time',[
                                            'template' => ' {input}',
                                        ])->input('text',['class'=>'clock'])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?= $form->field($model, 'roomPrice',['template' => ' {input}'])->input('number',['class'=>'hidden','id'=>'roomPrice'])->label(false) ?>
            <div class="name-section" ><?=Yii::t('app','Alege Camera')?></div>
            <div class="choose-room">
                    <?php
                    foreach ($rooms as $room) {
                        ?>
                        <?= $form->field($model, 'RoomID',['template' => ' {input}'])->input('radio',['class'=>'hidden room-input','id'=>'room-input-'.$room->ID,'value'=>$room->ID,'data-price'=>$room->Price,'onchange'=>'setPrice(this)'])->label(false) ?>

                        <label for="room-input-<?=$room->ID?>">
                        <div class="room-wrap <?= ($room->Avaiability != 'Avaiable') ? Yii::t('app',$room->Avaiability):'disabled'?>">
                            <div class="room-item">
                                <div class="img-box">
                                    <img src="<?= $room->image ?>"
                                         alt="">
                                </div>
                                <div class="name-room">
                                   <?=$room->lang->Name?>
                                </div>
                            </div>
                            <div class="text">
                                <?=Yii::t('app',$room->Avaiability)?>
                            </div>
                            <a href="<?=\yii\helpers\Url::to(['/ajax/get-room','id'=>$room->ID])?>" class="more-details details" data-remote="false" data-toggle="modal" data-target="#hotel-modal">
                                <?=Yii::t('app','Detalii')?>
                            </a>
                        </div>
                        </label>
                        <?php
                    }
                    ?>
                </div>
                <div class="text-center">
                    <?= Html::submitButton( 'Next', ['class' => 'btn btn-default','id'=>'next-btn','disabled'=>true]) ?>
                </div>

            <?php \kartik\form\ActiveForm::end(); ?>
        </div>
    </div>
</section>
<?php $this->registerJs(
    "   function setPrice(ob){
                $('#roomPrice').val($(ob).attr('data-price'));
            }
        $(document).on('click','.more-details',function(){
                 $('.modal-body').empty();       
            });
            
            $(window).trigger('resize');
            
            $(document).on('change','#buyer-form input',function(){
        var errors = 0;
        $('#buyer-form input').each(function(){
           
            if(!$(this).val()){
                errors = errors +1;
            }
            
            if(errors > 0){
                $('#next-btn').prop('disabled',true);
            }else{
                $('#next-btn').prop('disabled',false);
            }
        });
            
        });
            "
    , \yii\web\View::POS_END);
?>

