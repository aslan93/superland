<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;

$bundle = SuperFrontAssets::register($this);
$this->title = 'Buy Ticket';
?>

<div class="name-page" style="min-height: 544px">
    <img class="parallax-img" src='<?=$bundle->baseUrl?>/images/name-page-bg.jpg' alt="" style="top: -160%">
</div>

<section class="bye-ticket">
    <div class="container">
        <div class="ticket-box no-padding">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="big-title">
                        <?=Yii::t('app','Buy ticket')?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="ticket-page-active">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page">
                                    1
                                </div>
                                <div class="name">
                                    <?=Yii::t('app','Choose')?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page active">
                                    2
                                </div>
                                <div class="name"><?=Yii::t('app','Sign up/ Sign in')?></div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page">
                                    3
                                </div>
                                <div class="name">
                                    <?=Yii::t('app','Play')?>
                                </div>
                            </div>
                        </div>
                        <div class="border"></div>
                    </div>
                </div>
            </div>
            <div class="mt70">

                    <div class="row remove-padding">
                        <?php $form = \kartik\form\ActiveForm::begin([
                            'id' => 'buyer-form-t2',
                            'options' => [
                                'data-pjax' => '',

                            ],
                            'fieldConfig' => [
                                'template' => "{input}",
                                'options' => [
                                    'tag' => false,
                                ],
                            ],
                        ]); ?>
                        <div class="col-md-6">
                            <div class="insert-data" data-mh="5">

                                    <div>
                                        <label>
                                            <?= $form->field($buyer, 'FullName')->input('text',['class'=>'style-input','placeholder'=>'Full Name'])->label(false) ?>
                                        </label>
                                    </div>

                                    <div>
                                        <label>
                                            <?= $form->field($buyer, 'Email')->input('text',['class'=>'style-input','placeholder'=>'Email'])->label(false) ?>

                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <?= $form->field($buyer, 'Address')->input('text',['class'=>'style-input','placeholder'=>'Address'])->label(false) ?>

                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <?= $form->field($buyer, 'CardID')->input('text',['class'=>'style-input','placeholder'=>'Card ID'])->label(false) ?>

                                        </label>
                                    </div>

                                    <div>
                                        <label>
                                            <?= $form->field($buyer, 'Password')->input('password',['class'=>'style-input','placeholder'=>'Password'])->label(false) ?>

                                        </label>
                                    </div>

                                    <div>
                                        <label class="confirm-terms">
                                            <input type="checkbox" class="style-input">
                                             <?=Yii::t('app','By accepting this I agree')?><a href="<?=\yii\helpers\Url::to(['/info/terms'])?>" target="_blank"><?=Yii::t('app','Terms & Conditions')?></a>
                                        </label>
                                    </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info_package" data-mh="5">
                                <?php
                                foreach($tickets as $ticket){
                                    ?>
                                    <div class="big-text-light">
                                        <?=Yii::t('app',$ticket->Type)?>
                                    </div>
                                    <div class="text">
                                        <?=$ticket->lang->Name?>    -    <?=$ticket->Price?> <?=Yii::t('app','RON / allday')?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="mt20">
                                    <div class="name-section"></div>
                                </div>
                                <ul>
                                    <li>
                                        <span class="fa fa-check"></span>
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                    </li>
                                    <li>
                                        <span class="fa fa-check"></span>
                                        Aliquam tincidunt mauris eu risus.
                                    </li>
                                    <li>
                                        <span class="fa fa-check"></span>
                                        Vestibulum auctor dapibus neque.
                                    </li>
                                </ul>
                                <div class="text-center mt40 submit-button">
                                    <div class="text-center mt40">
                                        <?= Html::submitButton( 'Next', ['class' => 'btn btn-default' ,'id'=>'next-btn','disabled'=>'true']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php \kartik\form\ActiveForm::end(); ?>
                    </div>

            </div>
        </div>
    </div>
</section>

<?php $this->registerJs(
    "      
         
        $(document).on('change','#buyer-form-t2 input',function(){
        var errors = 0;
        $('#buyer-form-t2 input').each(function(){
            
            if(!$(this).val()){
                errors = errors +1;
            }
            
            if(errors > 0){
                $('#next-btn').prop('disabled',true);
            }else{
                $('#next-btn').prop('disabled',false);
            }
        });
            
        });
           
            
            "
    , \yii\web\View::POS_END);
?>