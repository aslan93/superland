<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;

$bundle = SuperFrontAssets::register($this);
$this->title = 'Buy Ticket';
?>

<div class="name-page" style="min-height: 544px">
    <img class="parallax-img" src='<?=$bundle->baseUrl?>/images/name-page-bg.jpg' alt="" style="top: -160%">
</div>

<section class="bye-ticket">
    <div class="container">
        <div class="ticket-box no-padding">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="big-title">
                        <?=Yii::t('app','Buy ticket')?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="ticket-page-active">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page">
                                    1
                                </div>
                                <div class="name">
                                    <?=Yii::t('app','Choose')?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page">
                                    2
                                </div>
                                <div class="name"><?=Yii::t('app','Sign up/ Sign in')?></div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page active">
                                    3
                                </div>
                                <div class="name">
                                    <?=Yii::t('app','Play')?>
                                </div>
                            </div>
                        </div>
                        <div class="border"></div>
                    </div>
                </div>
            </div>
            <div class="mt70">
                <form action="#" method="post">
                    <div class="row remove-padding">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="insert-data" data-mh="5">
                                <form action="#" method="post">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div>
                                                <label>
                                                    <input type="checkbox" class="style-input">
                                                    <?=Yii::t('app','PLAY WITH YOUR CARD')?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div>
                                                <label>
                                                    <input type="checkbox" class="style-input">
                                                    <?=Yii::t('app','PLAY WITH CASH MONEY')?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="short-description">
                                                <?=Yii::t('app','Credibly deliver cross-media vortals whereas
                                                efficient strategic theme areas. Dynamically recaptiualize
                                                global e-services with best-of-breed outsourcing.
                                                Dramatically create wireless products after
                                                leading-edge schemas.')?>

                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div>
                                                <ul>
                                                    <li>
                                                        <img src="<?=$bundle->baseUrl?>/images/visa.png" alt="">
                                                    </li>
                                                    <li>
                                                        <img src="<?=$bundle->baseUrl?>/images/maestro.png" alt="">
                                                    </li>
                                                    <li>
                                                        <img src="<?=$bundle->baseUrl?>/images/masterCard.png" alt="">
                                                    </li>
                                                    <li>
                                                        <img src="<?=$bundle->baseUrl?>/images/bank-card.png" alt="">
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div>
                                                <label>
                                                    <input type="text" placeholder="Card Id">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div>
                                                <label>
                                                    <input type="text" placeholder="Expiration date">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div>
                                                <label>
                                                    <input type="text" placeholder="CSV">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <label class="confirm-terms">
                                            <input type="checkbox" class="style-input">
                                             <?=Yii::t('app','By accepting this I agree')?><a href="<?=\yii\helpers\Url::to(['/info/terms'])?>" target="_blank"><?=Yii::t('app','Terms & Conditions')?></a>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info_package" data-mh="5">
                                <?php
                                foreach($tickets as $ticket){
                                ?>
                                <div class="big-text-light">
                                    <?=Yii::t('app',$ticket->Type)?>
                                </div>
                                <div class="text">
                                    <?=$ticket->lang->Name?>    -    <?=$ticket->Price?> <?=Yii::t('app','RON / allday')?>
                                </div>
                                <?php
                                }
                                ?>
                                <div class="mt20">
                                    <div class="name-section"></div>
                                </div>
                                <div class="text-center mt40 submit-button">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="price-total">
                                                <div class="text">
                                                    <?=Yii::t('app','TOTAL')?>
                                                </div>
                                                <div class="price">
                                                    <?=$price?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <a href="#" class="btn btn-default">
                                                <?=Yii::t('app','BY TICKET')?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
