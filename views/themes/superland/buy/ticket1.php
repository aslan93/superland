<?php

use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;

$bundle = SuperFrontAssets::register($this);
$this->title = 'Buy Ticket';
?>

<div class="name-page" style="min-height: 544px">
    <img class="parallax-img" src='<?=$bundle->baseUrl?>/images/name-page-bg.jpg' alt="" style="top: -160%">
</div>

<section class="bye-ticket">
    <div class="container">
        <div class="ticket-box">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="big-title">
                        <?=Yii::t('app','Buy ticket')?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="ticket-page-active">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page active">
                                    1
                                </div>
                                <div class="name">
                                    <?=Yii::t('app','Choose')?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page">
                                    2
                                </div>
                                <div class="name"><?=Yii::t('app','Sign up/ Sign in')?></div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="number-page">
                                    3
                                </div>
                                <div class="name">
                                    <?=Yii::t('app','Play')?>
                                </div>
                            </div>
                        </div>
                        <div class="border"></div>
                    </div>
                </div>
            </div>
            <div class="name-section">
                <?=Yii::t('app','choose a package')?>
            </div>
            <div>
                <?php $form = \kartik\form\ActiveForm::begin([
                    'id' => 'buyer-form',
                    'options' => [
                        'data-pjax' => '',

                    ],
                    'fieldConfig' => [
                        'template' => "{input}",
                        'options' => [
                            'tag' => false,
                        ],
                    ],
                ]); ?>

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="big-check-box" data-value="kids" data-mh="4">
                            <div class="overlay"></div>
                            <div class="choose-package-box">
                                <div class="big-check-box-bg first">
                                    <img src="<?= $bundle->baseUrl ?>/images/kids-section.png" alt="">
                                </div>
                                <div class="choose-package-box-dif">
                                        <span class="number-package">
                                            1
                                        </span>
                                    <?=Yii::t('app','KIDS')?>
                                </div>
                            </div>
                            <?php
                            foreach($ticketsK as $ticket) {
                                ?>
                                <div>
                                    <label for='<?=$ticket->ID?>'>
                                        <?= $form->field($buyer, 'KidTicketID',[
                                        'template' => ' {input}',
                                       ])->input('radio',['class'=>'style-input','value'=>$ticket->ID,'id'=>$ticket->ID])->label() ?>

                                        <?=$ticket->lang->Name .' '. $ticket->Price .' Lei'?>
                                    </label>

                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="mt30">
                            <div class="name-section">
                                <?=Yii::t('app','ADITIONAL')?>
                            </div>
                            <div class="text">
                                <?=Yii::t('app','FRI.-SUN - 15 RON/EACH EXTRA HOUR')?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="big-check-box" data-value="parent" data-mh="4">
                            <div class="overlay"></div>
                            <div class="choose-package-box">
                                <div class="big-check-box-bg last">
                                    <img src="<?=$bundle->baseUrl?>/images/parents-section.png" alt="">
                                </div>
                                <div class="choose-package-box-dif">
                                        <span class="number-package">
                                            2
                                        </span>
                                    <?=Yii::t('app','PARENTS')?>
                                </div>
                            </div>
                            <?php
                            foreach($ticketsP as $ticket) {
                                ?>
                                <div>
                                    <label>
                                        <?= $form->field($buyer, 'ParTicketID',[
                                            'template' => ' {input}',
                                        ])->input('radio',['class'=>'style-input','value'=>$ticket->ID])->label() ?>

                                        <?=$ticket->lang->Name .' '. $ticket->Price .' Lei'?>
                                    </label>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="mt30">
                            <div class="name-section">
                                <?=Yii::t('app','ADVANTAGES OF ONLINE SHOPPING')?>
                            </div>
                            <ul>
                                <li>
                                    <span class="fa fa-check"></span>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                </li>
                                <li>
                                    <span class="fa fa-check"></span>
                                    Aliquam tincidunt mauris eu risus.
                                </li>
                                <li>
                                    <span class="fa fa-check"></span>
                                    Vestibulum auctor dapibus neque.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="text-center mt40">
                    <?= Html::submitButton( 'Next', ['class' => 'btn btn-default']) ?>
                </div>

                <?php \kartik\form\ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>
