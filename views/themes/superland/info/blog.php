<?php
use app\assets\SuperFrontAssets;
$bundle = SuperFrontAssets::register($this);

?>
<div class="name-page" style="min-height: 344px">
    <img class="parallax-img" src='<?=$bundle->baseUrl?>/images/name-page-bg.jpg' alt="" style="top: -250%">
    <div class="overlay"></div>
    <div class="title">
        <?=Yii::t('app','Blog')?>
    </div>
</div>

<section class="blog-posts">
    <div class="container">
        <div class="posts-wrap">

                <?php
                \yii\widgets\Pjax::begin([
                    'timeout' => 10000,
                    'id' => 'blog',
                ]);
                ?>

                <?=
                \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "<div class='row'>{items}</div>{pager}",
                    'itemView' => 'blog_item',
                    'pager' => [
                        'class'               => 'mranger\load_more_pager\LoadMorePager',
                        'id'                  => 'comment-list-pagination',
                        'buttonText'          => 'LOAD MORE', // Текст на кнопке пагинации
                        'template'            => '<div class="text-center">{button}</div>', // Шаблон вывода кнопки пагинации
                        'contentSelector'     => '#w0', // Селектор контента
                        'contentItemSelector' => '.item', // Селектор эллементов контента
                        'includeCssStyles'    => false, // Подключать ли CSS стили виджета, или вы оформите пагинацию сами


                    ],
                ]);
                ?>
                <?php
                \yii\widgets\Pjax::end();
                ?>
            </div>

        </div>
    </div>
</section>



