<?php

use app\assets\SuperFrontAssets;
use yii\helpers\Url;

$bundle = SuperFrontAssets::register($this);
?>
<meta property="og:type"               content="article" />
<meta property="og:title"              content="<?=$event->lang->Name?>" />
<meta property="og:description"        content="<?=$event->lang->Description?>" />
<meta property="og:image"              content="<?=\yii\helpers\Url::home(['/event'])?><?= isset($event->mainImage)?$event->mainImage->imagePath:''?>" />

<div>
    <div class="modal-dialog full-screen" role="document">
        <div class="modal-content">
            <div class="modal-header text-right" >
                <a href="<?=\yii\helpers\Url::to(['/events-tab#view'])?>">
                    <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                </a>
            </div>
            <div class="modal-body">
                <div class="post-info">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="img-post" style="overflow: hidden">
                                    <?php
                                    if ($event->mainImage) {
                                        ?>
                                        <img src="<?= $event->mainImage->imagePath ?>" alt="">
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="title-post">
                                    <?=$event->lang->Name?>
                                </div>
                                <div class="post-details">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="post-box-details">
                                                <div class="img">
                                                    <img src="<?=$bundle->baseUrl?>/images/clock-circular-outline.png" alt="">
                                                </div>
                                                <div class="simple-text">
                                                    <?=$event->niceHour?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="post-box-details">
                                                <div class="img">
                                                    <img src="<?=$bundle->baseUrl?>/images/calendar-icon.png" alt="">
                                                </div>
                                                <div class="simple-text">
                                                    <?=$event->niceDate?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="post-box-details">
                                                <div class="img">
                                                    <img src="<?=$bundle->baseUrl?>/images/placeholder-filled-point.png" alt="">
                                                </div>
                                                <div class="simple-text">
                                                    OR.BRASOV SUPERLAND
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="description">
                                        <p>
                                            <?=$event->lang->Description?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <section class="gallery">
                        <div class="container">
                            <?php
                            if(count($galleries)>0) {
                            ?>
                            <div class="name-section">
                                <?=Yii::t('app','Gallery with photos from past events')?>
                            </div>
                            <div class="gallery-grid">
                                <div class="row">
                                    <?php
                                    foreach ($galleries as $event) {
                                        ?>
                                        <div class="col-md-2 col-sm-2 col-xs-4">
                                            <div class="gallery-item">
                                                <a onclick="getElem('<?= \yii\helpers\Url::to(['/ajax/get-gallery', 'id' => $event->ID]) ?>')">
                                                    <?php
                                                    if ($event->image) {
                                                        ?>
                                                        <img src="<?= $event->image ?>" alt="">
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="date-post">
                                                        <?php
                                                        if ($event->event) {
                                                            ?>
                                                            <?= $event->event->niceDate ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                                <?php
                            }
                            ?>
                            <div class="social-retails-plugins">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <div class="text-left">
                                            <button class="btn btn-primary views">
                                                <span class="fa fa-binoculars"></span>
                                                <?= $event->Visualisations ?>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <div class="text-right">
                                            <button class="btn btn-primary share" disabled>
                                                <span class="fa fa-share-alt"></span>
                                                SHARE
                                            </button>

                                            <span  data-href="<?=Url::home(['/event','id'=>$event->ID]).Url::to(['/event','id'=>$event->ID])?>" data-layout="button" data-size="small" data-mobile-iframe="true">
                                                <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=urlencode(Url::home(['/event','id'=>$event->ID]).Url::to(['/event','id'=>$event->ID]))?>&amp;src=sdkpreparse">
                                                    <button class="btn btn-primary facebook-share">
                                                        <span class="fa fa-facebook"></span>
                                                    </button>
                                                </a>
                                            </span>
                                            <a class="" href="https://twitter.com/intent/tweet?url=<?=urlencode(Url::home(['/event','id'=>$event->ID]).Url::to(['/event','id'=>$event->ID]))?>">
                                                <button class="btn btn-primary twitter-share">
                                                    <span class="fa fa-twitter"></span>
                                                </button>
                                            </a>
                                            <a href="https://plus.google.com/share?url=<?=urlencode(Url::home(['/event','id'=>$event->ID]).Url::to(['/event','id'=>$event->ID]))?>">
                                                <button class="btn btn-primary google-plus-share">
                                                    <span class="fa fa-google-plus"></span>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                <div class="footer-modal">
                    <div class="container">
                        <div class="text-center">
                            <button type="button" class="btn btn-default">
                                <?=Yii::t('app','BOOK NOW')?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- AJAX gallery MODAL-->
<div class="modal fade" id="gallery-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="<?=$bundle->baseUrl?>/images/close-modal.png" alt="">
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<?php $this->registerJs(
    "   
            $(document).on('click','.more-details',function(){
                 $('.modal-body').empty();       
            });
            
            function submitForm(data,type){
            if(type == 'Type'){
                $('.type-input').val(data);
              }else{
                $('.date-input').val(data);
              }
                var form = $('#main-form');
                var formData = form.serialize();
                $.pjax.reload({container: '#events',data: formData, push:false,replace:false,timeout:10000});
            }
            
         function resetCat(){
            $('.categories a').removeClass('active');
            submitForm('','Category',null);
         }
         function resetBrand(){
            $('.brands a').removeClass('active');
            $('.brand').val('');
            submitForm('','Brand',null);
            
         }
         function resetPrice(){
            
            $('#keyboard').val($('#keyboard').attr('data-min'));
            $('#keyboard2').val($('#keyboard2').attr('data-max'));
            submitForm();
            
         }
         $(document).on('pjax:end','#catalog',function(){
            $('.product-box').matchHeight();
         });
         
         if($('.summary').length > 0){
            $('.result-product').html($('.summary').html());
            }else{
            $('.result-product').html('');
         }    
         
         function getElem(url){
             $('#gallery-modal .modal-body').empty();                 
             $.ajax({
                  url: url,
                }).done(function(data) {
                 $('#gallery-modal .modal-body').html(data);
                 $('#gallery-modal').modal();
                 $('#gallery-modal').on('shown.bs.modal', function(){
                         var galleryTop = new Swiper('#gallery-modal .gallery-top', {
                              spaceBetween: 10,
                              navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                              },
                            });
                            var galleryThumbs = new Swiper('#gallery-modal .gallery-thumbs', {
                              spaceBetween: 10,
                              centeredSlides: true,
                              slidesPerView: 'auto',
                              touchRatio: 0.2,
                              slideToClickedSlide: true,
                            });
                            galleryTop.controller.control = galleryThumbs;
                            galleryThumbs.controller.control = galleryTop;
                    });
                   
                }); 
         }
           
            
            "
    , \yii\web\View::POS_END);
?>

