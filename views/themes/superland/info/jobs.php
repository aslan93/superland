<?php
$this->title = 'Superland Jobs';

use app\assets\SuperFrontAssets;

$bundle = SuperFrontAssets::register($this);
?>
?>
<div class="name-page" style="background: url('<?=$bundle->baseUrl?>/images/name-page-bg.jpg') ; height: 350px ;
background-position: center 50%; background-repeat: no-repeat ;
background-size: 100% auto"></div>

<section class="jobs-section">
    <div class="container">
            <?=
            \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}",
                'itemView' => 'job_item',
            ]);
            ?>
    </div>
</section>

<?php
$this->registerJs('
 /* NEW SCRIP */
    var swiper = new Swiper(".form-slider", {
        pagination: {
            el: ".swiper-pagination",
            clickable: true
        },
    });

    $(".job-head").click(function(){
        $(this).find(".number-item").toggleClass("animated pulse");
        $(this).find(".arrow-down").toggleClass("active");
        $(this).closest(".job-item").find(".job-content").slideToggle();
        $(this).closest(".job-item").find(".job-content").find(".animate-btn").toggleClass("animated pulse")
    });

    ',\yii\web\View::POS_READY);
?>
