<?php
use app\assets\SuperFrontAssets;

$bundle = SuperFrontAssets::register($this);
?>

<div class="job-item">
    <div class="job-head">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-2">
                <div class="elem-wrap">
                    <div class="number-item">
                        <?=$index+1?>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <div class="elem-wrap">
                    <div class="title">
                        <?=$model->lang->Name?>
                    </div>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="elem-wrap">
                    <div class="arrow-down">
                        <img src="<?=$bundle->baseUrl?>/images/down-arrow.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="job-content">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="img">
                    <?=\yii\bootstrap\Html::img($model->image, ['class'=> ''])?>
                </div>
                <div class="applay-btn">
                    <a href="mailto:superland@example.com?Subject=Hello%20again" target="_top" class="btn btn-default">
                        <img class="animate-btn" src="<?=$bundle->baseUrl?>/images/applay-bg.png" alt="">
                        <?=Yii::t('app','APPLY')?>
                    </a>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="text-content">
                    <?=$model->lang->Description?>
                </div>
            </div>
        </div>
    </div>
</div>