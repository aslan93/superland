<?php
use app\assets\SuperFrontAssets;
$bundle = SuperFrontAssets::register($this);
$lastModel = \app\modules\Post\models\Post::find()->orderBy(['ID'=> SORT_ASC])->one();
if ($index==0 && $key == $lastModel->ID) {
    ?>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="big-box-post">
            <div class="wrap-post-item">
                <a href="<?= \yii\helpers\Url::to(['/info/single-blog', 'id' => $model->ID]) ?>" data-pjax="">
                    <div class="img">
                        <?= \yii\bootstrap\Html::img($model->mainImage->imagePath) ?>
                    </div>
                    <div class="content">
                        <div class="title-wrap" data-mh="112">
                            <?=$key?><?= $model->lang->Title ?>
                        </div>
                        <div class="date-post">
                            <?= $model->niceDate ?>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <?php
}else{
    ?>
    <div class="col-md-3 col-sm-6 col-xs-6 item">
        <div class="wrap-post-item">
            <a href="<?= \yii\helpers\Url::to(['/info/single-blog', 'id' => $model->ID]) ?>" data-pjax="">
                <div class="img">
                    <?= \yii\bootstrap\Html::img($model->mainImage->imagePath) ?>
                </div>
                <div class="content">
                    <div class="title-wrap" data-mh="112">
                        <?=$key?><?= $model->lang->Title ?>
                    </div>
                    <div class="date-post">
                        <?= $model->niceDate ?>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <?php
}
?>

