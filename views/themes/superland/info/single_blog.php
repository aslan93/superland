<?php

use app\assets\SuperFrontAssets;

$this->title = $post->lang->Title;
$bundle = SuperFrontAssets::register($this);
?>
<section class="blog-top" style="height: 684px; background: url('<?=$bundle->baseUrl?>/images/blog-bg-top.jpg');
        background-repeat: no-repeat; background-size: cover; background-position: center center">
</section>
<section class="blog-post">
    <div class="container">
        <div class="header-post">
            <div class="title-blog">
                <?=$post->lang->Title?>
            </div>
            <div class="date-post">
                <?=$post->niceDate?>
            </div>
        </div>
        <div class="content">
            <?=$post->lang->Content?>
        </div>
        <div class="other-posts mt70">
            <div class="title">
                <?=Yii::t('app','Related blogs')?>
            </div>
            <div class="row">
                <?php
                foreach ($related as $blog) {
                    if ($blog->ID != $post->ID){
                    ?>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="wrap-post-item">
                            <a href="<?= \yii\helpers\Url::to(['/info/single-blog', 'id' => $blog->ID]) ?>" data-pjax="">
                                <div class="img">
                                    <?= \yii\bootstrap\Html::img($blog->mainImage->imagePath) ?>
                                </div>
                                <div class="content">
                                    <div class="title-wrap" data-mh="111">
                                        <?= $blog->lang->Title ?>
                                    </div>
                                    <div class="date-post">
                                        <?= $blog->niceDate ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</section>

