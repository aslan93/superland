<?php
    use app\assets\SuperFrontAssets;
$bundle = SuperFrontAssets::register($this);
?>


<div class="name-page" style="min-height: 544px">
    <img class="parallax-img" src='<?=$bundle->baseUrl?>/images/name-page-bg.jpg' alt="" style="top: -160%">
    <div class="overlay"></div>
    <div class="title">
        <?=Yii::t('app','Rules & Regulations')?>
    </div>
</div>

<section class="rules">
    <div class="container">
        <div class="content">
            <?=\app\modules\Settings\Settings::getByName('rules',true)?>
        </div>
    </div>
</section>