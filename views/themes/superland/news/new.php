<?php
use app\assets\SuperFrontAssets;

$bundle = SuperFrontAssets::register($this);
?>

<div class="about-post">
    <div class="img-post">
        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/ajax-img-post.png" alt="">
    </div>
    <div id="elem2" class="description-post">
        <a class="close-ajax-post" href="#">
            <span class="fa fa-close"></span>
        </a>
        <div class="date-post">
            <?=$new->getdMYDate()?>
        </div>
        <div class="title-news-box">
            <?=$new->lang->Title?>
        </div>
        <div>
            <?=$new->lang->Text?>
        </div>
    </div>
</div>
