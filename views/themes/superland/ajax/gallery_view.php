<?php
use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;
$bundle = SuperFrontAssets::register($this);
?>
<div class="container">
    <div class="gallery-slider">
        <div class="small-title">
            <?=$gallery->event->lang->Name?>
        </div>
        <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
                <?php
                foreach ($gallery->items as $item) {
                    ?>
                    <div class="swiper-slide">
                        <img src="<?=$item->image?>" alt="">
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
                    <?php
                    foreach ($gallery->items as $item) {
                        ?>
                        <div class="swiper-slide">
                            <div class="blue-overlay"></div>
                            <img src="<?=$item->image?>" alt="">
                        </div>
                        <?php
                    }
                    ?>
            </div>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next swiper-button-black gallery-btn-next"></div>
        <div class="swiper-button-prev swiper-button-black gallery-btn-prev"></div>

        <div class="social-retails-plugins">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="text-left">
                        <button class="btn btn-primary views">
                            <span class="fa fa-binoculars"></span>
                            <?=$gallery->Visualisations?>
                        </button>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-9">
<!--                    <div class="text-right">-->
<!--                        <button class="btn btn-primary share" disabled>-->
<!--                            <span class="fa fa-share-alt"></span>-->
<!--                            SHARE-->
<!--                        </button>-->
<!--                        <button class="btn btn-primary facebook-share">-->
<!--                            <span class="fa fa-facebook"></span>-->
<!--                        </button>-->
<!--                        <button class="btn btn-primary twitter-share">-->
<!--                            <span class="fa fa-twitter"></span>-->
<!--                        </button>-->
<!--                        <button class="btn btn-primary google-plus-share">-->
<!--                            <span class="fa fa-google-plus"></span>-->
<!--                        </button>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs(  "



"  , \yii\web\View::POS_END);
?>
