<?php
use yii\helpers\Html;
use app\assets\SuperFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\modules\Settings\Settings;
$bundle = SuperFrontAssets::register($this);
?>
<div class="post-info">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="img-post" style="overflow: hidden;">
                    <a href="<?=$bundle->baseUrl?>" data-toggle="lightbox" data-gallery="mixedgallery">
                        <img src="<?=$bundle->baseUrl?>/images/modal-post-img.jpg" alt="">
                    </a>
                </div>
                <div class="images row">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="img-thumb">
                            <img src="<?=$bundle->baseUrl?>/images/modal-post-img.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="img-thumb">
                            <div class="overlay">
                                <span class='fa fa-eye'></span>
                            </div>
                            <img src="<?=$bundle->baseUrl?>/images/party.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="img-thumb">
                             <div class="overlay">
                                <span class='fa fa-eye'></span>
                            </div>
                            <img src="<?=$bundle->baseUrl?>/images/people.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="img-thumb">
                             <div class="overlay">
                                <span class='fa fa-eye'></span>
                            </div>
                            <img src="<?=$bundle->baseUrl?>/images/people.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="img-thumb">
                             <div class="overlay">
                                <span class='fa fa-eye'></span>
                            </div>
                            <img src="<?=$bundle->baseUrl?>/images/people.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="title-post">
                    <?=$room->lang->Name?>
                </div>
                <div class="description">
                    <p>
                        <?=$room->lang->Description?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs(
    "   
            $(document).on('click','.more-details',function(){
                 $('.modal-body').empty();       
            });
            "
    , \yii\web\View::POS_END);
?>

