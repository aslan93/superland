<?php
use app\assets\SuperFrontAssets;

$bundle = SuperFrontAssets::register($this);
?>
<div class="post-info">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="img-post" style="overflow: hidden">
                    <?php
                    if ($model->mainImage) {
                        ?>
                        <img src="<?= $model->mainImage->imagePath ?>" alt="">
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="title-post">
                    <?=$model->lang->Title?>
                </div>
                <div class="post-details parameters" >
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="post-box-details">
                                <div class="img">
                                    <span class="fa fa-arrows-h" style="font-size: 40px"></span>
                                </div>
                                <div class="simple-text">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="post-box-details">
                                <div class="img">
                                    <span class="fa fa-eye" style="font-size: 40px"></span>
                                </div>
                                <div class="simple-text">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="post-box-details">
                                <div class="img">
                                    <span class=""style="font-size: 20px; font-weight: 900" >max <?=$model->KGTo?> KG</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="description">
                        <p>
                            <?=$model->lang->Description?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


