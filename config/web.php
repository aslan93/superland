<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'ro',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
            'imagemanager' => [
                'class' => 'noam148\imagemanager\components\ImageManagerGetPath',
                //set media path (outside the web folder is possible)
                'mediaPath' => '/path/where/to/store/images/media/imagemanager',
                //path relative web folder to store the cache images
                'cachePath' => 'assets/images',
                //use filename (seo friendly) for resized images else use a hash
                'useFilename' => true,
                //show full url (for example in case of a API)
                'absoluteUrl' => false,
                'databaseComponent' => 'db' // The used database component by the image manager, this defaults to the Yii::$app->db component
            ],

        'assetManager' => [
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyC_XXNZQK5LYRLij0oQNGPpxD4JVOVcJhU',
                        'language' => Yii::$app->language,
                        'version' => '3.1.18'
                    ]
                ]
            ],
            'forceCopy' => true,
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'aH-Duaufn8iaqyzr0F8yW0tlPKdhhLDC',
            'baseUrl' => ''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'test@gmail.com',
//                'password' => '',
//                'port' => '587',
//                'encryption' => 'tls',
//            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'class' => 'app\core\CoreUrlManager',
            'languages' => $params['siteLanguages'],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableDefaultLanguageUrlCode' => true,
            'enableLanguageDetection' => false,
            'rules' => [
                'admin' => 'admin/dashboard',
                'admin/<module>/<controller>/<action>' => 'admin/<module>/<controller>/<action>',
                'admin/<module>/<controller>/' => 'admin/<module>/<controller>',
                'admin/<module>/' => 'admin/<module>',
                'map' => 'site/map',
                'events'=>'site/events',
                'event' => 'info/event',
                'contact'=>'site/contact',
                'faq'=>'site/faq',
                'site'=> 'site/index',
                'registration'=>'site/registration',
                'imagemanager'=> 'imagemanager',
                '<action>'=>'site/<action>',
                'site/<action>' => 'site/<action>',
                '<controller>/<action>' => '<controller>/<action>',
                '<route:(.*)>' => 'landing/index',
            ]
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/views/themes/superland',
                'baseUrl' => '@web/themes/superland',
                'pathMap' => [
                    '@app/views' => '@app/views/themes/superland',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app'       => 'app.php',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\Admin\Admin',
            'modules' => [
                'user' => [
                    'class' => 'app\modules\User\User',
                    'defaultRoute' => 'user/index',
                ],
                'dashboard' => [
                    'class' => 'app\modules\Dashboard\Dashboard',
                ],
                'hotel' => [
                    'class' => 'app\modules\Hotel\Hotel',
                    'defaultRoute' => 'hotel/index',
                ],
                'reservation' => [
                    'class' => 'app\modules\Reservation\Reservation',
                    'defaultRoute' => 'reservation/index',
                ],
                'map' => [
                    'class' => 'app\modules\Map\Map',
                    'defaultRoute' => 'map-markers/index',
                ],
                'faq' => [
                    'class' => 'app\modules\Faq\Faq',
                    'defaultRoute' => 'faq/index',
                ],
                'faq-category' => [
                    'class' => 'app\modules\FaqCategory\FaqCategory',
                    'defaultRoute' => 'faq-category/index',
                ],
                'article' => [
                    'class' => 'app\modules\Article\Article',
                    'defaultRoute' => 'article/index',
                ],
                'events' => [
                    'class' => 'app\modules\Events\Events',
                    'defaultRoute' => 'events/index',
                ],
                'food' => [
                    'class' => 'app\modules\Food\Food',
                    'defaultRoute' => 'food/index',
                ],
                'food-category' => [
                    'class' => 'app\modules\FoodCategory\FoodCategory',
                    'defaultRoute' => 'food-category/index',
                ],
                'locations' => [
                    'class' => 'app\modules\Locations\Locations',
                    'defaultRoute' => 'locations/index',
                ],
                'games' => [
                    'class' => 'app\modules\Games\Games',
                    'defaultRoute' => 'games/index',
                ],
                'menu' => [
                    'class' => 'app\modules\Menu\Menu',
                    'defaultRoute' => 'menu/index',
                ],
                'slider' => [
                    'class' => 'app\modules\Slider\Slider',
                ],
                'settings' => [
                    'class' => 'app\modules\Settings\Settings',
                    'defaultRoute' => 'default/settings',
                ],
                'feedback' => [
                    'class' => 'app\modules\Feedback\Feedback',
                    'defaultRoute' => 'feedback/index',
                ],
                'gallery' => [
                    'class' => 'app\modules\Gallery\Gallery',
                    'defaultRoute' => 'gallery/index',
                ],
                'poll' => [
                    'class' => 'app\modules\Poll\Poll',
                ],
                'tickets' => [
                    'class' => 'app\modules\Tickets\Tickets',
                    'defaultRoute' => 'tickets/index',
                ],
                'buy-tickets' => [
                    'class' => 'app\modules\BuyTickets\BuyTickets',
                    'defaultRoute' => 'buy-tickets/index',
                ],
                'boutique-category' => [
                    'class' => 'app\modules\BoutiqueCategory\BoutiqueCategory',
                    'defaultRoute' => 'boutique-category',
                ],
                'boutique' => [
                    'class' => 'app\modules\Boutique\Boutique',
                    'defaultRoute' => 'boutique/index',
                ],
                'blog' => [
                    'class' => 'app\modules\Post\Post',
                    'defaultRoute' => 'post/index',
                ],
                'jobs' => [
                    'class' => 'app\modules\Jobs\Jobs',
                    'defaultRoute' => 'jobs/index',
                ],
                'event-menu' => [
                    'class' => 'app\modules\EventMenu\EventsMenu',
                    'defaultRoute' => 'event-menu/index',
                ],
                'extra-options' => [
                    'class' => 'app\modules\ExtraOptions\ExtraOptions',
                    'defaultRoute' => 'extra-options/index',
                ],
                'event-package' => [
                    'class' => 'app\modules\EventPackage\EventPackage',
                    'defaultRoute' => 'event-package/index',
                ],
                'event-room' => [
                    'class' => 'app\modules\EventRoom\EventRoom',
                    'defaultRoute' => 'event-room/index',
                ],
            ]
        ],
        'imagemanager' => [
            'class' => 'noam148\imagemanager\Module',
            //set accces rules ()
            'canUploadImage' => true,
            'canRemoveImage' => function(){
                return true;
            },
            'deleteOriginalAfterEdit' => false, // false: keep original image after edit. true: delete original image after edit
            // Set if blameable behavior is used, if it is, callable function can also be used
            'setBlameableBehavior' => false,
            //add css files (to use in media manage selector iframe)
            'cssFiles' => [
                'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',
            ],
        ],
        // system modules
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
