<?php

return [
    'adminEmail' => 'colesnic89@gmail.com',
    'siteLanguages' => ['ro', 'en' , 'fr','ru'],
    'googleAnalitics' => [
        'siteUACode' => 'UA-35194014-1',
        'clientId' => '1070409710005-29jeg72rvbesdu1a14c6uk7sfo9etdlp.apps.googleusercontent.com',
    ],
    'displayDateFormat' => 'd.m.Y',
    'dateTimeFormatPHP' => 'd F Y ',
    'dateTimeFormatJS' => 'dd.mm.yyyy',
];
